﻿using KG.Props.Data.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using SqlKata.Compilers;

namespace KG.Props.Data.Repository.Real
{
    public class RepositoryWithCompiler: Repository, IRepositoryWithCompiler
    {
        protected Compiler _compiler;

        public RepositoryWithCompiler(DbContext dbContext): base(dbContext)
        {
            _compiler = new SqlServerCompiler();
        }

        public Compiler GetCompiler()
        {
            return _compiler;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);
            _compiler = null;
        }
    }
}
