﻿using KG.Props.Data.Repository.Abstract;
using KG.Props.Extensions.Expressions;
using KG.Props.Extensions.Types;
using KG.Props.Resources.Text;
using KG.Props.Extensions.DataTables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using System.Data.SqlClient;
using System.Threading;
using System.ComponentModel;

namespace KG.Props.Data.Repository.Real
{
    public class Repository : IRepository
    {
        #region Exposed properties
        // from interface
        public DbContext DbContext { get; set; }
        public IDbContextTransaction DbTransaction { get { return _transaction; } set { _transaction = value; } }
        public bool AutoSaveAfterEachOperation { get; set; } = false;

        // protected, for children
        /// <summary>
        /// The name that identified this repository. Useful for debug, exceptions
        /// </summary>
        protected string name = "Default repository";

        /// <summary>
        /// The current transaction
        /// </summary>
        protected IDbContextTransaction _transaction;

        /// <summary>
        /// The isolation levels when querying for data (no alterations)
        /// </summary>
        protected IsolationLevel _isolationLevelQuery = IsolationLevel.ReadCommitted;

        /// <summary>
        /// The isolation levels when modifying data
        /// </summary>
        protected IsolationLevel _isolationLevelCommand = IsolationLevel.ReadCommitted;
        #endregion


        public Repository(DbContext dbContext)
        {
            DbContext = dbContext ?? throw new ArgumentNullException("dbContext");
        }


        #region CRUD Operations
        public IQueryable<T> GetAll<T>()
            where T : class
        {
            try
            {
                EnsureEverythingOkQuery();
                return DbContext.Set<T>();
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }
        }

        public T GetById<T, I>(I id, string idFieldName = "Id")
            where T : class
        {
            if (id == null)
                throw new ArgumentNullException("id");

            try
            {
                EnsureEverythingOkQuery();
                var equalsExpression = ExpressionsExtensions.PredicatePropertyEquals<T, I>(id, idFieldName);
                return DbContext.Set<T>().FirstOrDefault(equalsExpression);
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }
        }

        public I GetMaxId<T, I>(string idFieldName = "Id", Func<I> defaultFunc = null) where T : class
        {
            try
            {
                defaultFunc = defaultFunc ?? (() => default);
                var latestId = defaultFunc();

                EnsureEverythingOkQuery();
                var desiredField = ExpressionsExtensions.PredicateMemberAccess<T, I>(idFieldName);
                if (DbContext.Set<T>().Any())
                    latestId = DbContext.Set<T>().Max(desiredField);

                return latestId == null ? defaultFunc() : latestId;
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }
        }

        public async Task<I> GetMaxIdAsync<T, I>(string idFieldName = "Id", Func<I> defaultFunc = null) where T : class
        {
            try
            {
                defaultFunc = defaultFunc ?? (() => default);
                var latestId = defaultFunc();

                await EnsureEverythingOkQueryAsync();
                var desiredField = ExpressionsExtensions.PredicateMemberAccess<T, I>(idFieldName);
                if (DbContext.Set<T>().Any())
                    latestId = await DbContext.Set<T>().MaxAsync(desiredField);

                return latestId == null ? defaultFunc() : latestId;
            }
            finally
            {
                if (AutoSaveAfterEachOperation) await FinishTransactionAsync(commit: false);
            }
        }

        public int CountAll<T>() where T : class
        {
            try
            {
                EnsureEverythingOkQuery();
                return DbContext.Set<T>().Count();
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }
        }

        public async Task<int> CountAllAsync<T>() where T : class
        {
            try
            {
                await EnsureEverythingOkQueryAsync();
                return await DbContext.Set<T>().CountAsync();
            }
            finally
            {
                if (AutoSaveAfterEachOperation) await FinishTransactionAsync(commit: false);
            }
        }

        public void DeleteAll<T>() where T : class
        {
            var data = GetAll<T>().ToList();
            foreach (var item in data)
            {
                MarkAsDeleted(item);
            }

        }


        public T Create<T, I>(T obj, I idFieldValue, string idFieldName = "Id", bool savechanges = true, bool checkForExisting = true) where T : class
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            if (idFieldValue == null)
                throw new ArgumentNullException("idFieldValue");

            try
            {
                EnsureEverythingOkCommand();

                if (checkForExisting && idFieldName != null)
                {
                    var existing = GetById<T, I>(idFieldValue, idFieldName);
                    if (existing != null)
                        throw new TypeAccessException(string.Format("Object of type {0} with [{1}] - '{2}' already exists.", typeof(T).Name, idFieldName, idFieldValue));
                }

                DbContext.Entry(obj).State = EntityState.Added;
                if (savechanges)
                    DbContext.SaveChanges();

                if (AutoSaveAfterEachOperation) FinishTransaction();

                return obj;
            }
            catch (TypeAccessException)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw;
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }

                throw new Exception(Literals.CannotCreate, ex);
            }
        }

        public void MarkAsInserted<T>(T obj, bool saveChanges = true) where T : class
        {
            try
            {
                EnsureEverythingOkCommand();
                DbContext.Entry(obj).State = EntityState.Added;
                if (saveChanges)
                    DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw new Exception(Literals.CannotCreate, ex);
            }
        }

        public async Task MarkAsInsertedAsync<T>(T obj, bool saveChanges = true) where T : class
        {
            try
            {
                await EnsureEverythingOkCommandAsync();
                DbContext.Entry(obj).State = EntityState.Added;
                if (saveChanges)
                    await DbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                try
                {
                    await FinishTransactionAsync(commit: false);
                }
                catch { }
                throw new Exception(Literals.CannotCreate, ex);
            }
        }

        public void AddEntities<T>(IEnumerable<T> data) where T : class
        {
            this.EnsureEverythingOkCommand();
            this.DisableChangeDetection();

            foreach (var item in data)
                MarkAsInserted(item, false);

            this.EnableChangeDetection();
            this.DbContext.SaveChanges();
        }


        public void Delete<T, I>(I idFieldValue, string idFieldName = "Id") where T : class
        {
            if (idFieldValue == null)
                throw new ArgumentNullException("idFieldValue");
            try
            {
                EnsureTransactionExists(_isolationLevelCommand);
                EnsureConnectionOpen();

                var equalExpression = ExpressionsExtensions.PredicatePropertyEquals<T, I>(idFieldValue, idFieldName);
                var existingAll = GetAll<T>().Where(equalExpression);
                if (!existingAll.Any())
                    return; // all good, it does not exist

                foreach (var existing in existingAll)
                {
                    DbContext.Entry(existing).State = EntityState.Deleted;
                }
                DbContext.SaveChanges();

                if (AutoSaveAfterEachOperation) FinishTransaction();
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw new Exception(Literals.CannotDelete, ex);
            }
        }

        public void MarkAsDeleted<T>(T obj, bool saveChanges = true) where T : class
        {
            try
            {
                EnsureEverythingOkCommand();
                DbContext.Entry(obj).State = EntityState.Deleted;
                if (saveChanges)
                    DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw new Exception(Literals.CannotDelete, ex);
            }
        }

        public async Task MarkAsDeletedAsync<T>(T obj, bool saveChanges = true) where T : class
        {
            try
            {
                await EnsureEverythingOkCommandAsync();
                DbContext.Entry(obj).State = EntityState.Deleted;
                if (saveChanges)
                    await DbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                try
                {
                    await FinishTransactionAsync(commit: false);
                }
                catch { }
                throw new Exception(Literals.CannotDelete, ex);
            }
        }

        public void DeleteEntities<T>(IEnumerable<T> data) where T : class
        {
            this.EnsureEverythingOkCommand();
            this.DisableChangeDetection();

            foreach (var item in data)
                MarkAsDeleted(item, false);

            this.EnableChangeDetection();
            this.DbContext.SaveChanges();
        }

        public void DeleteEntities<T>(Expression<Func<T, bool>> predicate, bool saveChanges = true) where T : class
        {
            try
            {
                this.EnsureEverythingOkCommand();
                GetAll<T>().Where(predicate).Delete();
                if (saveChanges)
                    DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch
                {
                }

                throw new Exception(Literals.CannotDelete, ex);
            }
        }


        public void MarkAsModified<T>(T obj, bool saveChanges = true) where T : class
        {
            try
            {
                EnsureEverythingOkCommand();
                DbContext.Entry(obj).State = EntityState.Modified;
                if (saveChanges)
                    DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw new Exception(Literals.CannotUpdate, ex);
            }
        }

        public async Task MarkAsModifiedAsync<T>(T obj, bool saveChanges = true) where T : class
        {
            try
            {
                await EnsureEverythingOkCommandAsync();
                DbContext.Entry(obj).State = EntityState.Modified;
                if (saveChanges)
                    await DbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                try
                {
                    await FinishTransactionAsync(commit: false);
                }
                catch { }
                throw new Exception(Literals.CannotUpdate, ex);
            }
        }

        public void ModifyEntities<T>(IEnumerable<T> data) where T : class
        {
            this.EnsureEverythingOkCommand();
            this.DisableChangeDetection();

            foreach (var item in data)
                MarkAsModified(item, false);

            this.EnableChangeDetection();
            this.DbContext.SaveChanges();
        }
        #endregion


        #region Queries and Stored Procedures
        public IEnumerable<T> ExecuteQuery<T>(string queryString, Dictionary<string, object> parameters = null) where T : new()
        {
            var output = new List<T>();
            if (String.IsNullOrEmpty(queryString))
            {
                throw new ArgumentException(Literals.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureEverythingOkQuery();
                var command = CreateCommand(queryString, parameters);
                command.Transaction = _transaction.GetDbTransaction();
                var typeT = typeof(T);
                var isPrimitive = typeT.IsPrimitive || typeT.IsValueType || (typeT == typeof(string));
                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        T tempObject = default;
                        if (isPrimitive)
                        {
                            try
                            {
                                tempObject = (T)reader[0];
                            }
                            catch { }
                        }
                        else
                        {
                            tempObject = new T();
                            foreach (var property in typeof(T).GetProperties())
                            {
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    var colName = reader.GetName(i);
                                    var type = reader.GetFieldType(i);
                                    property.SetValue(tempObject, reader[i]);
                                    // dt.Columns.Add(colName, type);
                                }
                            }
                        }
                        output.Add(tempObject);
                    }
                }
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }

            return output;
        }

        public DataTable ExecuteQuery(string queryString, Dictionary<string, object> parameters = null)
        {
            var dt = new DataTable();
            if (String.IsNullOrEmpty(queryString))
            {
                throw new ArgumentException(Literals.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureEverythingOkQuery();
                var command = CreateCommand(queryString, parameters);
                command.Transaction = _transaction.GetDbTransaction();
                using (var reader = command.ExecuteReader())
                {
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        var colName = reader.GetName(i);
                        var type = reader.GetFieldType(i);
                        dt.Columns.Add(colName, type);
                    }

                    while (reader.Read())
                    {
                        var rowData = new List<object>();
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            var type = reader.GetFieldType(i);
                            rowData.Add(TypesExtensions.ChangeType(reader[i], type));
                        }
                        dt.Rows.Add(rowData.ToArray());
                    }
                }
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }

            return dt;
        }

        public T ExecuteQueryForValue<T>(string queryString, Dictionary<string, object> parameters = null)
            where T : new()
        {
            T result = default;
            result = ExecuteQuery<T>(queryString, parameters).FirstOrDefault();

            if (result == null)
                return default;

            return result;
        }

        public DataTable ExecuteGenericQueryTypeStoredProcedure(string storedProcedurename, Dictionary<string, object> parameters = null)
        {
            DataTable dt = new DataTable();
            if (String.IsNullOrEmpty(storedProcedurename))
            {
                throw new ArgumentException(Literals.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureEverythingOkQuery();
                var command = CreateCommand(storedProcedurename, parameters);
                command.Transaction = _transaction.GetDbTransaction();
                command.CommandType = CommandType.StoredProcedure;
                using (var reader = command.ExecuteReader())
                {
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        var colName = reader.GetName(i);
                        var type = reader.GetFieldType(i);
                        dt.Columns.Add(colName, type);
                    }

                    while (reader.Read())
                    {
                        var rowData = new List<object>();
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            var type = reader.GetFieldType(i);
                            rowData.Add(TypesExtensions.ChangeType(reader[i], type));
                        }
                        dt.Rows.Add(rowData.ToArray());
                    }
                }
            }
            catch (Exception)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw;
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }

            return dt;
        }

        public IEnumerable<T> ExecuteGenericQueryTypeStoredProcedure<T>(string storedProdecureName, Dictionary<string, object> parameters = null)
        {
            var dt = ExecuteGenericQueryTypeStoredProcedure(storedProdecureName, parameters);
            if (dt == null)
                return new List<T>();

            return dt.ToList<T>();
        }

        public void ExecuteCommandWithNoResults(string queryString, Dictionary<string, object> parameters = null)
        {
            if (String.IsNullOrEmpty(queryString))
            {
                throw new ArgumentException(Literals.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureEverythingOkCommand();
                var command = CreateCommand(queryString, parameters);
                command.Transaction = _transaction.GetDbTransaction();
                command.ExecuteNonQuery();

                if (AutoSaveAfterEachOperation) FinishTransaction();
            }
            catch (Exception)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw;
            }
        }

        public void ExecuteStoredProcedure(string storedProcedurename, Dictionary<string, object> parameters = null)
        {
            if (String.IsNullOrEmpty(storedProcedurename))
            {
                throw new ArgumentException(Literals.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureEverythingOkCommand();
                var command = CreateCommand(storedProcedurename, parameters);
                command.Transaction = _transaction.GetDbTransaction();
                command.CommandType = CommandType.StoredProcedure;
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw;
            }
            finally
            {
                if (AutoSaveAfterEachOperation) FinishTransaction(commit: false);
            }
        }
        #endregion


        #region Table Helpers
        public IEnumerable<String> GetTableColumns(string tableName, string tableSchema = "dbo")
        {
            var spname = tableSchema + ".SP_GET_TABLE_COLUMNS";
            var parameters = new Dictionary<string, object>()
            {
                { "@table_schema", tableSchema },
                { "@table_name", tableName }
            };

            return ExecuteGenericQueryTypeStoredProcedure<string>(spname, parameters);
        }
        #endregion


        #region Tools
        public void EnsureReady()
        {
            EnsureTransactionExists(_isolationLevelCommand);
            EnsureConnectionOpen();
        }

        public async Task EnsureReadyAsync()
        {
            await EnsureTransactionExistsAsync(_isolationLevelCommand);
            await EnsureConnectionOpenAsync();
        }

        public void PersistChanges()
        {
            FinishTransaction();
        }

        public async Task PersistChangesAsync()
        {
            await FinishTransactionAsync();
        }

        public void RevertChanges()
        {
            FinishTransaction(commit: false);
        }

        public async Task RevertChangesAsync()
        {
            await FinishTransactionAsync(commit: false);
        }

        public void CloseConnection(bool saveChanges = true)
        {
            try
            {
                if (saveChanges)
                    DbContext.SaveChanges();

                if (DbContext.Database.GetDbConnection().State != ConnectionState.Closed)
                {
                    DbContext.Database.GetDbConnection().Close();
                }
            }
            catch (Exception) // closes connection quitely
            {
                // Console.WriteLine(e.Message);
            }
        }

        public async Task CloseConnectionAsync(bool saveChanges = true)
        {
            try
            {
                if (saveChanges)
                    await DbContext.SaveChangesAsync();

                if (DbContext.Database.GetDbConnection().State != ConnectionState.Closed)
                {
                    DbContext.Database.GetDbConnection().Close();
                }
            }
            catch (Exception) // closes connection quitely
            {
                // Console.WriteLine(e.Message);
            }
        }

        public void EnableChangeDetection()
        {
            DbContext.ChangeTracker.AutoDetectChangesEnabled = true;
        }

        public void DisableChangeDetection()
        {
            DbContext.ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public T GetContext<T>() where T : DbContext
        {
            EnsureReady();
            return (DbContext as T);
        }
        #endregion


        #region PRIVATE / PROTECTED
        #region Commands
        /// <summary>
        /// Creates a Database Command with the given parameters
        /// </summary>
        /// <param name="commandText">The sql query to execute</param>
        /// <param name="parameters">Parameters to pass to the sql command</param>
        /// <returns></returns>
        private DbCommand CreateCommand(string commandText, Dictionary<string, object> parameters)
        {
            var command = DbContext.Database.GetDbConnection().CreateCommand();
            command.CommandText = commandText;
            command.CommandTimeout = 7200;
            if (parameters != null)
                command.Parameters.AddRange(ParseParameters(parameters));

            return command;
        }

        /// <summary>
        /// Given a set of parameters as dictionary, transforms them into a list of sql parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private object[] ParseParameters(Dictionary<string, object> parameters)
        {
            if (parameters == null)
                return Array.Empty<object>();

            var outputList = new List<DbParameter>();
            foreach (var param in parameters)
            {
                var parameter = new SqlParameter
                {
                    ParameterName = param.Key,
                    Value = param.Value ?? DBNull.Value
                };
                outputList.Add(parameter);
            }

            return outputList.ToArray();
        }
        #endregion

        #region Making sure exists
        /// <summary>
        /// Opens a connection if not open
        /// </summary>
        protected void EnsureConnectionOpen()
        {
            var retries = 5;
            if (DbContext.Database.GetDbConnection().State == ConnectionState.Open)
                return;

            while (retries >= 0 && DbContext.Database.GetDbConnection().State != ConnectionState.Open)
            {
                try
                {
                    DbContext.Database.OpenConnection();
                    DbContext.Database.SetCommandTimeout(9000);
                }
                catch (Exception) // too many connections, sleep a little
                {
                    Thread.Sleep(500);
                }
                finally
                {
                    retries--;
                    Thread.Sleep(30);
                }
            }
        }

        /// <summary>
        /// Opens a connection if not open
        /// </summary>
        protected async Task EnsureConnectionOpenAsync()
        {
            var retries = 5;
            if (DbContext.Database.GetDbConnection().State == ConnectionState.Open)
                return;

            while (retries >= 0 && DbContext.Database.GetDbConnection().State != ConnectionState.Open)
            {
                try
                {
                    await DbContext.Database.OpenConnectionAsync();
                    DbContext.Database.SetCommandTimeout(9000);
                }
                catch (Exception) // too many connections, sleep a little
                {
                    Thread.Sleep(500);
                }
                finally
                {
                    retries--;
                    Thread.Sleep(30);
                }
            }
        }

        /// <summary>
        /// If the transaction scope is null, opens a new one, otherwise leaves it be.
        /// </summary>
        /// <param name="isolationLevel">todo: describe isolationLevel parameter on EnsureTransactionExists</param>
        protected void EnsureTransactionExists(IsolationLevel isolationLevel)
        {
            if (_transaction != null)
                return;

            try
            {
                _transaction = DbContext.Database.BeginTransaction(isolationLevel);
            }
            catch { }
        }

        /// <summary>
        /// If the transaction scope is null, opens a new one, otherwise leaves it be.
        /// </summary>
        /// <param name="isolationLevel">todo: describe isolationLevel parameter on EnsureTransactionExists</param>
        protected async Task EnsureTransactionExistsAsync(IsolationLevel isolationLevel)
        {
            if (_transaction != null)
                return;

            try
            {
                _transaction = await DbContext.Database.BeginTransactionAsync(isolationLevel);
            }
            catch { }
        }


        protected void EnsureEverythingOkQuery()
        {
            EnsureTransactionExists(_isolationLevelQuery);
            EnsureConnectionOpen();
        }

        protected async Task EnsureEverythingOkQueryAsync()
        {
            await EnsureTransactionExistsAsync(_isolationLevelQuery);
            await EnsureConnectionOpenAsync();
        }

        protected void EnsureEverythingOkCommand()
        {
            EnsureTransactionExists(_isolationLevelCommand);
            EnsureConnectionOpen();
        }

        protected async Task EnsureEverythingOkCommandAsync()
        {
            await EnsureTransactionExistsAsync(_isolationLevelCommand);
            await EnsureConnectionOpenAsync();
        }
        #endregion

        #region Saving / Discarding
        /// <summary>
        /// Finishes a transaction, disposing the transaction scope object. Can be specified if to commit the transaction or dispose it.
        /// </summary>
        /// <param name="commit">Commit the transaction?</param>
        /// <param name="dispose">Dispose the transaction?</param>
        protected void FinishTransaction(bool commit = true, bool dispose = true)
        {
            if (_transaction == null)
            {
                return;
            }

            try
            {
                if (commit)
                {
                    try
                    {
                        DbContext.SaveChanges();
                        _transaction.Commit();
                    }
                    catch { }// commit if not already commited
                }

                if (dispose && !commit)
                {
                    try
                    {
                        _transaction.Rollback();
                    }
                    catch { }// dispose if not already disposed
                }

                // dispose of the transaction
                try
                {
                    _transaction.Dispose();
                }
                catch { }
            }
            catch (Exception)
            {
                // Console.WriteLine(e.Message);
            }
            finally
            {
                _transaction = null;
            }
        }

        /// <summary>
        /// Finishes a transaction, disposing the transaction scope object. Can be specified if to commit the transaction or dispose it.
        /// </summary>
        /// <param name="commit">Commit the transaction?</param>
        /// <param name="dispose">Dispose the transaction?</param>
        protected async Task FinishTransactionAsync(bool commit = true, bool dispose = true)
        {
            if (_transaction == null)
            {
                return;
            }

            try
            {
                if (commit)
                {
                    try
                    {
                        await DbContext.SaveChangesAsync();
                        _transaction.Commit();
                    }
                    catch { }// commit if not already commited
                }

                if (dispose && !commit)
                {
                    try
                    {
                        _transaction.Rollback();
                    }
                    catch { }// dispose if not already disposed
                }

                // dispose of the transaction
                try
                {
                    _transaction.Dispose();
                }
                catch { }
            }
            catch (Exception)
            {
                // Console.WriteLine(e.Message);
            }
            finally
            {
                _transaction = null;
            }
        }
        #endregion
        #endregion


        #region Dispose Implementation

        // Default initialization for a bool is 'false'
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Disposes everything. If everything was executing leaving the connection / transactions open for rapidity, closes them now and saves work. Make this transient to automatically save the workloads.
        /// </summary>
        public void Dispose()
        {
            // clear
            Dispose(true);

            // Always use SuppressFinalize() in case a subclass
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Executes the actual disposing of Literals. Thanks to:
        /// https://stackoverflow.com/questions/898828/finalize-dispose-pattern-in-c-sharp
        /// </summary>
        /// <param name="isDisposing"></param>
        protected virtual void Dispose(bool isDisposing)
        {
            try
            {
                if (!this.IsDisposed)
                {
                    if (isDisposing)
                    {
                        // Release all managed resources here
                        try
                        {
                            // make sure no transaction is left out hanging when services dispose this object
                            // also autocommits the transaction
                            if (DbContext?.Database != null)
                            {
                                FinishTransaction();
                            }
                        }
                        catch (Exception)
                        {
                        }

                        try
                        {
                            if (_transaction != null)
                                _transaction.Dispose();
                            if (DbContext != null)
                                DbContext.Dispose();
                        }
                        catch (Exception)
                        {
                        }

                    }

                    // Release all unmanaged resources here

                    // explicitly set root references to null to expressly tell the GarbageCollector
                    // that the resources have been disposed of and its ok to release the memory allocated for them.
                    _transaction = null;
                    DbContext = null;
                }
            }
            finally
            {
                // explicitly call the base class Dispose implementation
                this.IsDisposed = true;
            }
        }
        #endregion
    }
}
