﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KG.Props.Data.Repository.Abstract
{
    public interface IRepository : IDisposable
    {
        #region Exposed properties
        /// <summary>
        /// Returns the reference to the current database context
        /// </summary>
        DbContext DbContext { get; set; }

        /// <summary>
        /// Returns the reference to the current used transaction, if any transaction has been open at call time
        /// </summary>
        IDbContextTransaction DbTransaction { get; set; }

        /// <summary>
        /// The flag that specified if this repository should save changes after each operation that it does
        /// (sort of like running DbContext.SaveChanges everytime, only this commits the outer transaction)
        /// </summary>
        bool AutoSaveAfterEachOperation { get; set; }
        #endregion


        #region CRUD Operations
        /// <summary>
        /// Retrieves a list of all the objects of the generic type T from the database.
        /// </summary>
        /// <typeparam name="T">The Entity object type.</typeparam>
        /// <returns>Returns a IEnumerable streamable list (use .ToList() to make it concrete)</returns>
        IQueryable<T> GetAll<T>() where T : class;

        /// <summary>
        /// Retrieves an object of the generic type T from the database, by id. The id field can be dynamically specified.
        /// </summary>
        /// <typeparam name="T">The Entity object type.</typeparam>
        /// <typeparam name="I">The type of the ID Field.</typeparam>
        /// <param name="id">The value of the ID field.</param>
        /// <param name="idFieldName">The name of the Id field (this is the name of the property that uniquely identified the object T, and it is one of the properties of the object T).Can be dynamically specified. By default is "Id". It will throw exception if the name is not in the list of properties of T.</param>
        /// <returns>The object, if found in the database, or null.</returns>
        T GetById<T, I>(I id, string idFieldName = "Id") where T : class;

        /// <summary>
        /// Returns the latest max value for an ID field, for a table (represented by T), from database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="I"></typeparam>
        /// <param name="idFieldName"></param>
        /// <param name="defaultFunc">A function that returns a default value for type I (id type). If not specified, returns default(I)</param>
        /// <returns></returns>
        I GetMaxId<T, I>(string idFieldName = "Id", Func<I> defaultFunc = null) where T : class;

        /// <summary>
        /// Returns the latest max value for an ID field, for a table (represented by T), from database
        /// Runs async
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="I"></typeparam>
        /// <param name="idFieldName"></param>
        /// <param name="defaultFunc">A function that returns a default value for type I (id type). If not specified, returns default(I)</param>
        /// <returns></returns>
        Task<I> GetMaxIdAsync<T, I>(string idFieldName = "Id", Func<I> defaultFunc = null) where T : class;

        /// <summary>
        /// Counts all the objects in the database of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        int CountAll<T>() where T : class;

        /// <summary>
        /// Counts all the objects in the database of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<int> CountAllAsync<T>() where T : class;


        /// <summary>
        /// Creates an object of the generic type T in the database. The id field value must be specified as parameter in order to make this method be dynamic and to find if the object already exists in the database. Exception is thrown if the object already exists.
        /// </summary>
        /// <typeparam name="T">The Entity object type.</typeparam>
        /// <typeparam name="I">The type of the ID Field.</typeparam>
        /// <param name="obj">The object (entity, of type T) with values populated.</param>
        /// <param name="idFieldValue">The value of the ID field.</param>
        /// <param name="idFieldName">The name of the Id field (this is the name of the property that uniquely identified the object T, and it is one of the properties of the object T).Can be dynamically specified. By default is "Id". It will throw exception if the name is not in the list of properties of T.</param>
        /// <returns>Returns the created object.</returns>
        T Create<T, I>(T obj, I idFieldValue, string idFieldName = "Id", bool savechanges = true, bool checkForExisting = true) where T : class;

        /// <summary>
        /// Marks an entity as inserted in the DB
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        void MarkAsInserted<T>(T obj, bool saveChanges = true) where T : class;

        /// <summary>
        /// Marks an entity as inserted in the DB
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        Task MarkAsInsertedAsync<T>(T obj, bool saveChanges = true) where T : class;

        /// <summary>
        /// Inserts all entities of type {T}
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        void AddEntities<T>(IEnumerable<T> data) where T : class;

        /// <summary>
        /// Deletes an object of the generic type T in from the database. The id field value must be specified as parameter in order to make this method be dynamic and to find if the object already exists in the database. Exception is thrown if the object does not exists.
        /// </summary>
        /// <typeparam name="T">The Entity object type.</typeparam>
        /// <typeparam name="I">The type of the ID Field.</typeparam>
        /// <param name="obj">The object (entity, of type T) with values populated.</param>
        /// <param name="idFieldValue">The value of the ID field.</param>
        /// <param name="idFieldName">The name of the Id field (this is the name of the property that uniquely identified the object T, and it is one of the properties of the object T).Can be dynamically specified. By default is "Id". It will throw exception if the name is not in the list of properties of T.</param>
        void Delete<T, I>(I idFieldValue, string idFieldName = "Id") where T : class;

        /// <summary>
        /// Marks an entity as deleted in the DB
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param> 
        void MarkAsDeleted<T>(T obj, bool saveChanges = true) where T : class;

        /// <summary>
        /// Marks an entity as deleted in the DB
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param> 
        Task MarkAsDeletedAsync<T>(T obj, bool saveChanges = true) where T : class;

        /// <summary>
        /// Deletes all entities of type {T} based on predicate
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        void DeleteEntities<T>(Expression<Func<T, bool>> predicate, bool saveChanges = true) where T : class;

        /// <summary>
        /// Deletes multiple DB Entities from a list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        void DeleteEntities<T>(IEnumerable<T> data) where T : class;

        /// <summary>
        /// Uses GetAll and MarkAsDeleted to empty givven table row by row 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        void DeleteAll<T>() where T : class;

        /// <summary>
        /// Marks an entity as updated in the DB
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        void MarkAsModified<T>(T obj, bool saveChanges = true) where T : class;

        /// <summary>
        /// Marks an entity as updated in the DB
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        Task MarkAsModifiedAsync<T>(T obj, bool saveChanges = true) where T : class;

        /// <summary>
        /// Updates all entities of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        void ModifyEntities<T>(IEnumerable<T> data) where T : class;

        #endregion


        #region Queries and Stored Procedures
        /// <summary>
        /// Executes a SQL Query, using the given Database context for this repository. This method returns a list of the generic provided type T (if the fields from the database match - invariant). Remember that the returned list is concrete (materialized) and cannot be streamed in EF style. Good to use when you know exactly the data you need to map to a custom object.
        /// </summary>
        /// <typeparam name="T">The generic type T that the query results with map to properties. Property name must match (case does not matter)</typeparam>
        /// <param name="queryString">The query string. This must match the language of the Database Context.</param>
        /// <param name="parameters">The dictionary of parameters. Can be ignored if no parameters.</param>
        /// <returns></returns>
        IEnumerable<T> ExecuteQuery<T>(string queryString, Dictionary<string, object> parameters = null) where T : new();

        /// <summary>
        /// Same as ExecuteQuery with generic parameter, only it returns data under the form of a datatable.
        /// </summary>
        /// <param name="queryString"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        DataTable ExecuteQuery(string queryString, Dictionary<string, object> parameters = null);

        /// <summary>
        /// Executes a query that returns only a row from the table, that can be mapped to an object (primitives included).
        /// </summary>
        /// <typeparam name="T">The generic type of the object to map to.</typeparam>
        /// <param name="queryString">The query string. This must match the language of the Database Context.</param>
        /// <param name="parameters">The dictionary of parameters. Can be ignored if no parameters.</param>
        /// <returns></returns>
        T ExecuteQueryForValue<T>(string queryString, Dictionary<string, object> parameters = null)
            where T : new();

        /// <summary>
        /// Executes a stored procedure that will return data. The data will be mapped to a DataTable object
        /// </summary>
        /// <param name="storedProcedurename"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        DataTable ExecuteGenericQueryTypeStoredProcedure(string storedProcedurename, Dictionary<string, object> parameters = null);

        /// <summary>
        /// Executes a stored procedure that will return data and maps that data to a list of objects of type T, by doing property matching.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProdecureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        IEnumerable<T> ExecuteGenericQueryTypeStoredProcedure<T>(string storedProdecureName, Dictionary<string, object> parameters = null);

        /// <summary>
        /// Executes an insert / update / delete command (that returns no results).
        /// </summary>
        /// <param name="queryString">The query string. This must match the language of the Database Context.</param>
        /// <param name="parameters">The dictionary of parameters. Can be ignored if no parameters.</param>
        void ExecuteCommandWithNoResults(string queryString, Dictionary<string, object> parameters = null);

        /// <summary>
        /// Executes a stored procedure that alters data (insert / update / delete).
        /// </summary>
        /// <param name="storedProcedurename"></param>
        /// <param name="parameters"></param>
        void ExecuteStoredProcedure(string storedProcedurename, Dictionary<string, object> parameters = null);
        #endregion


        #region Table Helpers
        /// <summary>
        /// Returns a list with all the columns of a table, as retrieved from the database
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="tableSchema"></param>
        /// <returns></returns>
        IEnumerable<string> GetTableColumns(string tableName, string tableSchema = "dbo");
        #endregion


        #region Tools
        /// <summary>
        /// Ensures that the transaction and the connection to the database are open. Use this before any other commands.
        /// </summary>
        void EnsureReady();

        /// <summary>
        /// Ensures that the transaction and the connection to the database are open. Use this before any other commands.
        /// </summary>
        Task EnsureReadyAsync();

        /// <summary>
        /// Saves all the changes done in this repository, in this transaction.
        /// </summary>
        void PersistChanges();

        /// <summary>
        /// Saves all the changes done in this repository, in this transaction.
        /// </summary>
        Task PersistChangesAsync();

        /// <summary>
        /// Reverts all the changes made in this repository, in this transaction.
        /// </summary>
        void RevertChanges();

        /// <summary>
        /// Reverts all the changes made in this repository, in this transaction.
        /// </summary>
        Task RevertChangesAsync();

        /// <summary>
        /// Closes a connection if open and saved ongoing changes.
        /// </summary>
        void CloseConnection(bool saveChanges = true);

        /// <summary>
        /// Closes a connection if open and saved ongoing changes.
        /// </summary>
        Task CloseConnectionAsync(bool saveChanges = true);

        /// <summary>
        /// Enables the change detection of the inner mechanism. 
        /// Usually, with change detection enabled, the ORM will track entities more carefully but each operation will take longer.
        /// </summary>
        void EnableChangeDetection();

        /// <summary>
        /// Disables the change detection of the inner mechanism.
        /// Usually, with change detection disabled, the ORM does not track all changes, but the operations take less time (usefull when processing batches of data)
        /// </summary>
        void DisableChangeDetection();

        /// <summary>
        /// Returns the database context
        /// </summary>
        /// <returns></returns>
        T GetContext<T>() where T : DbContext;
        #endregion
    }
}
