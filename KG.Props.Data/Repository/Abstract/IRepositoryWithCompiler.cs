﻿using SqlKata.Compilers;

namespace KG.Props.Data.Repository.Abstract
{
    public interface IRepositoryWithCompiler: IRepository
    {

        /// <summary>
        /// Returns the compiler reference
        /// </summary>
        /// <returns></returns>
        Compiler GetCompiler();
    }
}
