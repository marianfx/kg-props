﻿namespace KG.Props.Storage.Basic.Abstract
{
    public interface IDataSaver
    {
        /// <summary>
        /// Sets parameters to be used in this
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters"></param>
        void SetParams<T>(T parameters);

        /// <summary>
        /// Reads the object of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters"></param>
        /// <returns></returns>
        T Read<T>();

        /// <summary>
        /// Writes the object of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters"></param>
        /// <param name="data"></param>
        void Write<T>(T data);
    }
}
