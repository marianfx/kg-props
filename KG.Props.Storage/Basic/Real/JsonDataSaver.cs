﻿using KG.Props.Storage.Basic.Abstract;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;

namespace KG.Props.Storage.Basic.Real
{
    public class JsonDataSaver : IDataSaver
    {
        public string FilePath { get; set; }
        private readonly ILogger _logger;

        public JsonDataSaver(ILogger logger)
        {
            _logger = logger;
        }

        public void SetParams<T>(T parameters)
        {
            FilePath = parameters as string;
        }

        public T Read<T>()
        {
            if (string.IsNullOrWhiteSpace(FilePath))
                throw new Exception("File path not specified");

            if (!File.Exists(FilePath))
                return default(T);

            var contents = File.ReadAllText(FilePath);
            try
            {
                return JsonConvert.DeserializeObject<T>(contents);
            }
            catch (Exception ex)
            {
                _logger.LogError("ERROR READING JSON: " + ex.Message);
                return default(T);
            }
        }

        public void Write<T>(T data)
        {
            if (string.IsNullOrWhiteSpace(FilePath))
                throw new Exception("File path not specified");

            if (data == null)
                throw new Exception("No data to save");

            if (File.Exists(FilePath))
                File.Delete(FilePath);

            try
            {
                var serialized = JsonConvert.SerializeObject(data);
                File.WriteAllText(FilePath, serialized);
            }
            catch (Exception ex)
            {
                _logger.LogError("ERROR WRITING JSON: " + ex.Message);
            }
        }
    }
}
