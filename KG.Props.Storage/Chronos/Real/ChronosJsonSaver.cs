﻿using KG.Props.Resources.Values;
using KG.Props.Storage.Chronos.Abstract;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace KG.Props.Storage.Chronos.Real
{
    public class ChronosJsonSaver : IChronosSaver
    {
        /// <summary>
        /// Expected structure: RootPath\DataHome\ParentDirectory\file.json
        /// </summary>
        public string RootPath { get; set; }

        /// <summary>
        /// Expected structure: RootPath\DataHome\ParentDirectory\file.json
        /// </summary>
        public string DataHome { get; set; } = Constants.DefaultDataHome;

        /// <summary>
        /// Expected structure: RootPath\DataHome\ParentDirectory\file.json
        /// </summary>
        public string ParentDirectory { get; set; } = Constants.DefaultSavedFilesDirectory;


        public ChronosJsonSaver(string rootPath)
        {
            RootPath = rootPath;
        }

        public ChronosJsonSaver(IHostingEnvironment hostingEnvironment)
        {
            RootPath = hostingEnvironment.ContentRootPath;
        }

        public bool DataExists(string guid)
        {
            var fileName = guid + ".json";
            var serverPath = Path.Combine(RootPath, DataHome, ParentDirectory, fileName);

            return DataExistsOnPath(serverPath);
        }

        private static bool DataExistsOnPath(string serverPath)
        {
            return File.Exists(serverPath);
        }

        public Task DeleteDataAsync(string guid)
        {
            var fileName = guid + ".json";
            var serverPath = Path.Combine(RootPath, DataHome, ParentDirectory, fileName);

            try
            {
                File.Delete(serverPath);
            }
            catch { }

            return Task.CompletedTask;
        }

        public Task<T> GetData<T>(string guid) where T : class
        {
            var fileName = guid + ".json";
            var serverPath = Path.Combine(RootPath, DataHome, ParentDirectory, fileName);

            if (!DataExistsOnPath(serverPath))
                return null;

            using (var asyncFileStream = new FileStream(serverPath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true))
            {
                using (var reader = new JsonTextReader(new StreamReader(asyncFileStream)))
                {
                    var serializer = new JsonSerializer
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        Formatting = Formatting.None,
                        PreserveReferencesHandling = PreserveReferencesHandling.All
                    };
                    var output = serializer.Deserialize<T>(reader);
                    return Task.FromResult(output);
                }
            }
        }

        public async Task SaveDataAsync(object response, string identifier)
        {
            var fileName = identifier + ".json";
            var serverPath = Path.Combine(RootPath, DataHome, ParentDirectory, fileName);

            if (File.Exists(serverPath))
                File.Delete(serverPath);

            // make sure data directory exists
            var dataFolderPath = Path.Combine(RootPath, DataHome);
            if (!Directory.Exists(dataFolderPath))
                Directory.CreateDirectory(dataFolderPath);

            // make sure parent directory exists
            var parentFolderPath = Path.Combine(RootPath, DataHome, ParentDirectory);
            if (!Directory.Exists(parentFolderPath))
                Directory.CreateDirectory(parentFolderPath);
            
            // write asynchronously to a file
            using (var asyncFileStream = new FileStream(serverPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write, 4096, true))
            {
                using (var writer = new JsonTextWriter(new StreamWriter(asyncFileStream)))
                {
                    var serializer = new JsonSerializer
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        Formatting = Formatting.None,
                        PreserveReferencesHandling = PreserveReferencesHandling.All
                    };
                    serializer.Serialize(writer, response);
                    await writer.FlushAsync();
                }
            }
        }
    }
}
