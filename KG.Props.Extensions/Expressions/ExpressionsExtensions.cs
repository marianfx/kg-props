﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KG.Props.Extensions.Expressions
{
    public static class ExpressionsExtensions
    {
        /// <summary>
        /// Builds an OR predicate between two exiting expressions
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="e1"></param>
        /// <param name="e2"></param>
        /// <returns></returns>
        public static Expression<Func<TEntity, bool>> PredicateOr<TEntity, T>(Expression<Func<TEntity, bool>> e1,
            Expression<Func<TEntity, bool>> e2)
        {
            return PredicateBuilder.New<TEntity>().Or(e1).Or(e2);
        }

        /// <summary>
        /// Builds an Expression that can be passed to .Where, FirstOrDefault etc (predicates, return bool)
        /// Returns True if the given value equals the named object property.
        /// Fully compatible with EF / Data providers / IQueriable. Useful when property names are dynamic
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Expression<Func<TEntity, bool>> PredicatePropertyEquals<TEntity, T>(T value, string propertyName = "Id")
        {
            var type = typeof(TEntity);

            // set constants
            var constant = Expression.Constant(value);
            var parameter = Expression.Parameter(type, "p");
            var valueGetter = Expression.Property(parameter, propertyName);

            // build expression => first check for property existance
            var propType = type.GetProperty(propertyName)?.PropertyType;
            if (propType == null)
                throw new Exception($"Property {propertyName} not found on object {typeof(TEntity).Name}.");

            // check if conversion needed
            Expression exprToUse;
            if (propType != typeof(T))
                exprToUse = Expression.Convert(constant, propType);
            else
                exprToUse = constant;

            var fullExpression = Expression.Equal(valueGetter, exprToUse);
            return Expression.Lambda<Func<TEntity, bool>>(fullExpression, parameter);
        }

        /// <summary>
        /// Based on the PropertyEquals Expression, filters a sequence of values (using Where) with dynamic property name.
        /// Fully compatible with EF / Data providers / IQueriable. Useful when property names are dynamic
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> PropertyEquals<TEntity, T>(this IQueryable<TEntity> source, T value,
            string propertyName = "Id")
        {
            var expressionInLambda = PredicatePropertyEquals<TEntity, T>(value, propertyName);
            var where = Expression.Call(typeof(Queryable), "Where", new[] { source.ElementType }, source.Expression, expressionInLambda);
            return source.Provider.CreateQuery<TEntity>(where);
        }

        /// <summary>
        /// Builds an Expression that can be passed to anything LINQ/like (Where, OrderBy, GroupBy etc) that builds a member access dynamically
        /// (eg p => p.Id), wiith "Id" given as string param
        /// Fully compatible with EF / Data providers / IQueriable. Useful when property names are dynamic
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Expression<Func<TEntity, T>> PredicateMemberAccess<TEntity, T>(string propertyName = "Id")
        {
            var type = typeof(TEntity);
            var property = type.GetProperty(propertyName);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            return Expression.Lambda<Func<TEntity, T>>(propertyAccess, parameter);
        }


        // https://stackoverflow.com/questions/7265186/how-do-i-specify-the-linq-orderby-argument-dynamically
        /// <summary>
        /// Builds .OrderBy or .OrderByDescending (x => x.Prop), with a STRING property name accessor
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="orderByProperty"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string orderByProperty,
                          bool desc = false)
        {
            var command = desc ? "OrderByDescending" : "OrderBy";
            var type = typeof(TEntity);
            var property = type.GetProperty(orderByProperty);
            if (property == null)
                throw new Exception("Invalid Order By property name '" + orderByProperty + "'.");

            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new[] { type, property.PropertyType },
                                          source.Expression, Expression.Quote(orderByExpression));
            return source.Provider.CreateQuery<TEntity>(resultExpression);
        }

        // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/expression-trees/how-to-use-expression-trees-to-build-dynamic-queries
        public static Expression<Func<TEntity, bool>> PredicateContainsLowered<TEntity, T>(Expression<Func<TEntity, T>> selector, string value)
        {
            // get prerequisites
            var valueLowered = value?.ToLower().Trim();
            var lowerMethod = typeof(string).GetMethods().First(m => m.Name == "ToLower" && m.GetParameters().Length == 0);
            var trimMethod = typeof(string).GetMethods().First(m => m.Name == "Trim" && m.GetParameters().Length == 0);
            var containsMethod = typeof(string).GetMethods().First(m => m.Name == "Contains" && m.GetParameters().Length == 1);

            // set constants
            var constant = Expression.Constant(valueLowered);
            var nullConst = Expression.Constant(null);
            var memberExpression = (MemberExpression)selector.Body;
            var parameterExpression = Expression.Parameter(typeof(TEntity), "p");
            var valueGetter = Expression.Property(parameterExpression,
                typeof(TEntity).GetProperty(memberExpression.Member.Name));

            // build expression x.Description != null && description != null
            var exprNotNull1 = Expression.NotEqual(constant, nullConst);
            var exprNotNull2 = Expression.NotEqual(valueGetter, nullConst);
            var exprNotNull = Expression.AndAlso(exprNotNull1, exprNotNull2);

            // build expression x.Description.ToLower().Trim().Contains(valueLowered)
            var expressionLower = Expression.Call(valueGetter, lowerMethod);
            var expressionTrim = Expression.Call(expressionLower, trimMethod);
            var expressionContains = Expression.Call(expressionTrim, containsMethod, constant);

            // build final expression
            var expressionFull = Expression.AndAlso(exprNotNull, expressionContains);
            return Expression.Lambda<Func<TEntity, bool>>(expressionFull, parameterExpression);
        }

        /// <summary>
        /// Builds .Where(x.Description != null && description != null && x.Description.Lower().Trim().Contains(description.ToLowere().Trim())
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> ContainsLowered<TEntity, T>(this IQueryable<TEntity> source, Expression<Func<TEntity, T>> selector, string value)
        {
            var expressionInLambda = PredicateContainsLowered(selector, value);
            var where = Expression.Call(typeof(Queryable), "Where", new[] { source.ElementType }, source.Expression, expressionInLambda);
            return source.Provider.CreateQuery<TEntity>(where);
        }

        #region Specifics for Dates
        /// <summary>
        /// Creates a predicate function that checks if the given StartDate and EndDate properties of an object are valid at a given date.
        /// The output is a predicate. This predicate will filter only elements that are 'Valid' at the given date.
        /// The rule: StartDate <= date && (EndDate is null || EndDate > date)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="date"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> FilterByValidDate<T>(DateTime? date, string startDatePropName = "StartDate", string endDatePropName = "EndDate")
        {
            var parameter = Expression.Parameter(typeof(T), "x");
            var member1 = Expression.Property(parameter, startDatePropName);
            var member2 = Expression.Property(parameter, endDatePropName);
            var dateConst = Expression.Constant(date, typeof(DateTime?));
            var dateNotNull = Expression.Constant(date, typeof(DateTime));
            var nullConst = Expression.Constant(null);
            var startDateLower = Expression.GreaterThanOrEqual(dateNotNull, member1);
            var equalNull = Expression.Equal(member2, nullConst);
            var endDateGreater = Expression.GreaterThan(member2, dateConst);
            var innerRight = Expression.OrElse(equalNull, endDateGreater);
            var fullOp = Expression.AndAlso(startDateLower, innerRight);
            return Expression.Lambda<Func<T, bool>>(fullOp, parameter);
        }


        /// <summary>
        /// On the given object, applies a dynamic lambda operation that checks if it is valid. Uses the predicate method.
        /// x => x.StartDate le date AND (x.EndDate = null OR x.EndDate > date)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static IEnumerable<T> FilterByValidDate<T>(this IQueryable<T> obj, DateTime date)
        {
            return obj.Where(FilterByValidDate<T>(date));
        }
        #endregion
    }
}
