﻿using KG.Props.Extensions.Strings;
using System;
using System.Collections.Generic;

namespace KG.Props.Extensions.Excel
{
    public static class ExcelGeneralExtensions
    {
        /// <summary>
        /// Replaces a the 'name' that should be assigned to an Excel sheet with proper chars (removes unwanted chars) and up to 'maxChars' length (usually 31).
        /// If the array 'usedNames' is specified, the method also generates a name (using guid parts attached at the end) that does not exist in the workbook.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="usedNames"></param>
        /// <param name="maxChars"></param>
        /// <returns></returns>
        public static string ToSheetNameFormat(this string name, IList<string> usedNames = null, int maxChars = 31)
        {
            var nameLength = maxChars;
            do
            {
                var length = Math.Min(name.Length, nameLength);
                var guid = Guid.NewGuid().ToString();
                var portfolioNamePart = name.Substring(0, length).ChrTran("_");
                var guidNamePart = ("_" + guid).Substring(0, maxChars - nameLength);
                name = portfolioNamePart + guidNamePart;
                nameLength--;
            } while (nameLength > 0 && (usedNames?.Contains(name) ?? false));

            return name;
        }
    }
}
