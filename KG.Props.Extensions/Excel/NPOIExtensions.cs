﻿using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;

namespace KG.Props.Extensions.Excel
{
    public static class NPOIExtensions
    {
        /// <summary>
        /// Simpler accessor for cell by row and col for NPOI ISheet.
        /// This uses 1-based indexes
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowNum"></param>
        /// <param name="colNum"></param>
        /// <returns></returns>
        public static ICell Cells(this ISheet sheet, int rowNum, int colNum)
        {
            if (rowNum < 1)
                throw new Exception($"Invalid excel row number accessed {rowNum}");
            if (colNum < 1)
                throw new Exception($"Invalid excel col number accessed {colNum}");

            var actualRowNum = rowNum - 1;
            var actualCollNum = colNum - 1;

            var row = sheet.GetRow(actualRowNum) ?? sheet.CreateRow(actualRowNum);
            var cell = row.GetCell(actualCollNum) ?? row.CreateCell(actualCollNum);
            return cell;
        }

        /// <summary>
        /// Copies a row at the position {rowId} in the {sheet}, {count} times.
        /// By default, if {whereToCopy} is not specifies, it inserts the row at the same position as {rowId}. This works ok when just wanting to replicate rows. If wanting to explicitly copy a row at a given row index, specify the {whereToCopy} parameter.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="count"></param>
        /// <param name="rowId"></param>
        /// <param name="whereToCopy"></param>
        /// <param name="indexesAre1Based"></param>
        public static void MultiplyRow(this ISheet sheet, int count, int rowId, int? whereToCopy = null, bool indexesAre1Based = true)
        {
            var whereToCopyFrom = rowId;
            var whereToCopyTo = whereToCopy ?? rowId + 1;

            // treat 1-based indexation
            if (indexesAre1Based)
            {
                whereToCopyFrom--;
                whereToCopyTo--;
            }

            //var existingRow = sheet.GetRow(whereToCopyTo);
            //if (existingRow == null)
            //    sheet.CreateRow(whereToCopyTo);

            for (var i = 0; i < count; i++)
            {
                sheet.CopyRowRemastered(whereToCopyFrom, whereToCopyTo + i);
            }
        }

        /// <summary>
        /// This is copied and modified from the official repo of NPOI.
        /// Fixes NULL access when copying ranges.
        /// Fixes newrow index not being kept to the same value after rows are shiftet (references the next row)
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="sourceRowIndex"></param>
        /// <param name="targetRowIndex"></param>
        /// <returns></returns>
        public static IRow CopyRowRemastered(this ISheet sheet, int sourceRowIndex, int targetRowIndex)
        {
            if (sourceRowIndex == targetRowIndex)
                throw new ArgumentException("sourceIndex and targetIndex cannot be same");

            // Get the source / new row
            IRow newRow = sheet.GetRow(targetRowIndex);
            IRow sourceRow = sheet.GetRow(sourceRowIndex);

            // If the row exist in destination, push down all rows by 1 else create a new row
            if (newRow != null)
            {
                sheet.ShiftRows(targetRowIndex, sheet.LastRowNum, 1);
            }

            // FIX: re-add the row reference after rows are shifted
            newRow = sheet.GetRow(targetRowIndex) ?? sheet.CreateRow(targetRowIndex);

            // Loop through source columns to add to new row
            for (int i = sourceRow.FirstCellNum; i < sourceRow.LastCellNum; i++)
            {
                // Grab a copy of the old/new cell
                ICell oldCell = sourceRow.GetCell(i);

                // If the old cell is null jump to next cell
                if (oldCell == null)
                {
                    continue;
                }
                ICell newCell = newRow.CreateCell(i);

                if (oldCell.CellStyle != null)
                {
                    // apply style from old cell to new cell 
                    newCell.CellStyle = oldCell.CellStyle;
                }

                // If there is a cell comment, copy
                if (oldCell.CellComment != null)
                {
                    newCell.CellComment = oldCell.CellComment;
                }

                // If there is a cell hyperlink, copy
                if (oldCell.Hyperlink != null)
                {
                    newCell.Hyperlink = oldCell.Hyperlink;
                }

                // Set the cell data type
                newCell.SetCellType(oldCell.CellType);

                // Set the cell data value
                switch (oldCell.CellType)
                {
                    case CellType.Blank:
                        newCell.SetCellValue(oldCell.StringCellValue);
                        break;
                    case CellType.Boolean:
                        newCell.SetCellValue(oldCell.BooleanCellValue);
                        break;
                    case CellType.Error:
                        newCell.SetCellErrorValue(oldCell.ErrorCellValue);
                        break;
                    case CellType.Formula:
                        newCell.SetCellFormula(oldCell.CellFormula);
                        break;
                    case CellType.Numeric:
                        newCell.SetCellValue(oldCell.NumericCellValue);
                        break;
                    case CellType.String:
                        newCell.SetCellValue(oldCell.RichStringCellValue);
                        break;
                }
            }

            // If there are are any merged regions in the source row, copy to new row
            for (int i = 0; i < sheet.NumMergedRegions; i++)
            {
                CellRangeAddress cellRangeAddress = sheet.GetMergedRegion(i); // FIX: this sometimes can be null => do null check
                if (cellRangeAddress != null && cellRangeAddress.FirstRow == sourceRow.RowNum)
                {
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.RowNum,
                            (newRow.RowNum +
                                    (cellRangeAddress.LastRow - cellRangeAddress.FirstRow
                                            )),
                            cellRangeAddress.FirstColumn,
                            cellRangeAddress.LastColumn);
                    sheet.AddMergedRegion(newCellRangeAddress);
                }
            }
            return newRow;
        }

        #region Range and formulas variations for EXCEL

        /// <summary>
        /// Returns the given range (identified by column names (C,D) and row numbers (1,2))
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="column1Inclusive"></param>
        /// <param name="column2Inclusive"></param>
        /// <param name="rowFromInclusive"></param>
        /// <param name="rowToInclusive"></param>
        /// <returns></returns>
        public static CellRangeAddress GetCellRange(this ISheet sheet, string column1Inclusive, string column2Inclusive, int rowFromInclusive, int rowToInclusive)
        {
            string range = string.Format("{0}{2}:{1}{3}", column1Inclusive, column2Inclusive, rowFromInclusive, rowToInclusive);
            return CellRangeAddress.ValueOf(range);
        }

        /// <summary>
        /// Returns the given range (identified by column ids and row numbers)
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="column1Inclusive"></param>
        /// <param name="column2Inclusive"></param>
        /// <param name="rowFromInclusive"></param>
        /// <param name="rowToInclusive"></param>
        /// <returns></returns>
        public static CellRangeAddress GetCellRange(this ISheet sheet, int column1Inclusive, int column2Inclusive, int rowFromInclusive, int rowToInclusive)
        {
            string colString1 = CellReference.ConvertNumToColString(column1Inclusive);
            string colString2 = CellReference.ConvertNumToColString(column2Inclusive);
            return sheet.GetCellRange(colString1, colString2, rowFromInclusive, rowToInclusive);
        }

        /// <summary>
        /// Returns a single cell from the document, as RANGE.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CellRangeAddress GetCellAsRange(this ISheet sheet, string column, int row)
        {
            return sheet.GetCellRange(column, column, row, row);
        }

        /// <summary>
        /// Returns a single cell from the document, as RANGE.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static CellRangeAddress GetCellAsRange(this ISheet sheet, int column, int row)
        {
            return sheet.GetCellRange(column, column, row, row);
        }

        /// <summary>
        /// Sets the given formula at the given cell.
        /// </summary>
        /// <param name="xlSheet"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <param name="formula"></param>
        public static void SetFormula(this ISheet xlSheet, string formula, string column, int row)
        {
            var range = xlSheet.GetCellAsRange(column, row);
            xlSheet.SetArrayFormula(formula, range);
        }

        /// <summary>
        /// Sets the given formula at the given cell.
        /// </summary>
        /// <param name="xlSheet"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <param name="formula"></param>
        public static void SetFormula(this ISheet xlSheet, string formula, int column, int row)
        {
            var range = xlSheet.GetCellAsRange(column, row);
            xlSheet.SetArrayFormula(formula, range);
        }
        #endregion


        /// <summary>
        /// Sets, for the given range, the format for borders.
        /// </summary>
        /// <param name="xlSheet"></param>
        /// <param name="cStart"></param>
        /// <param name="cEnd"></param>
        /// <param name="rStart"></param>
        /// <param name="rEnd"></param>
        /// <param name="borderStyle"></param>
        /// <param name="lineStyle"></param>
        public static void FormatBorders(this ISheet xlSheet, string cStart, string cEnd, int rStart, int rEnd, BorderStyle borderStyle = BorderStyle.Thin, LineStyle lineStyle = LineStyle.Solid)
        {
            var range = xlSheet.GetCellRange(cStart, cEnd, rStart, rEnd);
            RegionUtil.SetBorderTop((int)borderStyle, range, xlSheet, xlSheet.Workbook);
            RegionUtil.SetBorderBottom((int)borderStyle, range, xlSheet, xlSheet.Workbook);
            RegionUtil.SetBorderLeft((int)borderStyle, range, xlSheet, xlSheet.Workbook);
            RegionUtil.SetBorderRight((int)borderStyle, range, xlSheet, xlSheet.Workbook);
        }

        /// <summary>
        /// Sets, for the given range, the format for borders.
        /// THE ROW INDICES ARE 1-BASED by default
        /// </summary>
        /// <param name="xlSheet"></param>
        /// <param name="cStart"></param>
        /// <param name="cEnd"></param>
        /// <param name="rStart"></param>
        /// <param name="rEnd"></param>
        /// <param name="borderStyle"></param>
        /// <param name="lineStyle"></param>
        /// <param name="oneBased"></param>
        public static void FormatBorders(this ISheet xlSheet, int cStart, int cEnd, int rStart, int rEnd, BorderStyle borderStyle = BorderStyle.Thin, LineStyle lineStyle = LineStyle.Solid, bool oneBased = true)
        {
            if (oneBased)
            {
                cStart--;
                cEnd--;
                rStart--;
                rEnd--;
            }

            string colString1 = CellReference.ConvertNumToColString(cStart);
            string colString2 = CellReference.ConvertNumToColString(cEnd);
            xlSheet.FormatBorders(colString1, colString2, rStart, rEnd, borderStyle, lineStyle);
        }
    }
}
