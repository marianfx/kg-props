﻿using KG.Props.Resources.Values;
using OfficeOpenXml;
using OfficeOpenXml.ConditionalFormatting;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;

namespace KG.Props.Extensions.Excel
{
    public static class EPPLUSExtensions
    {
        /// <summary>
        /// Copies a row at the position {rowId} in the {sheet}, {count} times.
        /// By default, if {whereToCopy} is not specifies, it inserts the row at the same position as {rowId}. This works ok when just wanting to replicate rows. If wanting to explicitly copy a row at a given row index, specify the {whereToCopy} parameter.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="count"></param>
        /// <param name="rowId"></param>
        /// <param name="whereToCopy"></param>
        /// <param name="removeBorders">todo: describe removeBorders parameter on MultiplyRow</param>
        public static void MultiplyRow(this ExcelWorksheet sheet, int count, int rowId, int? whereToCopy = null, bool removeBorders = false, int rowRange = 1)
        {
            if (count <= 0)
                return;

            var whereToCopyFrom = rowId;
            var whereToCopyFromEnd = whereToCopyFrom + rowRange - 1;
            var whereToCopyTo = whereToCopy ?? rowId + rowRange;
            var whereToCopyEnd = whereToCopyTo + rowRange - 1;

            var minCol = ExcelAddress.GetAddressCol(1);
            var maxCol = ExcelAddress.GetAddressCol(sheet.Dimension.End.Column);

            var startRange = sheet.Cells[string.Format("{2}{0}:{3}{1}", whereToCopyFrom, whereToCopyFromEnd, minCol, maxCol)];
            var howManyRows = rowRange * count;
            sheet.InsertRow(whereToCopyTo, howManyRows);

            for (var i = 0; i < count; i++)
            {
                var endRange = sheet.Cells[string.Format("{2}{0}:{3}{1}", whereToCopyTo + i * rowRange, whereToCopyEnd + i * rowRange, minCol, maxCol)];
                startRange.Copy(endRange);
            }

            if (removeBorders)
                sheet.ClearInnerBorders(count, whereToCopyFrom, rowRange);
        }

        /// <summary>
        /// Clears the inner borders of all cells from a range, leaving be just the outer ones.
        /// Treats the first row, keeping top border and last row -> keeping the bottom one.
        /// Does not treat left + right borders.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="count"></param>
        /// <param name="startRow"></param>
        /// <param name="rowRange"></param>
        /// <param name="clearStyle"></param>
        public static void ClearInnerBorders(this ExcelWorksheet sheet, int count, int startRow, int rowRange = 1, ExcelBorderStyle clearStyle = ExcelBorderStyle.None)
        {
            var minCol = ExcelAddress.GetAddressCol(1);
            var maxCol = ExcelAddress.GetAddressCol(sheet.Dimension.End.Column);
            var startRange = sheet.Cells[string.Format("{2}{0}:{3}{1}", startRow, startRow + rowRange - 1, minCol, maxCol)];

            var whereToCopyTo = startRow + rowRange;
            var whereToCopyEnd = whereToCopyTo + rowRange - 1;

            for (var i = 0; i < count; i++)
            {
                var endRange = sheet.Cells[string.Format("{2}{0}:{3}{1}", whereToCopyTo + i * rowRange, whereToCopyEnd + i * rowRange, minCol, maxCol)];

                for (var row = endRange.Start.Row; row <= endRange.End.Row; row++)
                    for (var col = endRange.Start.Column; col <= endRange.End.Column; col++)
                    {
                        var cell = sheet.Cells[row, col];
                        cell.Style.Border.DiagonalUp = cell.Style.Border.DiagonalDown = false;
                        cell.Style.Border.Diagonal.Style = clearStyle;
                        cell.Style.Border.Top.Style = clearStyle;
                        if (i != count - 1) // don't remove bottom border of last row
                            cell.Style.Border.Bottom.Style = clearStyle;
                    }
            }

            // remove bottom border of first row if it has any + if there are multiple
            if (count > 0)
            {
                for (var row = startRange.Start.Row; row <= startRange.End.Row; row++)
                    for (var col = startRange.Start.Column; col <= startRange.End.Column; col++)
                    {
                        var cell = sheet.Cells[row, col];
                        cell.Style.Border.DiagonalUp = cell.Style.Border.DiagonalDown = false;
                        cell.Style.Border.Diagonal.Style = clearStyle;
                        cell.Style.Border.Bottom.Style = clearStyle;
                    }
            }
        }


        /// <summary>
        /// Auto fits columns while maintaining a minWidth.
        /// Uses a max of 100 columns by default. Specify if there are more.
        /// </summary>
        /// <param name="sheet"></param>
        public static void AutoFitAllColumnsWithMinWidth(this ExcelWorksheet sheet, int? maxColumn = 100)
        {
            var end = maxColumn.HasValue ? (Math.Min(maxColumn.Value, sheet.Dimension.End.Column)) : sheet.Dimension.End.Column;

            for (var col = sheet.Dimension.Start.Column; col <= end; col++)
            {
                sheet.Column(col).AutoFit(sheet.Column(col).Width);
            }
        }

        /// <summary>
        /// Sets the borders of all cells (per each cell) to a specific border type, differentiating left + right from top + bottom
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="range"></param>
        /// <param name="borderBottomTop"></param>
        public static void SetAllCellsBorder(this ExcelWorksheet sheet, ExcelRange range,
            ExcelBorderStyle borderBottomTop = ExcelBorderStyle.Thin, ExcelBorderStyle borderLeftRight = ExcelBorderStyle.Thin)
        {
            var minEndColl = Math.Min(range.End.Column, sheet.Dimension.End.Column);
            for (var row = range.Start.Row; row <= range.End.Row; row++)
                for (var col = range.Start.Column; col <= minEndColl; col++)
                {
                    var cell = sheet.Cells[row, col];
                    cell.Style.Border.Diagonal.Style = borderLeftRight;
                    cell.Style.Border.Left.Style = borderLeftRight;
                    cell.Style.Border.Right.Style = borderLeftRight;
                    cell.Style.Border.Top.Style = borderBottomTop;
                    cell.Style.Border.Bottom.Style = borderBottomTop;
                }
        }

        /// <summary>
        /// Applies a formatting to columns
        /// </summary>
        /// <param name="thisSheet">The sheet</param>
        /// <param name="startRow">The start row</param>
        /// <param name="startCol">The start col</param>
        /// <param name="colStep">The step (number of column between intervals)</param>
        /// <param name="rowCount">The number of rows to apply this to</param>
        /// <param name="colCount">The number of intervals to apply to</param>
        /// <param name="formatIndex">The index of  the conditional formatting (from sheet.ConditionalFormatting array) of which address to modify</param>
        public static void CopyConditionalFormattingAtIndexToRange(this ExcelWorksheet thisSheet, int startRow, int startCol, int colStep, int rowCount, int colCount, int formatIndex = 0)
        {
            // see if index of format is available
            if (!(thisSheet.ConditionalFormatting.Count > formatIndex && formatIndex >= 0))
                return;

            // create full range desired
            var rangeList = thisSheet.GetRangeList(startRow, startCol, colStep, rowCount, colCount);
            thisSheet.CopyConditionalFormattingToRangeList(formatIndex, rangeList.ToArray());
        }

        /// <summary>
        /// First searchs for Conditional Formatting that matched Cell.Equals = {{formatTextToMatch}} and if found, copies that format to the given range
        /// </summary>
        /// <param name="thisSheet"></param>
        /// <param name="startRow"></param>
        /// <param name="startCol"></param>
        /// <param name="colStep"></param>
        /// <param name="rowCount"></param>
        /// <param name="colCount"></param>
        /// <param name="formatTextToMatch"></param>
        public static void CopyConditionalFormattingAtIndexToRange(this ExcelWorksheet thisSheet, int startRow,
            int startCol, int colStep, int rowCount, int colCount, string formatTextToMatch)
        {
            var formatIndex = thisSheet.ConditionalFormatting.OfType<ExcelConditionalFormattingEqual>()
                .ToList().FindIndex(x => x.Formula == "\"" + formatTextToMatch + "\"");

            thisSheet.CopyConditionalFormattingAtIndexToRange(startRow, startCol, colStep, rowCount, colCount, formatIndex);
        }

        /// <summary>
        /// Applies the conditional formatting from the index of 'formatIndex' to all the cells in the specified range list
        /// </summary>
        /// <param name="thisSheet"></param>
        /// <param name="formatIndex"></param>
        /// <param name="ranges"></param>
        public static void CopyConditionalFormattingToRangeList(this ExcelWorksheet thisSheet, int formatIndex = 0, params ExcelRange[] ranges)
        {
            // see if index of format is available
            if (!(thisSheet.ConditionalFormatting.Count > formatIndex))
                return;

            if (ranges.Length <= 0) return;

            var rangeString = ranges[0].Address;
            for (var i = 1; i < ranges.Length; i++)
            {
                var rangeH = ranges[i];
                rangeString += "," + rangeH.Address;
            }

            // set new address
            thisSheet.ConditionalFormatting[formatIndex].Address = thisSheet.Cells[rangeString];
        }

        /// <summary>
        /// First searchs for Conditional Formatting that matched Cell.Equals = {{formatTextToMatch}} and if found, copies that format to the given range
        /// </summary>
        /// <param name="thisSheet"></param>
        /// <param name="startRow"></param>
        /// <param name="startCol"></param>
        /// <param name="colStep"></param>
        /// <param name="rowCount"></param>
        /// <param name="colCount"></param>
        public static void ApplyAllUniqueConditionalFormattingFromSheetToRange(this ExcelWorksheet thisSheet, int startRow,
            int startCol, int colStep, int rowCount, int colCount)
        {
            var allUnique = thisSheet.ConditionalFormatting.OfType<ExcelConditionalFormattingEqual>()
                .Select(x => x.Formula).Distinct().ToList();

            var conditionalsAsList = thisSheet.ConditionalFormatting.OfType<ExcelConditionalFormattingEqual>().ToList();
            foreach (var uniqueTextTomatch in allUnique)
            {
                var formatIndex = conditionalsAsList.FindIndex(x => x.Formula == uniqueTextTomatch);
                thisSheet.CopyConditionalFormattingAtIndexToRange(startRow, startCol, colStep, rowCount, colCount, formatIndex);
            }
        }


        /// <summary>
        /// Builds a list of cell ranges from a repetitive loop of columns described by (startrow, startcol), stepCol, colCount
        /// </summary>
        /// <param name="thisSheet"></param>
        /// <param name="startRow"></param>
        /// <param name="startCol"></param>
        /// <param name="colStep"></param>
        /// <param name="rowCount"></param>
        /// <param name="colCount"></param>
        /// <returns></returns>
        public static List<ExcelRange> GetRangeList(this ExcelWorksheet thisSheet, int startRow,
            int startCol, int colStep, int rowCount, int colCount)
        {
            // create full range desired
            var range = thisSheet.Cells[startRow, startCol, Math.Max(startRow, startRow + rowCount - 1), startCol];
            var rangeList = new List<ExcelRange> { range };

            for (var i = 1; i < colCount; i++)
            {
                var rangeH = thisSheet.Cells[startRow, startCol + (colStep * i), Math.Max(startRow, startRow + rowCount - 1), startCol + (colStep * i)];
                rangeList.Add(rangeH);
            }

            return rangeList;
        }

        /// <summary>
        /// Shifts the ranges of the graph with the given limits.
        /// </summary>
        /// <typeparam name="T">The type of the graph, for example ExcelBarChart</typeparam>
        /// <param name="sheet">The sheet containing the graph</param>
        /// <param name="chartname">The name of the graph (to identify it)</param>
        /// <param name="serieIndex">The index of the serie to modify. Remember that the graph can have multiple series of data. Graphs with single serie will have only index 0.</param>
        /// <param name="shift">How much to shift reference rows</param>
        /// <param name="refSheet">The sheet that the graph references. If not specified, defaults to self sheet</param>
        /// <param name="hasXSeries">Specify if this series has an X-Series component (the x-series are usually the titles of data) and if it needs to be modified</param>
        /// <param name="shiftTitle">How much to shift title reference rows (valid only if graph header references cells)</param>
        /// <param name="shiftXSeries">How much to shift the x-series reference rows (valid only if hasXSeries is true)</param>
        /// <paramref name="newRowCount">Custom specify a new row length for the series (added over shift)</param>
        public static void ShiftGraphRange<T>(this ExcelWorksheet sheet, string chartname, int serieIndex, int shift, ExcelWorksheet refSheet = null, bool hasXSeries = false, int? shiftTitle = null, int? shiftXSeries = null, int? newRowCount = null)
            where T : ExcelChart
        {
            var graph = sheet.Drawings.OfType<T>().FirstOrDefault(x => x.Name == chartname);
            if (graph == null) return;

            refSheet = refSheet ?? sheet;
            var titleShift = shiftTitle ?? shift;
            var xShift = shiftXSeries ?? shift;

            // max them by 0
            shift = Math.Max(shift, 0);
            titleShift = Math.Max(titleShift, 0);
            xShift = Math.Max(xShift, 0);

            var addrHeader = graph.Series[serieIndex].HeaderAddress;
            graph.Series[serieIndex].HeaderAddress =
                refSheet.Cells[addrHeader.Start.Row + titleShift, addrHeader.Start.Column,
                    addrHeader.End.Row + titleShift, addrHeader.End.Column];

            var seriesAddr = refSheet.Cells[graph.Series[serieIndex].Series];
            var endRow = newRowCount == null
                ? seriesAddr.End.Row + shift
                : seriesAddr.Start.Row + shift + newRowCount.Value;
            var newRange = refSheet.Cells[seriesAddr.Start.Row + shift, seriesAddr.Start.Column,
                endRow, seriesAddr.End.Column];

            graph.Series[serieIndex].Series = newRange.FullAddressAbsolute;

            if (hasXSeries)
            {
                seriesAddr = refSheet.Cells[graph.Series[serieIndex].XSeries];
                endRow = newRowCount == null
                    ? seriesAddr.End.Row + xShift
                    : seriesAddr.Start.Row + xShift + newRowCount.Value;
                newRange = refSheet.Cells[seriesAddr.Start.Row + xShift, seriesAddr.Start.Column,
                    endRow, seriesAddr.End.Column];
                graph.Series[serieIndex].XSeries = newRange.FullAddressAbsolute;
            }
        }


        /// <summary>
        /// Removes borders (diagonals and left + right + bottom) from a range of cells.
        /// </summary>
        /// <param name="range"></param>
        public static void RemoveBorders(this ExcelRange range)
        {
            range.Style.Border.DiagonalDown = false;
            range.Style.Border.DiagonalUp = false;
            //range.Style.Border.Left.Style =  ExcelBorderStyle.Thin;
            //range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            //range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
        }

        /// <summary>
        /// Adjust special column width according to the contained text(needs array) and estimated width per letter
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="column"></param>
        /// <param name="rowText"></param>
        /// <param name="widthPerLetter"></param>
        public static void AdjustWidthOfColumnToContext(this ExcelWorksheet sheet, string column, IEnumerable<string> rowText,
            double widthPerLetter = 1.4)
        {
            if (!rowText.Any())
                return;

            var maxText = rowText.Max(x => x.Length);
            var upperLimit = (int)Math.Ceiling(maxText * widthPerLetter);
            sheet.Cells[$"{column}:{column}"].AutoFitColumns(upperLimit);
        }


        /// <summary>
        /// Seths the headers of a sheet, according to the parameters
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="columns"></param>
        /// <param name="startRow"></param>
        /// <param name="startCol"></param>
        /// <param name="stepCol"></param>
        /// <param name="doMerge"></param>
        /// <param name="doBigHeader"></param>
        public static void SetSheetHeaders(this ExcelWorksheet sheet, IList<String> columns, int startRow, int startCol, int stepCol, bool doMerge = false, bool doBigHeader = false)
        {
            // add header
            for (var i = 0; i < columns.Count; i++)
            {
                sheet.Cells[startRow, i * stepCol + startCol].Value = columns[i];

                if (doMerge && stepCol > 1)
                {
                    var r = sheet.Cells[startRow, i * stepCol + startCol, startRow, (i * stepCol + startCol) + stepCol - 1];
                    r.Merge = true;
                }
            }

            // format
            var rFull = sheet.Cells[startRow, startCol, startRow, columns.Count * stepCol + startCol - 1];
            if (doBigHeader)
            {
                sheet.Row(startRow).Height = 31.5;
                rFull.Style.WrapText = true;
                rFull.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.CenterContinuous;
                rFull.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            }

            if (columns.Count != 0)
            {
                rFull.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                rFull.Style.Font.Bold = true;
                rFull.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            }

            // sheet.SetBoxBorder(startRow, startCol, startRow, columns.Count * stepCol);
        }

        /// <summary>
        /// Transposes content from a datatable to the current sheet
        /// </summary>
        /// <param name="xlSheet"></param>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="useBottomBorder"></param>
        /// <param name="useBoxBorder"></param>
        public static void SetSheetContent(this ExcelWorksheet xlSheet, DataTable dt, IList<string> format, int row, int col, bool useBottomBorder, bool useBoxBorder = false)
        {
            // Inserisco i valori
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (dt.Rows[i][j] is double)
                    {
                        var val = (double)dt.Rows[i][j];

                        if (double.IsNaN(val))
                            xlSheet.Cells[i + row, j + col].Value = string.Empty;
                        else
                            xlSheet.Cells[i + row, j + col].Value = val;

                        // Setto il formato
                        var r = xlSheet.Cells[i + row, j + col];
                        if (format == null) continue;

                        if (Math.Abs(val) >= 0.00001)
                        {
                            if (format.Count <= j) continue;

                            var fieldFormat = format[j]; // Estraggo il formato
                            r.Style.Numberformat.Format = fieldFormat;
                        }
                        else if (Math.Abs(val) > 0 & Math.Abs(val) < 0.00001)
                            r.Style.Numberformat.Format = "#0.00E+00";
                        else
                            r.Style.Numberformat.Format = "_-* #,##0_-;-* #,##0_-;_-* \"-\"??_-;_-@_-";
                    }
                    else
                    {
                        var fieldFormat = format != null && format.Count > j ? format[j] : ""; // Estraggo il formato
                        ExcelRange target;

                        if (fieldFormat.Equals("FORMULAR1C1"))
                        {
                            target = xlSheet.Cells[i + row, j + col];
                            target.FormulaR1C1 = (string)dt.Rows[i][j];
                        }
                        else // normal values
                        {
                            var isNull = dt.Rows[i][j] == null || dt.Rows[i][j] == DBNull.Value;
                            var value = isNull ? "" : dt.Rows[i][j];

                            // check datetime
                            if (!isNull && dt.Rows[i][j] is DateTime)
                            {
                                xlSheet.Cells[i + row, j + col].Style.Numberformat.Format = Constants.DefaultDisplayDateFormat;
                                xlSheet.Cells[i + row, j + col].Value = (DateTime)dt.Rows[i][j];
                            }
                            else
                                xlSheet.Cells[i + row, j + col].Value = value;

                            // Setto il formato
                            target = xlSheet.Cells[i + row, j + col];

                            if (!string.IsNullOrWhiteSpace(fieldFormat))
                                target.Style.Numberformat.Format = fieldFormat;
                        }

                        target.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    }
                }
            }

            var rows = dt.Rows.Count > 0 ? dt.Rows.Count : 1;
            var columns = dt.Columns.Count > 0 ? dt.Columns.Count : 1;
            // Formatto i bordi
            if (useBottomBorder)
            {
                var r = xlSheet.Cells[rows + row - 1, col, rows + row - 1, columns + col - 1];
                r.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            }

            // Formatto tutti i bordi
            if (useBoxBorder)
                xlSheet.SetBoxBorder(row, col, rows, columns);
        }


        /// <summary>
        /// Borders the range with medium continous line
        /// </summary>
        /// <param name="xlSheet"></param>
        /// <param name="startRow"></param>
        /// <param name="startCol"></param>
        /// <param name="nRows"></param>
        /// <param name="nCols"></param>
        public static void SetBoxBorder(this ExcelWorksheet xlSheet, int startRow, int startCol, int nRows, int nCols)
        {
            var range = xlSheet.Cells[startRow, startCol, Math.Max(startRow, startRow + nRows - 1), Math.Max(startCol, startCol + nCols - 1)];
            range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            //range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            //range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            //range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            //range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
        }


        public static void AddBarChartColumnStacked(this ExcelWorksheet xlSheet, string chartId, string title, int rowTitle, int colTitle, int left, int top, int width, int height, int rowStart, int rowEnd, int colSerieX, int colSerieY1, int colSerieY2)
        {
            var barChart = xlSheet.Drawings.AddChart(chartId, eChartType.ColumnStacked) as ExcelBarChart;
            barChart.SetPosition(top, left);
            barChart.SetSize(width, height);
            barChart.AdjustPositionAndSize();

            // adjust thinghies
            barChart.Fill.Color = Color.White;

            // set title
            var titleX = string.Empty;
            if (!string.IsNullOrWhiteSpace(title))
                titleX = title;
            if (colTitle != 0 && rowTitle != 0)
                titleX = (string)xlSheet.Cells[rowTitle, colTitle].Value;

            // barChart.Title.Text = titleX;

            var seriesXRange = xlSheet.Cells[rowStart, colSerieX, rowEnd, colSerieX];
            var serieY1Range = xlSheet.Cells[rowStart, colSerieY1, rowEnd, colSerieY1];
            var serieY2Range = xlSheet.Cells[rowStart, colSerieY2, rowEnd, colSerieY2];

            barChart.Series.Add(serieY1Range, seriesXRange);
            barChart.Series[0].Fill.Color = Color.FromArgb(255, 31, 73, 125);
            barChart.Series.Add(serieY2Range, seriesXRange);
            barChart.Series[1].Fill.Color = Color.FromArgb(255, 255, 192, 0);

            barChart.GapWidth = 30;
            barChart.XAxis.Title.Text = titleX;
            barChart.XAxis.Title.Font.Bold = true;
            barChart.XAxis.Title.Font.Color = Color.Black;
            barChart.XAxis.Title.Font.Size = 16;

            barChart.YAxis.MajorGridlines.LineCap = OfficeOpenXml.Drawing.eLineCap.Flat;
            barChart.YAxis.MajorGridlines.LineStyle = OfficeOpenXml.Drawing.eLineStyle.Solid;
            barChart.YAxis.MajorGridlines.Fill.Color = Color.DarkGray;

            barChart.Legend.Remove();
        }


        public static void CreateHistogramChartValues(this ExcelWorksheet sheet, string title, int left, int top, int rowStart, int rowEnd, int colx, int coly, int width = 600, int height = 500)
        {
            var chart = sheet.Drawings.AddChart(title, eChartType.ColumnClustered);
            chart.SetPosition(top, left);
            chart.SetSize(width, height);

            var range = sheet.Cells[rowStart, colx, rowEnd, colx];
            var serie = sheet.Cells[rowStart, coly, rowEnd, coly];

            if (!string.Empty.Equals(title))
                chart.Title.Text = title;

            chart.Series.Add(serie, range);
            chart.XAxis.MajorTickMark = eAxisTickMark.Out;
            chart.XAxis.MinorTickMark = eAxisTickMark.None;
            chart.Legend.Remove();
        }

        public static void ColorCells(this ExcelWorksheet sheet, int rowStart, int colStart, int rowEnd, int colEnd, Color? color = null)
        {
            Color colorToUse = color ?? Color.FromArgb(1, 146, 205, 220);
            sheet.Cells[rowStart, colStart, rowEnd, colEnd].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[rowStart, colStart, rowEnd, colEnd].Style.Fill.BackgroundColor.SetColor(colorToUse);
        }
    }
}
