﻿using KG.Props.SWA.Connected_Services.KG.Props.SWA.ProfilerService;
using KG.Props.SWA.Models;
using System;
using System.Collections.Generic;

namespace Ram.Profiling.Models
{
    public class UserProfile
    {
        public string UserID { get; set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string FiscalCode { get; private set; }
        public string Professional { get; private set; }
        public string EnterpriseID { get; set; }
        public string EnterpriseDescription { get; private set; }
        public string UO { get; private set; }
        public string UODescription { get; private set; }
        public string CompanyID { get; set; }
        public string CompanyDescription { get; private set; }
        public bool IsValid { get; set; }
        public string ErrorCode { get; private set; }
        public string ErrorMessage { get; private set; }
        public int ProfileNumber { get; private set; }
        public List<Profile> Profiles { get; set; }

        // dummy initializer that does not call the service
        public UserProfile()
        {

        }

        /// <summary>
        /// Use this method to search the profile of a user in SWP. Use IsValid field to test if the user is present in SWP.
        /// </summary>
        /// <param name="userID">The ID of a user</param>
        /// <param name="webServiceURL">The URL of the SWP web service</param>
        public UserProfile(string userID)
        {
            Init(userID, "");
        }

        /// <summary>
        /// Use this method to search the profile of a user in SWP. Use IsValid field to test if the user is present in SWP.
        /// </summary>
        /// <param name="userID">The ID of a user</param>
        /// <param name="webServiceURL">The URL of the SWP web service</param>
        public UserProfile(string userID, string webServiceURL)
        {
            Init(userID, webServiceURL);
        }

        private void Init(string userID, string webServiceURL)
        {
            if (string.IsNullOrEmpty(userID))
            {
                ErrorMessage = "The userID is null or empty";
                ErrorCode = "404";
                IsValid = false;
            }
            else
            {
                try
                {
                    UserID = userID;
                    Profiles = new List<Profile>();
                    var srv = new ServiceSoapClient();


                    if (!string.IsNullOrEmpty(webServiceURL))
                    {
                        // change endpoint with received one
                        var endpoint = webServiceURL.EndsWith("/") ? webServiceURL + "ServiceSoap" : webServiceURL + "/ServiceSoap";
                        srv = new ServiceSoapClient(ServiceSoapClient.EndpointConfiguration.ServiceSoapPort, endpoint);
                    }

                    // Call the web service
                    var result = srv.getProfilazioneAsync(new getProfilazioneRequest(userID, "", "", "", "", "true", "true", "true", "", "", "", "", "", "", SwaAppSettings.AppCodeName)).Result?.getProfilazione;

                    // Test the result status
                    if (result.responseStatus.retCode == "000" && result.responseStatus.numeroElementi > 0)
                    {
                        // Valid user!
                        // Set basic fields
                        var basic = result.profilazione.anagraficaUtente;
                        SetBasicFields(basic);

                        // Set user profiles
                        //ProfileNumber = result.responseStatus.numeroElementi;
                        //Console.WriteLine(ProfileNumber);
                        ProfileNumber = 0;


                        for (int i = 0; i < result.profilazione.userinfo[0].userprofile.Length; i++)
                        {
                            var profile = result.profilazione.userinfo[0].userprofile[i];
                            //if (ProfileNumber > 0 && profile.abilitazioni != null && profile.abilitazioni.Length < ProfileNumber) { ProfileNumber = profile.abilitazioni.Length; }
                            if (profile.abilitazioni != null)
                            {
                                for (int j = 0; j < profile.abilitazioni.Length; j++)
                                {
                                    if (profile.abilitazioni != null && profile.abilitazioni[j] != null)
                                    {
                                        Profiles.Add(new Profile(profile.abilitazioni[j].codice,
                                            profile.abilitazioni[j].anagraficaFunzione));
                                        ProfileNumber++;
                                    }
                                }
                            }
                        }
                        SetResult("", "", true);

                    }
                    else
                    {
                        // Invalid user !
                        if (result.responseStatus.numeroElementi <= 0)
                            SetResult("No profiles received from SWA Service.", "999", false);
                        else 
                            SetResult(result.responseStatus.message, result.responseStatus.retCode, false);
                    }

                }
                catch (Exception ex)
                {
                    // Exception management
                    SetExceptionResult(ex);
                }
            }
        }

        private void SetBasicFields(anagraficaUtenteBodyType basic)
        {
            if (basic == null)
                throw new Exception("No user data received");

            if (basic != null)
            {
                FirstName = basic.nome;
                LastName = basic.cognome;
                FiscalCode = basic.codiceFiscale;
                Professional = basic.figuraProfessionale;
                EnterpriseID = basic.azienda;
                EnterpriseDescription = basic.descAzienda;
                UO = basic.uo;
                UODescription = basic.descUo;
                CompanyID = basic.societa;
                CompanyDescription = basic.descSocieta;
            }
        }

        private void SetExceptionResult(Exception ex)
        {
            var errorMessage = ex.Message;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                errorMessage += "-->" + ex.Message;
            }
            SetResult(errorMessage, "999", false);
        }

        private void SetResult(string errorMessage, string errorCode, bool isValid)
        {
            ErrorMessage = errorMessage;
            ErrorCode = errorCode;
            IsValid = isValid;
        }
    }
}
