﻿using Microsoft.Extensions.Primitives;
using System.Collections.Generic;
using System.Linq;

namespace Ram.Profiling.Models
{
    public class UserRequest
    {
        public string UserID { get; private set; }
        public string UserDirectory { get; private set; }
        public string SWAProfileID { get; private set; }
        public string LinkSWP { get; private set; }
        public string UserIdentity { get; private set; }
        public bool IsValid { get; private set; }

        public UserRequest(IList<string> headersKeys, IList<StringValues> headers)
        {
            UserID = "";
            UserDirectory = "";
            LinkSWP = "";
            UserIdentity = "";
            var headersKeysCount = headersKeys.Count;
            for (int i = 0; i < headersKeysCount; i++)
            {
                if (i > headers.Count) continue;

                var value = headers[i].FirstOrDefault(x => !string.IsNullOrWhiteSpace(x));
                if (string.IsNullOrWhiteSpace(value))
                    continue;

                switch (headersKeys[i].Trim())
                {
                    case "ProfileServicesUrl":
                        LinkSWP = value;
                        break;
                    case "SWAProfileID":
                        SWAProfileID = value;
                        var splitted = SWAProfileID.Split('\\');
                        if (splitted.Length == 2)
                        {
                            UserDirectory = splitted[0];
                            UserID = splitted[1];
                        }
                        break;
                    case "SWAUserIdentity":
                        UserIdentity = value;
                        break;
                    default:
                        continue;
                }
            }
            IsValid = (!string.IsNullOrEmpty(LinkSWP) && !string.IsNullOrEmpty(UserID));
        }
    }
}
