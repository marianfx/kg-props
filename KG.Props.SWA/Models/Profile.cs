﻿using KG.Props.SWA.Connected_Services.KG.Props.SWA.ProfilerService;

namespace KG.Props.SWA.Models
{
    public class Profile
    {
        public string RoleID { get; private set; }
        public string RoleDescription { get; private set; }
        public string RoleShortDescription { get; private set; }

        public Profile(string RoleID, string RoleDescription, string RoleShortDescription)
        {
            this.RoleID = RoleID;
            this.RoleDescription = RoleDescription;
            this.RoleShortDescription = RoleShortDescription;
        }

        public Profile(string RoleID, anagraficaFunzioneSlimType anagraficaFunzione)
        {
            this.RoleID = RoleID;
            if (anagraficaFunzione != null)
            {
                RoleDescription = anagraficaFunzione.descrizioneFunzioneLunga;
                RoleShortDescription = anagraficaFunzione.descrizioneFunzioneBreve;
            }
        }
    }
}
