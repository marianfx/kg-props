﻿Prerequisites:
- the proxy service must be installed on the server and configured
- the proxy must be configured to accept the application code (from SwaAppSettings)
- web.config must be configured to use the paths to the installed (on server) proxy (see example web.config)

Example usage in a Controller (must read Http Context & Headers):

profileSwa = new ProfileManager().GetFullSWAUser(Request.HttpContext);
if (!profileSwa.IsValid)
    return new JsonResult(new { error = $"Cannot authenticate SWA. Details: {profileSwa.ErrorMessage} [{profileSwa.ErrorCode}]." }) { StatusCode = StatusCodes.Status400BadRequest };


var swaProfile = _mapper.Map<SwaUser>(profileSwa); // map to a local user model if needed
var userRam = _userManager.LoadUserFromSWAUser(swaProfile); // do eventually other mappings, load from local database based on id etc