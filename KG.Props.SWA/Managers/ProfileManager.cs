﻿using Microsoft.AspNetCore.Http;
using Ram.Profiling.Models;
using System.Collections.Generic;
using System.Linq;

namespace KG.Props.SWA.Managers
{
    public class ProfileManager
    {
        /// <summary>
        /// Parses the REQUEST and returns the full SWA User
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public UserProfile GetFullSWAUser(HttpContext httpContext)
        {
            UserProfile userProfile = null;
            var userRequest = new UserRequest(httpContext.Request.Headers.Keys.ToList(), httpContext.Request.Headers.Values.ToList());

            userProfile = new UserProfile(userRequest.UserID, userRequest.LinkSWP);
            return userProfile;
        }

        public string GetUserProfileId(HttpContext httpContext)
        {
            UserProfile userProfile = null;
            UserRequest userRequest = new UserRequest(httpContext.Request.Headers.Keys.ToList(), httpContext.Request.Headers.Values.ToList());
            string UserID = null;

            userProfile = new UserProfile(userRequest.UserID, userRequest.LinkSWP);
            UserID = userRequest.UserID;

            return UserID;
        }
        
        public string GetUserCompany(HttpContext httpContext)
        {
            UserProfile userProfile = null;
            UserRequest userRequest = new UserRequest(httpContext.Request.Headers.Keys.ToList(), httpContext.Request.Headers.Values.ToList());
            string Company = null;

            userProfile = new UserProfile(userRequest.UserID, userRequest.LinkSWP);
            Company = userProfile.EnterpriseID;

            return Company;
        }
        
        public List<string> GetUserRole(HttpContext httpContext)
        {
            UserProfile userProfile = null;
            UserRequest userRequest = new UserRequest(httpContext.Request.Headers.Keys.ToList(), httpContext.Request.Headers.Values.ToList());
            List<string> Roles = new List<string>();

            userProfile = new UserProfile(userRequest.UserID, userRequest.LinkSWP);

            foreach (var role in userProfile.Profiles)
            {
                if (role.RoleID == "YAVJN8" || role.RoleID == "YAVJN9" || role.RoleID == "YAVJN0")
                {
                    Roles.Add(role.RoleID);
                }
            }

            return Roles;
        }
    }
}
