﻿using System;
using System.Threading.Tasks;

namespace KG.Props.IO.Abstract
{
    public interface IFileHandlerBase
    {
        Task DeleteFileSilentlyInOtherThread(string fullPath);
        Task DeleteFilesFromDirOlderThanDate(string directoryPath, DateTime date);
    }
}
