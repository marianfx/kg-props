﻿using KG.Props.Resources.Enumerations;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace KG.Props.IO.Abstract
{
    public interface IFileHandler: IFileHandlerBase
    {
        /// <summary>
        /// Returns always the current processed file path;
        /// </summary>
        string CurrentFileName { get; set; }

        /// <summary>
        /// Given a file path, validates if the path is valid and the file exists.
        /// </summary>
        /// <param name="relativePath"></param>
        /// <param name="parentDirectory"></param>
        /// <returns></returns>
        FileInfo ValidateFile(string relativePath, string parentDirectory);

        /// <summary>
        /// Copies a file
        /// </summary>
        /// <param name="fromPath"></param>
        /// <param name="toPath"></param>
        void CopyFile(string fromPath, string toPath);

        /// <summary>
        /// Deletes a file not throwing exception if something bad happens
        /// </summary>
        /// <param name="fullPath"></param>
        void DeleteFileSilently(string fullPath);

        /// <summary>
        /// Deletes a bunch of files without throwing error if something bad happens
        /// </summary>
        /// <param name="allPaths"></param>
        void DeleteFilesSilently(IEnumerable<string> allPaths);

        /// <summary>
        /// Deletes a bunch of files without throwing error if something bad happens
        /// Runs in a separate thread
        /// </summary>
        /// <param name="allPaths"></param>
        Task DeleteFilesSilentlyInOtherThread(IEnumerable<string> allPaths);

        /// <summary>
        /// Returns a file stream based on parameters.
        /// </summary>
        /// <param name="relativePath">The file relative path.</param>
        /// <param name="fileType">The file type</param>
        /// <param name="parentDirectory">The path of the server containing directory (if exists)</param>
        /// <returns></returns>
        FileStream GetFileStream(string relativePath, FileTypes fileType, string parentDirectory);

        /// <summary>
        /// Returns a file stream for an excel file.
        /// </summary>
        /// <param name="fileRelativePath">The file relative path.</param>
        /// <returns></returns>
        FileStream GetExcelFileStream(string fileRelativePath);

        /// <summary>
        /// Given a Form File collection (presumably from a Request), saves the first file to the path specified.
        /// Throws error if no files are specified.
        /// </summary>
        /// <param name="formFiles"></param>
        /// <param name="relativePath"></param>
        /// <param name="parentDirectory"></param>
        Task SaveFileFromForm(IFormFileCollection formFiles, string relativePath, string parentDirectory);

        /// <summary>
        /// The same as the SaveFileFromForm, but for an excel file
        /// </summary>
        /// <param name="formFiles"></param>
        /// <param name="fileRelativePath"></param>
        Task SaveExcelFileFromForm(IFormFileCollection formFiles, string fileRelativePath);

        /// <summary>
        /// The same as the SaveFileFromForm, but for an csv file
        /// </summary>
        /// <param name="formFiles"></param>
        /// <param name="fileRelativePath"></param>
        Task SaveCsvFileFromForm(IFormFileCollection formFiles, string fileRelativePath);
    }
}
