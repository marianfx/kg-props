﻿using KG.Props.IO.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KG.Props.IO.Real
{
    public class FileHandlerBase: IFileHandlerBase
    {
        public void DeleteFileSilently(string fullPath)
        {
            try
            {
                File.Delete(fullPath);
            }
            catch (Exception)
            {
                // Console.WriteLine(ex);
            }
        }

        public async Task DeleteFileSilentlyInOtherThread(string fullPath)
        {
            await Task.Run(() =>
            {
                try
                {
                    File.Delete(fullPath);
                }
                catch (Exception)
                {
                    // Console.WriteLine(ex);
                }
            });
        }

        public void DeleteFilesSilently(IEnumerable<string> allPaths)
        {
            foreach (var path in allPaths)
            {
                DeleteFileSilently(path);
            }
        }

        public async Task DeleteFilesSilentlyInOtherThread(IEnumerable<string> allPaths)
        {
            await Task.Run(() =>
            {
                foreach (var path in allPaths)
                {
                    DeleteFileSilently(path);
                }
            });
        }

        public async Task DeleteFilesFromDirOlderThanDate(string directoryPath, DateTime date)
        {
            if (!Directory.Exists(directoryPath))
                return;

            try
            {
                var files = Directory.GetFiles(directoryPath, "*.*", SearchOption.AllDirectories)
                    .Select(f => new FileInfo(f))
                    .Where(f => f.CreationTime < date && f.Name != ".gitignore")
                    .Select(f => f.FullName)
                    .ToList();

                await DeleteFilesSilentlyInOtherThread(files);
            }
            catch (Exception)
            {
                // Console.WriteLine(ex);
            }
        }
    }
}
