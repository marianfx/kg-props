﻿using KG.Props.IO.Abstract;
using KG.Props.Resources.Enumerations;
using KG.Props.Resources.Text;
using KG.Props.Resources.Values;
using KG.Props.Validations.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KG.Props.IO.Real
{
    public class FileHandler : FileHandlerBase, IFileHandler
    {
        private readonly IValidator _validator;

        public string RootPath { get; set; }
        public string CurrentFileName { get; set; } = "";

        FileHandler(IValidator validator, string rootPath)
        {
            _validator = validator;
            RootPath = rootPath;
        }

        public FileHandler(IValidator validator, IHostingEnvironment environment)
        {
            _validator = validator;
            RootPath = environment.ContentRootPath;
        }

        public FileInfo ValidateFile(string relativePath, string parentDirectory)
        {
            var errors = _validator.ValidateString(relativePath, Literals.File).GetErrors();
            if (errors.Count > 0)
                throw new Exception(errors[0]);

            var serverPath = Path.Combine(RootPath, parentDirectory);
            var fullStreamFile = Path.Combine(serverPath, relativePath);
            var fileInfo = new FileInfo(fullStreamFile);
            if (!fileInfo.Exists)
                throw new Exception(Literals.FileDoesNotExist);

            return fileInfo;
        }

        public void CopyFile(string fromPath, string toPath)
        {
            var fromServerPath = Path.Combine(RootPath, fromPath);
            var toServerPath = Path.Combine(RootPath, toPath);

            var fileInfoFrom = new FileInfo(fromServerPath);
            if (!fileInfoFrom.Exists)
                throw new Exception("Source " + Literals.FileDoesNotExist);

            if (File.Exists(toServerPath))
                File.Delete(toServerPath);

            var fi = new FileInfo(toServerPath);
            if (!fi.Directory.Exists)
                fi.Directory.Create();

            try
            {
                File.Copy(fromServerPath, toServerPath);
            }
            catch (Exception ex)
            {
                throw new Exception(Literals.FileCannotBeCopied + " " + ex.Message);
            }
        }

        public async Task CopyFileAsync(string fromPath, string toPath)
        {
            var fromServerPath = Path.Combine(RootPath, fromPath);
            var toServerPath = Path.Combine(RootPath, toPath);

            var fileInfoFrom = new FileInfo(fromServerPath);
            if (!fileInfoFrom.Exists)
                throw new Exception("Source " + Literals.FileDoesNotExist);

            if (File.Exists(toServerPath))
                File.Delete(toServerPath);

            var fi = new FileInfo(toServerPath);
            if (!fi.Directory.Exists)
                fi.Directory.Create();

            try
            {
                using (var sourceStream = File.Open(fromServerPath, FileMode.Open))
                {
                    using (var destinationStream = new FileStream(toServerPath, FileMode.Create))
                    {
                        await sourceStream.CopyToAsync(destinationStream);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Literals.FileCannotBeCopied + " " + ex.Message);
            }
        }

        #region Streams (sending to CLIENT)
        public FileStream GetFileStream(string relativePath, FileTypes fileType, string parentDirectory)
        {
            if (!Constants.FileMimeTypes.Keys.Contains(fileType))
                throw new Exception(Literals.UnknownFileType);

            var fileInfo = ValidateFile(relativePath, parentDirectory);
            CurrentFileName = fileInfo.Name;
            return new FileStream(fileInfo.FullName, FileMode.Open);
        }

        public FileStream GetExcelFileStream(string fileRelativePath)
        {
            return GetFileStream(fileRelativePath, FileTypes.Excel, Constants.DefaultSavedExcelsDirectory);
        }
        #endregion

        #region Saving from FORM
        public async Task SaveFileFromForm(IFormFileCollection formFiles, string relativePath, string parentDirectory)
        {
            if (formFiles == null || formFiles.Count == 0 || formFiles[0].Length == 0)
                throw new Exception(Literals.NoFileSpecified);

            var file = formFiles[0];
            var serverPath = Path.Combine(RootPath, parentDirectory);
            try
            {
                if (!Directory.Exists(serverPath))
                    Directory.CreateDirectory(serverPath);

                var filePath = Path.Combine(serverPath, relativePath);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Literals.CannotAccessTheRequestedPath, ex);
            }
        }

        public async Task SaveExcelFileFromForm(IFormFileCollection formFiles, string fileRelativePath)
        {
            await SaveFileFromForm(formFiles, fileRelativePath, Constants.DefaultImportedExcelsDirectory);
        }

        public async Task SaveCsvFileFromForm(IFormFileCollection formFiles, string fileRelativePath)
        {
            await SaveFileFromForm(formFiles, fileRelativePath, Constants.DefaultImportedTextDirectory);
        }
        #endregion
    }
}
