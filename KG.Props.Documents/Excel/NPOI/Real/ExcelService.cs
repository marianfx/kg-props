﻿using KG.Props.Documents.Excel.NPOI.Abstract;
using KG.Props.Documents.Excel.NPOI.Models;
using KG.Props.Resources.Text;
using KG.Props.Extensions.Excel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using KG.Props.Extensions.Types;
using NPOI.HSSF.UserModel;
using System.Text.RegularExpressions;
using NPOI.SS.Util;
using KG.Props.Resources.Values;

namespace KG.Props.Documents.Excel.NPOI.Real
{
    public class ExcelService: IExcelService
    {
        #region Setup
        public int RowsPerSheet { get; set; } = int.MaxValue;
        public int RowsPerPage { get; set; } = 500;
        public int MinColumnWidth { get; set; } = 15 * 256;
        public string FilePath { get; set; }
        public IEnumerable<ExcelSheet> Sheets { get; set; }

        protected XSSFWorkbook Wb;
        protected ICellStyle HeaderCellStyle;
        protected ICellStyle NormalCellStyle;
        protected ICellStyle DateCellStyle;
        protected int CurrentRow;

        public ExcelService()
        {
            FilePath = null;
            Sheets = new List<ExcelSheet>();
        }

        public void Initialize(string filePath, IEnumerable<ExcelSheet> sheets)
        {
            FilePath = filePath;
            Sheets = sheets;
        }
        #endregion

        #region Access to inner layers 
        public ISheet GetSheetAt(int index)
        {
            if (Wb == null)
                throw new Exception("Trying to access Excel workbook before it is initialized");

            if (index < 0 || Wb.NumberOfSheets <= index)
                throw new Exception("Trying to read a sheet that doesn't exist.");

            return Wb.GetSheetAt(index);
        }

        public int GetSheetCount()
        {
            if (Wb == null)
                throw new Exception("Trying to access Excel workbook before it is initialized");

            return Wb.NumberOfSheets;
        }

        public ISheet GetSheet(string sheetName)
        {
            if (Wb == null)
                throw new Exception("Trying to access Excel workbook before it is initialized");

            var sheet = Wb.GetSheet(sheetName);
            if (sheet == null)
                throw new Exception($"Sheet {sheetName} not found in the current document.");

            return sheet;
        }
        #endregion

        #region Writing
        /// <summary>
        /// Using initialization data, creates the excel document, adds all the provided sheets (and their header rows if they exist).
        /// Initializes the style for the document. Saves the document on disk.
        /// </summary>
        public void CreateDocument()
        {
            var newFile = new FileInfo(FilePath);
            if (newFile.Exists) // CHECK IF THE FILE EXISTS. 
            {
                newFile.Delete();  // DELETE THE FILE IF IT ALREADY EXISTS
                newFile = new FileInfo(FilePath);
            }
            var dir = newFile.Directory;
            if (dir != null && !dir.Exists)
                dir.Create();

            Wb = new XSSFWorkbook();
            CreateStyles(Wb);
            try
            {
                foreach (var sheet in Sheets)
                {
                    AddSheet(sheet);
                }

                SaveXls();
            }
            catch (Exception ex)
            {
                throw new Exception(Literals.CannotCreateExcelFile, ex);
            }
        }

        public void ExportDataToExcelInBlocks(string fullPath, string mainSheetName, Func<int, int, DataTable> datatableRetriever)
        {
            // get first batch of rows and set columns
            var data = datatableRetriever(0, RowsPerPage);
            if (data == null || data.Columns.Count == 0)
                throw new Exception(Literals.ErrorWritingExcelFileCannotReadColumnsForResultSet);

            var columnList = data.Columns.OfType<DataColumn>().Select(c => c.ColumnName).ToList();
            var mainSheet = new ExcelSheet()
            {
                SheetName = mainSheetName /*+ "1"*/,
                Columns = columnList,
            };
            var sheetList = new List<ExcelSheet>() { mainSheet };

            Initialize(fullPath, sheetList);
            CreateDocument();

            var rowsRead = data.Rows.Count;
            var start = 0;

            while (rowsRead != 0)
            {
                AddRows(mainSheet, data, null, false);
                start += RowsPerPage;
                data = datatableRetriever(start, RowsPerPage);
                rowsRead = data?.Rows.Count ?? 0;
            }

            foreach (var sheet in Sheets)
            {
                AutoSizeColumns(sheet);
            }

            SaveXls();
        }

        public void ExportDataToExcelFromDataTable(string serverPath, string mainSheetName, DataTable data, string fileName = "export.xlsx", bool? withGraph = null)
        {
            var fullPath = Path.Combine(serverPath, fileName);
            var fi = new FileInfo(fullPath);
            if (fi.Directory != null && !fi.Directory.Exists)
                fi.Directory.Create();

            if (data == null || data.Columns.Count == 0)
                throw new Exception(Literals.ErrorWritingExcelFileCannotReadColumnsForResultSet);

            var columnList = data.Columns.OfType<DataColumn>().Select(c => c.ColumnName).ToList();
            var mainSheet = new ExcelSheet()
            {
                SheetName = mainSheetName,
                Columns = columnList,
            };
            var sheetList = new List<ExcelSheet>() { mainSheet };

            Initialize(fullPath, sheetList);
            if (withGraph == null || withGraph == false)
            {
                CreateDocument();
                AddRows(mainSheet, data, null, false);
                AutoSizeColumns(mainSheet);
                SaveXls();
            }

            if (withGraph == true)
            {
                ReadXls();
                if (data.Rows.Count == 0) // clear first row (template)
                {
                    var sh = this.GetSheet(mainSheet.SheetName);
                    sh.Cells(2, 1).SetCellValue("");
                    sh.Cells(2, 2).SetCellValue("");
                }
                AddRows(mainSheet, data, 0, false);
                SaveXls();
            }
        }

        public void ExportDataToExcelFromDataTable(string fullPath, params (string sheetName, DataTable sheetData)[] data)
        {
            if (data == null)
                throw new Exception(Literals.ErrorWritingExcelFileCannotReadColumnsForResultSet);

            var sheetList = new List<ExcelSheet>();
            foreach (var (sheetName, sheetData) in data)
            {
                if (sheetData.Columns.Count == 0)
                    throw new Exception(Literals.ErrorWritingExcelFileCannotReadColumnsForResultSet);

                var columnList = sheetData.Columns.OfType<DataColumn>().Select(c => c.ColumnName).ToList();
                var sheet = new ExcelSheet()
                {
                    SheetName = sheetName.ToSheetNameFormat(sheetList.Select(x => x.SheetName).ToList()),
                    Columns = columnList,
                };
                sheetList.Add(sheet);
            }

            Initialize(fullPath, sheetList);
            CreateDocument();

            for (int i = 0; i < sheetList.Count; i++)
            {
                AddRows(sheetList[i], data[i].sheetData, null, false);
                AutoSizeColumns(sheetList[i]);
            }
            SaveXls();
        }

        /// <summary>
        /// Adds a SHEET to a document. Does not add data.
        /// </summary>
        /// <param name="sheetToAdd"></param>
        /// <param name="reload"></param>
        /// <returns></returns>
        public ISheet AddSheet(ExcelSheet sheetToAdd, bool reload = false)
        {
            if (reload) ReadXls();

            var worksheet = (XSSFSheet)Wb.CreateSheet(sheetToAdd.SheetName);
            if (sheetToAdd.SheetColor.HasValue)
                worksheet.TabColorIndex = sheetToAdd.SheetColor.Value;

            AddHeaderRow(worksheet, sheetToAdd.Columns);

            if (reload) SaveXls();
            return worksheet;
        }

        /// <summary>
        /// Adds all the rows from a datatable, in the Excel file (if found).
        /// </summary>
        /// <param name="whereToAdd"></param>
        /// <param name="data"></param>
        /// <param name="startRow">Specify if in need to start adding data from a certain row</param>
        /// <param name="reread">Specify if in need to re-read the file.</param>
        public void AddRows(ExcelSheet whereToAdd, DataTable data, int? startRow = null, bool reread = true)
        {
            try
            {
                if (reread) ReadXls();

                var worksheet = Wb.GetSheet(whereToAdd.SheetName);

                CurrentRow = startRow != null ? Math.Max((startRow.Value - 1), 0) : worksheet.LastRowNum;

                for (int j = 0; j < data.Rows.Count; j++)
                {
                    PrepareWorksheetForMaxRows(ref worksheet, whereToAdd);

                    var row = worksheet.GetRow(CurrentRow) ?? worksheet.CreateRow(CurrentRow);
                    for (int k = 0; k < whereToAdd.Columns.Count; k++)
                    {
                        var cell = row.GetCell(k) ?? row.CreateCell(k);
                        cell.CellStyle = NormalCellStyle;

                        // insert data into the cells
                        var rowVal = data.Rows[j][k];
                        var cellType = rowVal?.GetType() ?? typeof(string);
                        var rowValue = (!Convert.IsDBNull(rowVal) && rowVal != null ? rowVal : "");
                        var isNumber = cellType.IsNumberType();
                        var isBool = cellType == typeof(bool);
                        var isDateTime = cellType == typeof(DateTime);
                        if (isNumber)
                        {
                            var numberValue = (double)TypesExtensions.ChangeType(rowValue, typeof(double));
                            if (!double.IsNaN(numberValue))
                            {
                                cell.SetCellType(CellType.Numeric);
                                cell.SetCellValue(numberValue);
                            }
                            else
                            {
                                cell.SetCellValue(string.Empty);
                            }
                        }
                        else if (isBool)
                        {
                            cell.SetCellType(CellType.Boolean);
                            cell.SetCellValue((bool)TypesExtensions.ChangeType(rowValue, typeof(bool)));
                        }
                        else if (isDateTime)
                        {
                            var valueAsDate =
                                ((DateTime)TypesExtensions.ChangeType(rowValue, typeof(DateTime)));

                            cell.CellStyle = DateCellStyle;
                            cell.SetCellValue(valueAsDate);
                        }
                        else
                        {
                            cell.SetCellType(CellType.String);
                            cell.SetCellValue(rowValue.ToString());
                        }
                    }
                }

                if (reread) SaveXls();
            }
            catch (Exception ex)
            {
                throw new Exception(Literals.ErrorWritingExcelFileFailedToAddRows, ex);
            }
        }

        /// <summary>
        /// Sets height of specific row in specific sheet, in the file (if found).
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="rowIndex"></param>
        /// <param name="height"></param>
        public void SetRowHeight(string sheetName, int rowIndex, short height)
        {
            try
            {
                var sheet = FindSheetByName(sheetName);
                var rowCount = sheet.LastRowNum + 1;

                // create row if inexistent
                if (sheet.GetRow(rowIndex) == null)
                    sheet.CreateRow(rowIndex);
                sheet.GetRow(rowIndex).Height = height;
                SaveXls();
            }
            catch (Exception ex)
            {
                throw new Exception(Literals.ErrorWritingExcelFileFailedToAddRows, ex);
            }
        }


        /// <summary>
        /// Evaluates all the formulas in the current workbook. Throws error if no workbook is opened.
        /// </summary>
        public void EvaluateAllFormulasInWorkbook()
        {
            if (Wb == null)
                throw new Exception(Literals.NotInitialized);

            try
            {
                HSSFFormulaEvaluator.EvaluateAllFormulaCells(Wb);//alternative
            }
            catch (Exception)
            {
                // Console.WriteLine(ex.Message);
            }
        }

        //var range = new CellRangeAddress(row, row, start, start + count - 1);
        //sheet.AddMergedRegion(range);
        #endregion

        #region Reading

        #region Reading with fields
        public DataTable ReadExcelFile(string filePath, IEnumerable<string> columnsExpected, IEnumerable<string> columnRequired)
        {
            var output = new DataTable();

            //loop through sheets
            foreach (var sheet in Sheets)
            {
                var dt = ReadSheet(filePath, sheet, columnsExpected, columnRequired);
                output.Merge(dt);
            }
            return output;
        }

        public DataTable GetExcelDatatableFromSheet(string filePath, Dictionary<string, string> fields, string sheetName = "Foglio1")
        {
            // initialize sheet and data
            var requiredFields = fields.Select(x => x.Key).ToList();

            var sheetInitial = new ExcelSheet
            {
                Columns = requiredFields.ToList(),
                SheetName = sheetName
            };
            Initialize("", new List<ExcelSheet>() { sheetInitial });
            return ReadExcelFile(filePath, sheetInitial.Columns, requiredFields);
        }

        public DataTable ReadSheet(string filePath, ExcelSheet sheet, IEnumerable<string> columnsExpected, IEnumerable<string> columnRequired)
        {
            FilePath = filePath;
            ReadXls();
            var worksheet = FindSheetByName(sheet.SheetName);
            DataTable dt = new DataTable();

            // no data
            if (worksheet.PhysicalNumberOfRows < 1)
                return dt;

            //get the first sheet
            IRow headerRow = worksheet.GetRow(0);
            var columnIndexes = new List<int>();

            //loop through cols to check if everything is ok
            for (int j = 0; j < columnsExpected.Count(); j++)
            {
                var expectedColumn = columnsExpected.ElementAt(j);
                var found = false;
                for (int k = 0; k < headerRow.LastCellNum; k++)
                {
                    ICell cell = headerRow.GetCell(k);
                    if (cell != null && cell.StringCellValue == expectedColumn)
                    {
                        dt.Columns.Add(cell.StringCellValue);
                        columnIndexes.Add(k);
                        found = true;
                        break;
                    }
                }

                if (!found)
                    throw new Exception(Literals.CannotFindColumn + " " + expectedColumn);
            }

            // read data; ignore empty rows; stop if a required column is not found
            for (int i = (worksheet.FirstRowNum + 1); i < worksheet.LastRowNum + 1; i++)
            {
                IRow row = worksheet.GetRow(i);
                DataRow dataRow = dt.NewRow();

                // check if whole row is empty. if so, ignore it
                if (IsAllRowEmpty(columnIndexes, row))
                    continue;

                // add the row
                var k = 0;
                foreach (var j in columnIndexes)
                {
                    var columnName = columnsExpected.ElementAt(k);
                    var columnValue = row.GetCell(j);

                    if (columnRequired.Contains(columnName) &&
                        (columnValue == null || string.IsNullOrWhiteSpace(columnValue.ToString())))
                        throw new Exception(Literals.CannotFindColumn + " " + columnName);

                    var actualValue = columnValue == null || Constants.ExcelIgnoredValues.Contains(columnValue.ToString()) ? "" : columnValue.ToString();
                    dataRow[k++] = actualValue;
                }

                dt.Rows.Add(dataRow);
            }
            return dt;
        }

        #endregion

        #region Reading with column index

        public DataTable ReadExcelFile(string filePath, Dictionary<string, int> fields, int skip = 0)
        {
            DataTable output = new DataTable();

            var expectedColumns = fields.Select(x => x.Key);
            var columnIndexes = fields.Select(x => x.Value - 1);

            Initialize(filePath, new List<ExcelSheet>());
            ReadXls();
            Sheets = GetAllSheetsInExcel();

            //loop through sheets
            foreach (var sheet in Sheets)
            {
                var dt = ReadSheet(filePath, sheet, expectedColumns, columnIndexes, skip);
                output.Merge(dt);
            }
            return output;
        }

        public DataTable ReadSheet(string filePath, ExcelSheet sheet, IEnumerable<string> columnsExpected, IEnumerable<int> columnIndexes, int skip = 0)
        {
            FilePath = filePath;
            ReadXls();
            var worksheet = FindSheetByName(sheet.SheetName);
            DataTable dt = new DataTable();

            // no data
            if (worksheet.PhysicalNumberOfRows == 0 || (worksheet.LastRowNum < skip))
                return dt;

            //loop through cols to check if everything is ok
            var firstRow = worksheet.GetRow(worksheet.FirstRowNum) ?? worksheet.CreateRow(worksheet.FirstRowNum);
            for (int j = 0; j < columnsExpected.Count(); j++)
            {
                var expectedColumn = columnsExpected.ElementAt(j);
                var expectedIndex = columnIndexes.ElementAt(j);
                if (expectedIndex < 0 || expectedIndex > firstRow.LastCellNum)
                    throw new Exception(Literals.CannotFindColumn + " " + expectedColumn);

                dt.Columns.Add(expectedColumn);
            }

            // read data; ignore empty rows; stop if a required column is not found
            for (int i = (worksheet.FirstRowNum + skip); i < worksheet.LastRowNum + 1; i++)
            {
                IRow row = worksheet.GetRow(i);
                DataRow dataRow = dt.NewRow();

                // check if whole row is empty. if so, ignore it
                if (IsAllRowEmpty(columnIndexes, row))
                    continue;

                // add the row
                var k = 0;
                var found = true;
                foreach (var j in columnIndexes)
                {
                    var columnName = columnsExpected.ElementAt(k);
                    var columnValue = row.GetCell(j);

                    if (columnValue == null || string.IsNullOrWhiteSpace(columnValue.ToString()))
                    {
                        found = false;
                        break;
                    }

                    var actualValue = Constants.ExcelIgnoredValues.Contains(columnValue.ToString()) ? "" : columnValue.ToString();
                    dataRow[k++] = actualValue;
                }

                if (found)
                    dt.Rows.Add(dataRow);
            }
            return dt;
        }


        #endregion

        #region Reading ignoring fields
        public DataTable ReadExcelFile(string filePath, int expectedColumnsCount = 0, bool hasHeader = false)
        {
            DataTable output = new DataTable();
            //loop through sheets
            foreach (var sheet in Sheets)
            {
                var dt = ReadSheet(filePath, sheet, expectedColumnsCount, hasHeader);
                output.Merge(dt);
            }
            return output;
        }

        public Dictionary<string, DataTable> ReadWholeExcelFile(string filePath, bool hasHeaders = false)
        {
            var output = new Dictionary<string, DataTable>();
            Initialize(filePath, new List<ExcelSheet>());
            ReadXls();
            Sheets = GetAllSheetsInExcel();

            foreach (var sheet in Sheets)
            {
                var dt = ReadSheet(filePath, sheet, 0, hasHeaders, false);
                if (output.Keys.Contains(sheet.SheetName))
                {
                    if (output[sheet.SheetName] != null)
                        output[sheet.SheetName].Merge(dt);
                    else
                        output[sheet.SheetName] = dt;
                }
                else
                    output.Add(sheet.SheetName, dt);
            }

            return output;
        }

        public DataTable GetExcelDatatableFromSheet(string filePath, string sheetName = "Foglio1", int expectedColumnCount = 0, bool hasHeader = false)
        {
            var sheetInitial = new ExcelSheet { SheetName = sheetName };
            Initialize("", new List<ExcelSheet>() { sheetInitial });
            return ReadExcelFile(filePath, expectedColumnCount, hasHeader);
        }

        public DataTable ReadSheet(string filePath, ExcelSheet sheet, int expectedColumnCount = 0, bool hasHeader = false, bool reread = true)
        {
            FilePath = filePath;
            if (Wb == null || reread)
                ReadXls();

            var worksheet = FindSheetByName(sheet.SheetName);
            DataTable dt = new DataTable();

            // no data
            if (worksheet.PhysicalNumberOfRows < 1)
                return dt;

            IRow firstRow = worksheet.GetRow(0);
            if (expectedColumnCount > firstRow.LastCellNum)
                throw new Exception(Literals.InvalidColumnCount);

            expectedColumnCount = expectedColumnCount > 0 ? expectedColumnCount : firstRow.LastCellNum;
            var columnIndexes = new List<int>();
            for (var i = 0; i < expectedColumnCount; i++)
            {
                columnIndexes.Add(i);
                dt.Columns.Add();
            }

            var start = hasHeader ? (worksheet.FirstRowNum + 1) : worksheet.FirstRowNum;
            for (int i = start; i < worksheet.LastRowNum + 1; i++)
            {
                IRow row = worksheet.GetRow(i);
                DataRow dataRow = dt.NewRow();

                // check if whole row is empty. if so, ignore it
                if (IsAllRowEmpty(columnIndexes, row))
                    continue;

                // add the row
                var k = 0;
                foreach (var j in columnIndexes)
                {
                    var columnValue = row.GetCell(j);
                    var actualValue = columnValue == null || Constants.ExcelIgnoredValues.Contains(columnValue.ToString()) ? "" : columnValue.ToString();
                    dataRow[k++] = actualValue;
                }

                dt.Rows.Add(dataRow);
            }

            return dt;
        }
        #endregion
        #endregion

        #region Utilities 
        #region Read / Write
        public void ReadXls()
        {
            try
            {
                try
                {
                    if (Wb != null)
                        Wb.Close();
                }
                catch { }

                using (var fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    Wb = new XSSFWorkbook(fs);
                }

                CreateStyles(Wb);

            }
            catch (Exception)
            {
                // Console.WriteLine(ex.Message);
            }

            if (Wb == null)
                throw new Exception(Literals.UnableToRetrieveExcelDocument);
        }

        /// <summary>
        /// Saves the current document on the disk.
        /// </summary>
        public void SaveXls()
        {
            if (Wb == null)
                throw new Exception(Literals.NotInitialized);

            try
            {
                using (var fs = new FileStream(FilePath, FileMode.Create, FileAccess.Write))
                {
                    Wb.Write(fs);
                }
            }
            catch (Exception)
            {
                // Console.WriteLine(ex.Message);
            }
        }
        #endregion

        #region Sheets
        /// <summary>
        /// Reads all the sheets in an excel document as ExcelSheet Wrappers
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ExcelSheet> GetAllSheetsInExcel()
        {
            if (Wb == null)
                throw new Exception(Literals.NotInitialized);

            var output = new List<ExcelSheet>();
            for (int i = 0; i < Wb.NumberOfSheets; i++)
            {
                var sheet = Wb.GetSheetAt(i);
                output.Add(new ExcelSheet() { SheetName = sheet.SheetName });
            }

            return output;
        }

        /// <summary>
        /// Creates the normal and the headers styles for the document
        /// </summary>
        /// <param name="wb"></param>
        private void CreateStyles(XSSFWorkbook wb)
        {
            // normal
            var fontn = wb.CreateFont();
            fontn.FontName = "Arial";
            NormalCellStyle = wb.CreateCellStyle();
            NormalCellStyle.SetFont(fontn);

            // date
            DateCellStyle = wb.CreateCellStyle();
            var fontd = wb.CreateFont();
            fontd.FontName = "Arial";
            DateCellStyle.SetFont(fontd);
            var dataFormatCustom = wb.CreateDataFormat();
            DateCellStyle.DataFormat = dataFormatCustom.GetFormat(Constants.DefaultDisplayDateFormat);

            // headers
            var font = wb.CreateFont();
            font.FontName = "Arial";
            font.IsBold = true;
            HeaderCellStyle = wb.CreateCellStyle();
            HeaderCellStyle.BorderBottom = HeaderCellStyle.BorderTop = HeaderCellStyle.BorderLeft = HeaderCellStyle.BorderRight = BorderStyle.Medium;
            HeaderCellStyle.Alignment = HorizontalAlignment.Center;
            HeaderCellStyle.SetFont(font);
        }

        /// <summary>
        /// Searches for a sheet by name in an open excel doc and returns it as XSSFSheet.
        /// </summary>
        /// <param name="sheetName">todo: describe sheetName parameter on FindSheetByName</param>
        /// <returns></returns>
        private XSSFSheet FindSheetByName(string sheetName)
        {
            if (Wb == null)
                throw new Exception(Literals.NotInitialized);

            //loop through sheets to find sheet I want

            if (!(Wb.GetSheet(sheetName) is XSSFSheet worksheet))
                throw new Exception(Literals.CannotFindWorksheet + " " + sheetName);

            return worksheet;
        }

        /// <summary>
        /// Returns true if an IRow is empty (no data for each column index)
        /// </summary>
        /// <param name="columnIndexes"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private bool IsAllRowEmpty(IEnumerable<int> columnIndexes, IRow row)
        {
            if (row == null)
                return true;

            foreach (var j in columnIndexes)
            {
                var columnValue = row.GetCell(j);
                if (columnValue != null && !string.IsNullOrWhiteSpace(columnValue.ToString()))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// The .xlsx can have configured a max number of rows per sheet. This method checks if that max rows is achieved and if so, generates another sheet do add data in
        /// </summary>
        /// <param name="sheet">The current Sheet. Will be re-referenced to a new sheet if the number of rows exceeds the maximum.</param>
        /// <param name="sheetMetadata">The data about this sheet.</param>
        private void PrepareWorksheetForMaxRows(ref ISheet sheet, ExcelSheet sheetMetadata)
        {
            if (RowsPerSheet < 0 || CurrentRow <= RowsPerSheet)
            {
                CurrentRow++;
                return;
            }

            //executes if the number of rows exceeds the sheet limit
            var result = Regex.Match(sheet.SheetName, @"\d+$").Value;
            var newSheetName = sheet.SheetName;
            if (!string.IsNullOrWhiteSpace(result))
            {
                var number = int.Parse(result);
                var sheetBaseName = newSheetName.Substring(0, newSheetName.Length - result.Length);
                for (int i = 0; i < Wb.NumberOfSheets; i++)
                {
                    var thisName = Wb.GetSheetAt(i).SheetName;
                    if (!thisName.StartsWith(sheetBaseName))
                        continue;

                    var possibleNumber = Regex.Match(thisName, @"\d+$").Value;
                    if (string.IsNullOrWhiteSpace(possibleNumber))
                        continue;

                    var possibleNumberValue = int.Parse(possibleNumber);
                    number = possibleNumberValue > number ? possibleNumberValue : number;
                }
                //here number will be the biggest
                number++;
                newSheetName = sheetBaseName + number;
            }
            else
            {
                newSheetName = sheet.SheetName + "1";
            }

            sheetMetadata.SheetName = newSheetName;
            sheet = AddSheet(sheetMetadata);
            CurrentRow = 0;
        }
        #endregion

        #region Columns

        /// <summary>
        /// Given an Excel File with data on it, runs through columns and autosizes data in the given sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="reread"></param>
        public void AutoSizeColumns(ExcelSheet sheet, bool reread = false)
        {
            try
            {
                if (reread)
                    ReadXls();

                var worksheet = Wb.GetSheet(sheet.SheetName);
                for (int k = 0; k < sheet.Columns.Count; k++)
                {
                    worksheet.AutoSizeColumn(k);
                    var colSize = worksheet.GetColumnWidth(k);
                    var headerText = worksheet.GetRow(0).Cells[k].StringCellValue;
                    var headerExpectedWidth = headerText == null ? MinColumnWidth : headerText.Length * 256 + 5 * 256;
                    if (colSize < headerExpectedWidth)
                        worksheet.SetColumnWidth(k, headerExpectedWidth);
                }
                if (reread)
                    SaveXls();
            }
            catch { }
        }

        /// <summary>
        /// Given an Excel File with data on it, runs through columns and autosizes data in the given sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="reread"></param>
        public void AutoSizeColumns(ISheet sheet, bool reread = false)
        {
            try
            {
                if (reread)
                    ReadXls();

                var worksheet = Wb.GetSheet(sheet.SheetName);
                var n = sheet.GetRow(sheet.FirstRowNum)?.PhysicalNumberOfCells ?? 0;
                for (int k = 0; k < n; k++)
                {
                    worksheet.AutoSizeColumn(k);
                    var colSize = worksheet.GetColumnWidth(k);
                    var headerText = worksheet.GetRow(0).Cells[k].StringCellValue;
                    var headerExpectedWidth = headerText == null ? MinColumnWidth : headerText.Length * 256 + 10 * 256;
                    if (colSize < headerExpectedWidth)
                        worksheet.SetColumnWidth(k, headerExpectedWidth);
                }
                if (reread)
                    SaveXls();
            }
            catch { }
        }

        /// <summary>
        /// Given an Excel File with data on it (already opened, does not re0read), and possibly multiple sheets, resizes the columns for autofit.
        /// Has the possibility to resize all sheets, or the sheets at the given indexes.
        /// </summary>
        public void AutoSizeColumns(IList<int> sheetIds = null)
        {
            try
            {
                if (sheetIds == null)
                {
                    sheetIds = new List<int>();
                    for (var i = 0; i < Wb.NumberOfSheets; i++)
                        sheetIds.Add(i);
                }

                foreach (var sheetId in sheetIds)
                {
                    AutoSizeColumns(Wb.GetSheetAt(sheetId));
                }
            }
            catch { }
        }

        public void MergeCellsHorizontally(string sheetName, int row, int start, int count)
        {
            if (Wb == null)
                throw new Exception(Literals.NotInitialized);

            var sheet = FindSheetByName(sheetName);
            var rowCount = sheet.LastRowNum + 1;

            // create row if inexistent
            if (sheet.GetRow(row) == null)
                sheet.CreateRow(row);

            if (count < 0 && (start - count + 1) < 0)
                throw new Exception("Invalid merge count");

            if (count == 0 || count == 1) // nothing to merge
                return;

            if (count < 0) // merge left case
            {
                start = start - count + 1;
                count = -count;
            }

            // create cells if inexistent
            var sheetRow = sheet.GetRow(row);
            for (var i = start; i < (start + count); i++)
            {
                if (i >= sheetRow.PhysicalNumberOfCells)
                    sheetRow.CreateCell(i);
            }

            // merge range
            var range = new CellRangeAddress(row, row, start, start + count - 1);
            sheet.AddMergedRegion(range);
        }

        public void MergeCellsVerticaly(string sheetName, int startRow, int cell, int count)
        {
            if (Wb == null)
                throw new Exception(Literals.NotInitialized);

            var sheet = FindSheetByName(sheetName);
            var rowCount = sheet.LastRowNum + 1;

            // create row if inexistent
            if (sheet.GetRow(startRow) == null)
                sheet.CreateRow(startRow);

            if (count < 0 && (startRow - count + 1) < 0)
                throw new Exception("Invalid merge count");

            if (count == 0 || count == 1) // nothing to merge
                return;

            // merge range
            var range = new CellRangeAddress(startRow, startRow + count, cell, cell);
            sheet.AddMergedRegion(range);
        }

        /// <summary>
        /// Clears a specifc row in the sheet
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="startRow"></param>
        /// <param name="reread"></param>
        public void ClearSpecificRow(string sheetName, int startRow, bool reread = false)
        {
            if (Wb == null)
                throw new Exception(Literals.NotInitialized);
            if (reread)
                ReadXls();
            var worksheet = Wb.GetSheet(sheetName);
            worksheet.RemoveRow(worksheet.GetRow(startRow));
            worksheet.CreateRow(startRow);
        }

        /// <summary>
        /// Adds a header row in the given sheet, identified (searched) by name.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="startRow"></param>
        public void AddHeaderRow(ExcelSheet sheet, int startRow = 0)
        {
            var worksheet = FindSheetByName(sheet.SheetName);
            AddHeaderRow(worksheet, sheet.Columns, startRow);
        }

        /// <summary>
        /// Adds to the given worksheet a row with the given columns, at the given row.
        /// If row exists, replaces data.
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="columns"></param>
        /// <param name="startRow">At what row to add the data.</param>
        public void AddHeaderRow(ISheet worksheet, IList<string> columns, int startRow = 0)
        {
            var actualRow = worksheet.GetRow(startRow);
            IRow row = actualRow ?? worksheet.CreateRow(startRow);

            for (int j = 0; j < columns.Count; j++)
            {
                var cell = row.PhysicalNumberOfCells > j ? row.GetCell(j) : row.CreateCell(j);
                cell.SetCellValue(columns[j]);
                cell.CellStyle = HeaderCellStyle;
            }
        }
        #endregion
        #endregion

        public void Dispose()
        {
            try
            {
                if (Wb != null)
                    Wb.Close();
            }
            catch { }

            Wb = null;
        }
    }
}
