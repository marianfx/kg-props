﻿using KG.Props.Documents.Excel.NPOI.Models;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace KG.Props.Documents.Excel.NPOI.Abstract
{
    public interface IExcelService : IDisposable
    {
        #region Setup
        /// <summary>
        /// How many rows should a sheet have when writing data
        /// </summary>
        int RowsPerSheet { get; set; }

        /// <summary>
        /// How many rows should one block of data have (when reading from a datasource)
        /// </summary>
        int RowsPerPage { get; set; }

        /// <summary>
        /// The minimum width of a column, used when adjusting widths
        /// </summary>
        int MinColumnWidth { get; set; }

        /// <summary>
        /// The path to this excel file (full, absolute)
        /// </summary>
        string FilePath { get; set; }

        /// <summary>
        /// The list of ExcelSheet that define this document
        /// </summary>
        IEnumerable<ExcelSheet> Sheets { get; set; }

        /// <summary>
        /// Sets the Excel Data for this service to handle. Initializes based on path (for read or write) and the list of sheets
        /// </summary>
        void Initialize(string filePath, IEnumerable<ExcelSheet> sheets);
        #endregion

        #region Access to inner layers
        /// <summary>
        /// Returns an NPOI ISheet that exists at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        ISheet GetSheetAt(int index);

        /// <summary>
        /// Returns an NPOI Isheet that has this name
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        ISheet GetSheet(string sheetName);

        /// <summary>
        /// Returns the number of sheets from this document
        /// </summary>
        /// <returns></returns>
        int GetSheetCount();
        #endregion

        #region Writing
        /// <summary>
        /// Creates and saves to disk the excel document, given the data set to this service with the Initialize and AddRows methods
        /// </summary>
        void CreateDocument();

        /// <summary>
        /// Saves the currently opened document to the disk
        /// </summary>
        void SaveXls();

        /// <summary>
        /// Workflow that generates Excel file at the given path, following the format for the sheets $mainSheetName$1, $mainSheetName$2 etc.. This method must be invoked with a function that returns the data paginated under a form of a datatable.
        /// </summary>
        /// <param name="fullPath">The full server path on where to save the data.</param>
        /// <param name="mainSheetName">The main sheet name. Example: 'Foglio'. The sheet names will be 'Foglio1, Foglio2, etc.'</param>
        /// <param name="datatableRetriever">The function that returns the data. Must accept pagination parameters.</param>
        void ExportDataToExcelInBlocks(string fullPath, string mainSheetName, Func<int, int, DataTable> datatableRetriever);

        /// <summary>
        /// Exports to excel from single Datatable
        /// </summary>
        /// <param name="serverPath"></param>
        /// <param name="mainSheetName"></param>
        /// <param name="data"></param>
        /// <param name="fileName"></param>
        /// <param name="withGraph"></param>
        void ExportDataToExcelFromDataTable(string serverPath, string mainSheetName, DataTable data, string fileName = "export.xlsx", bool? withGraph = null);

        /// <summary>
        /// Exports to excel from multiple Datatable
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="data"></param>
        void ExportDataToExcelFromDataTable(string fullPath, params (string sheetName, DataTable sheetData)[] data);

        /// <summary>
        /// Adds a sheet to a document. If reload is set to true, will read the document from the disk and will add the given sheet. If not, will read the data for the current working on document (created with CreateDocument).
        /// </summary>
        /// <param name="sheetToAdd">This contains data about the sheet to add to the document.</param>
        /// <param name="reload">Indicates if should re-read data for the excel file from the disk and then after adding the sheet save it.</param>
        /// <returns>The reference to the sheet added.</returns>
        ISheet AddSheet(ExcelSheet sheetToAdd, bool reload = false);

        /// <summary>
        /// Adds the given rows to the sheet (and saves the data to the disk)
        /// </summary>
        /// <param name="whereToAdd">The sheet to add in (the name matters as it will read the excel document from the disk and will try to add the data to the existing sheet, so the sheet must be pre-created).</param>
        /// <param name="data">The data holder.</param>
        /// <param name="startRow"></param>
        /// <param name="reread"></param>
        void AddRows(ExcelSheet whereToAdd, DataTable data, int? startRow = null, bool reread = true);


        void SetRowHeight(string sheetName, int rowIndex, short height);

        /// <summary>
        /// Loads the excel doc into memory (if specified to reread) and autosizes all columns .
        /// Use this after all the data has been added to the document.
        /// </summary>
        /// <param name="sheet">The worksheet of which columns to resize.</param>
        /// <param name="reread"></param>
        void AutoSizeColumns(ExcelSheet sheet, bool reread = false);

        /// <summary>
        /// Loads the excel doc into memory (if specified to reread) and autosizes all columns .
        /// Use this after all the data has been added to the document.
        /// </summary>
        /// <param name="sheet">The worksheet of which columns to resize.</param>
        /// <param name="reread"></param>
        void AutoSizeColumns(ISheet sheet, bool reread = false);

        /// <summary>
        /// Given an Excel File with data on it (already opened, does not re0read), and possibly multiple sheets, resizes the columns for autofit.
        /// Has the possibility to resize all sheets, or the sheets at the given indexes.
        /// </summary>
        void AutoSizeColumns(IList<int> sheetIds = null);

        /// <summary>
        /// Merges the cells, horizontally, to the right (count greater than 0) or to the left (count lower than 0)
        /// Count represents the total length of cells to merge (if count == 0, no merge is performed)
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="row"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        void MergeCellsHorizontally(string sheetName, int row, int start, int count);

        /// <summary>
        /// Merges the given cell of the number of rows 
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="cell"></param>
        /// <param name="count"></param>
        /// <param name="startRow"></param>
        void MergeCellsVerticaly(string sheetName, int startRow, int cell, int count);


        /// <summary>
        /// Adds to the given worksheet a row with the given columns, at the given row.
        /// If row exists, replaces data.
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="columns"></param>
        /// <param name="startRow">At what row to add the data.</param>
        void AddHeaderRow(ISheet worksheet, IList<string> columns, int startRow = 0);

        /// <summary>
        /// Adds a header row in the given sheet, identified (searched) by name.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="startRow"></param>
        void AddHeaderRow(ExcelSheet sheet, int startRow = 0);

        /// <summary>
        /// Evaluates all the formulas in the current workbook. Throws error if no workbook is opened.
        /// </summary>
        void EvaluateAllFormulasInWorkbook();

        /// <summary>
        /// Clears a specifc row in the sheet
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="startRow"></param>
        /// <param name="reread"></param>
        void ClearSpecificRow(string sheetName, int startRow, bool reread = false);
        #endregion

        #region Reading
        /// <summary>
        /// Reads the XLS File from disk (assumes the filepath is set in ExcelData object) and opens it in the WB object.
        /// </summary>
        void ReadXls();

        /// <summary>
        /// Reads the data from an excel file and returns it as a Datatable Based on columns (first row is header).
        /// </summary>
        /// <param name="filePath">The path of the imported file</param>
        /// <param name="columnsExpected">The list of the wanted columns</param>
        /// <param name="columnRequired">The list of the mandatory column</param>
        /// <returns></returns>
        DataTable ReadExcelFile(string filePath, IEnumerable<string> columnsExpected, IEnumerable<string> columnRequired);

        /// <summary>
        /// Just another wrapper that reads a datatable from a sheet. Based on columns (first row is header).
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fields"></param>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        DataTable GetExcelDatatableFromSheet(string filePath, Dictionary<string, string> fields, string sheetName = "Foglio1");

        /// <summary>
        /// Reads the data from an excel sheet and returns it as a Datatable. Based on columns (first row is header).
        /// </summary>
        /// <param name="filePath">The path of the imported file</param>
        /// <param name="sheet">The sheet which must be read</param>
        /// <param name="columnsExpected">The list of the wanted columns</param>
        /// <param name="columnRequired">The list of the mandatory fields</param>
        /// <returns></returns>
        DataTable ReadSheet(string filePath, ExcelSheet sheet, IEnumerable<string> columnsExpected, IEnumerable<string> columnRequired);

        /// <summary>
        /// Reads data from excel based on fields and their position in columns
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fields"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        DataTable ReadExcelFile(string filePath, Dictionary<string, int> fields, int skip = 0);

        /// <summary>
        /// Reads data from sheet based on fields and their position in columns
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="sheet"></param>
        /// <param name="columnsExpected"></param>
        /// <param name="columnIndexes"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        DataTable ReadSheet(string filePath, ExcelSheet sheet, IEnumerable<string> columnsExpected, IEnumerable<int> columnIndexes, int skip = 0);

        /// <summary>
        /// Reads the data from an excel file and returns it as a Datatable. Based on column count (ignores names)
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="expectedColumnsCount"></param>
        /// <param name="hasHeader"></param>
        /// <returns></returns>
        DataTable ReadExcelFile(string filePath, int expectedColumnsCount = 0, bool hasHeader = false);

        /// <summary>
        /// Reads all sheets from the excel file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="hasHeaders"></param>
        /// <returns></returns>
        Dictionary<string, DataTable> ReadWholeExcelFile(string filePath, bool hasHeaders = false);

        /// <summary>
        /// Just another wrapper that reads a datatable from a sheet. Based on column count (ignores names)
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="sheetName"></param>
        /// <param name="expectedColumnCount"></param>
        /// <param name="hasHeader"></param>
        /// <returns></returns>
        DataTable GetExcelDatatableFromSheet(string filePath, string sheetName = "Foglio1", int expectedColumnCount = 0, bool hasHeader = false);

        /// <summary>
        /// Reads the data from an excel sheet and returns it as a Datatable. Based on column count (ignores names)
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="sheet"></param>
        /// <param name="expectedColumnCount"></param>
        /// <param name="hasHeader"></param>
        /// <param name="reread">Specifies if to reread the excel file.</param>
        /// <returns></returns>
        DataTable ReadSheet(string filePath, ExcelSheet sheet, int expectedColumnCount = 0, bool hasHeader = false, bool reread = true);
        #endregion
    }
}
