﻿using System.Collections.Generic;

namespace KG.Props.Documents.Excel.NPOI.Models
{
    public class ExcelSheet
    {
        public string SheetName { get; set; } = string.Empty;
        public List<string> Columns { get; set; } = new List<string>();
        public short? SheetColor { get; set; }

        public ExcelSheet()
        {

        }

        public ExcelSheet(ExcelSheet copy)
        {
            SheetName = copy.SheetName;
            SheetColor = copy.SheetColor;
            Columns = new List<string>();
            Columns.AddRange(copy.Columns);
        }
    }
}
