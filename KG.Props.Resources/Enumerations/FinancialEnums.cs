﻿namespace KG.Props.Resources.Enumerations
{
    public enum DayCountType
    {
        ACT_ACT = 1,
        ACT_365 = 2,
        ACT_360 = 3,
        EUR_30_360 = 4,
        ISDA_30_360 = 5
    };

    public enum CompoundType
    {
        Simple = 1,
        Annual = 2,
        Continuous = 3
    };

    public enum FrequencyTypes
    {
        None = 5,
        Years = 1,
        Months = 2,
        Weeks = 3,
        Days = 4
    };

    public enum BdrTypes
    {
        Following = 1,
        Preceding = 2,
        None = 3
    };

    public enum ConventionTypes
    {
        Regular = 1,
        Modified = 2,
        None = 3
    };

    public enum TypeValue
    {
        DiscountFactor = 1,
        All = 2
    };

    public enum InterpolationType
    {
        Linear = 1,
        Geometric = 2,
        Exponential = 3,
        Constant = 4
    };

    public enum DateGenMethodType
    {
        Backward = 1,
        Forward = 2
    };
}
