﻿using KG.Props.Resources.Enumerations;
using System;
using System.Collections.Generic;

namespace KG.Props.Resources.Values
{
    public class Constants
    {
        #region Formats and defaults
        public const string DefaultAcceptedDateFormat = "yyyy-MM-dd";
        public const string DefaultDisplayDateFormat = "MM/dd/yyyy";
        public const string DefaultCurrency = "EUR";
        public const string DefaultDataHome = "App_Data";
        public const string DefaultSavedFilesDirectory = "Saved Data";
        public const string DefaultSavedExcelsDirectory = "Excel Exports";
        public const string DefaultImportedExcelsDirectory = "Excel Imports";
        public const string DefaultImportedTextDirectory = "Text Imports";
        public const int DefaultKeepDataInHours = 12;
        public static readonly string[] DefaultForbiddenChars = { "*", ".", "/", "\\", "[", "]", ":", ";", "|", "=" };
        public static int[] ExpectedInflationYears = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 15, 20, 25, 30, 50 };
        public static string[] ExcelIgnoredValues = new[]
        {
            "#N/A",
            "#N/A N Ap",
            "#N/A N.A.",
            "#N/A History",
            "#N/A Sec",
            "#N/A Tim",
            "#N/A N/A"
        };
        public const string ExcelNumberFormat = "_-* #,##0.000000_-;-* #,##0.000000_-;_-* \"-\"??_-;_-@_-"; // formatted with \
        #endregion

        #region Types

        public static readonly Dictionary<FileTypes, string> FileMimeTypes = new Dictionary<FileTypes, string>()
        {
            { FileTypes.Excel, "application/vnd.ms-excel" }
        };

        public static readonly HashSet<Type> NumericTypes = new HashSet<Type>()
        {
            typeof(byte), typeof(sbyte), typeof(UInt16), typeof(UInt32), typeof(UInt64),
            typeof(int), typeof(Int16), typeof(Int32), typeof(Int64),
            typeof(decimal), typeof(double), typeof(Single), typeof(float), typeof(long)
        };

        public static readonly HashSet<Type> NumericDoubleTypes = new HashSet<Type>()
        {
            typeof(decimal), typeof(double), typeof(Single), typeof(float)
        };

        public static readonly HashSet<Type> DateTypes = new HashSet<Type>()
        {
            typeof(DateTime), typeof(TimeSpan)
        };

        #endregion Types
    }
}
