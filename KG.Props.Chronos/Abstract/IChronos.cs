﻿using System;

namespace KG.Props.Chronos.Abstract
{
    public interface IChronos<T, TResult>
    {
        /// <summary>
        /// Sets a new timeout (with the given interval).
        /// After the interval ends, it will execute the FUNC that is passed in the ToExecute method.
        /// By default, does this only one time (TIMEOUT behavior). If 'oneTimeOnly' is false, it can do it multiple times (INTERVAL behavior).
        /// </summary>
        /// <param name="toExecute"></param>
        /// <param name="interval"></param>
        /// <param name="data"></param>
        /// <param name="oneTimeOnly"></param>
        void SetTimeout(Func<T, TResult> toExecute, TimeSpan interval, T data, bool oneTimeOnly = true);

        /// <summary>
        /// Stops any future actions that the timer can do
        /// </summary>
        void StopTimer();
    }
}
