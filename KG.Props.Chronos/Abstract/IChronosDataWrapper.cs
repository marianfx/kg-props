﻿namespace KG.Props.Chronos.Abstract
{
    public interface IChronosDataWrapper
    {
        /// <summary>
        /// Returns true if a calculation is running.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        bool IsCalculationRunning(string guid);
    }
}
