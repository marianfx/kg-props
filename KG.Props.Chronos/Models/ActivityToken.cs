﻿using System.Collections.Generic;

namespace KG.Props.Chronos.Models
{
    public class ActivityToken
    {
        public string Guid { get; set; }
        public string User { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public dynamic Data { get; set; }
        public dynamic ErrorObject { get; set; }
        public bool IsFinished { get; set; } = false;
        public bool IsSuccess { get; set; } = false;
        public IList<string> Log { get; set; } = new List<string>();

        public ActivityToken()
        {
            Guid = string.Empty;
            Name = "Unknown";
            Type = "Unknown";
            User = string.Empty;
            Data = null;
            IsFinished = false;
            Log = new List<string>();
        }
    }
}
