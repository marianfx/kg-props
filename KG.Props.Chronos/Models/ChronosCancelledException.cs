﻿using System;

namespace KG.Props.Storage.Chronos.Models
{
    public class ChronosCancelledException : Exception
    {
        public ChronosCancelledException() { }
        public ChronosCancelledException(string message) : base(message) { }
    }
}
