﻿using KG.Props.Chronos.Abstract;
using KG.Props.Chronos.Models;
using KG.Props.IO.Abstract;
using KG.Props.Resources.Values;
using KG.Props.Storage.Chronos.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KG.Props.Chronos.Real
{
    public class ChronosDataWrapper: IChronosDataWrapper, IDisposable
    {
        public int HoursToKeepData { get; set; } = Constants.DefaultKeepDataInHours;
        public string SavedFilesDirectory { get; set; } = Constants.DefaultSavedFilesDirectory;
        public string SavedExcelsDirectory { get; set; } = Constants.DefaultSavedExcelsDirectory;
        public string ImportedExcelsDirectory { get; set; } = Constants.DefaultImportedExcelsDirectory;


        // https://docs.microsoft.com/en-us/dotnet/api/system.threading.readerwriterlockslim?view=netframework-4.7.2
        private ReaderWriterLockSlim _activeTimersLock;
        private Dictionary<string, Chronos<string, Task<bool>>> _activeTimers; // method; string is the key to the dictionary where to delete from

        private ReaderWriterLockSlim _activeCalculationsLock;
        private Dictionary<string, ActivityToken> _activeCalculations;

        private ReaderWriterLockSlim _activeExportsLock;
        private Dictionary<string, ActivityToken> _activeExports;

        private ReaderWriterLockSlim _lastTimeDeletedLock;
        private DateTime _lastTimeIDeletedFiles;

        private readonly IChronosSaver _dataSaver;
        private readonly IFileHandlerBase _fileHandler;
        private readonly string _contentPath;

        public ChronosDataWrapper(string contentPath, IChronosSaver chronosSaver, IFileHandlerBase fileHandler)
        {
            _dataSaver = chronosSaver;
            _fileHandler = fileHandler;
            _contentPath = contentPath;

            _activeTimersLock = new ReaderWriterLockSlim();
            _activeTimers = new Dictionary<string, Chronos<string, Task<bool>>>();

            _activeCalculationsLock = new ReaderWriterLockSlim();
            _activeCalculations = new Dictionary<string, ActivityToken>();

            _activeExportsLock = new ReaderWriterLockSlim();
            _activeExports = new Dictionary<string, ActivityToken>();

            _lastTimeDeletedLock = new ReaderWriterLockSlim();
            _lastTimeIDeletedFiles = DateTime.Now;
        }

        #region Calculation
        /// <summary>
        /// Specified if data exists
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public bool ContainsData(string guid)
        {
            return _dataSaver.DataExists(guid);
        }

        /// <summary>
        /// Returns stored data if it exists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="guid"></param>
        /// <returns></returns>
        public async Task<T> GetData<T>(string guid) where T : class
        {
            return await _dataSaver.GetData<T>(guid);
        }

        /// <summary>
        /// Adds the given response to the temp dictionary, with an unique GUID associated.
        /// Also starts the timer that will delete it from the dictionary in Settings.HoursToKeep
        /// </summary>
        /// <param name="response"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public async Task<string> SaveResponseAndSetDeleteTimeout(object response, string identifier = null)
        {
            // save data
            var guid = identifier ?? Guid.NewGuid().ToString();
            await _dataSaver.SaveDataAsync(response, guid);

            // mark calc. as not active
            var propsImportant = new List<string> { "Log", "IsSuccess" };
            var objProps = response.GetType().GetProperties().Where(x => propsImportant.Contains(x.Name));

            var logProp = objProps.FirstOrDefault(x => x.Name == "Log");
            var logValues = logProp != null ? logProp.GetValue(response) as IList<string> : null;

            var isSuccessProp = objProps.FirstOrDefault(x => x.Name == "IsSuccess");
            var isSuccessValue = isSuccessProp != null ? isSuccessProp.GetValue(response) as bool? : null;

            var log = logValues;
            var isSuccess = isSuccessValue ?? false;
            StopCalculation(guid, false, log, isSuccess);


            // set up timer
            _activeTimersLock.EnterWriteLock();
            try
            {
                if (_activeTimers == null)
                    _activeTimers = new Dictionary<string, Chronos<string, Task<bool>>>();

                var timer = new Chronos<string, Task<bool>>();
                timer.SetTimeout(DeleteExpiredData, TimeSpan.FromHours(HoursToKeepData), guid);
                _activeTimers.Add(guid, timer);
            }
            finally
            {
                _activeTimersLock.ExitWriteLock();
            }

            // clean used data
            GC.Collect();

            return guid;
        }

        /// <summary>
        /// Deletes, if found, an entry from the temp dictionary
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public async Task<bool> DeleteExpiredData(string guid)
        {
            // clear timer
            ClearTimer(guid);

            // remove active calculations
            StopCalculation(guid);

            // remove active exports
            ClearExport(guid);

            // try deleting older files
            ClearImportsExports();

            // clear data
            await _dataSaver.DeleteDataAsync(guid);

            // clean used data
            GC.Collect();

            return true;
        }

        /// <summary>
        /// Sets a guid as Running.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="user">todo: describe user parameter on SetActiveCalculation</param>
        /// <param name="data">todo: describe data parameter on SetActiveCalculation</param>
        /// <returns></returns>
        public void SetActiveCalculation(string guid, string user = null, dynamic data = null)
        {
            _activeCalculationsLock.EnterWriteLock();
            try
            {
                _activeCalculations = _activeCalculations ?? new Dictionary<string, ActivityToken>();
                if (_activeCalculations.ContainsKey(guid))
                    _activeCalculations.Remove(guid);

                _activeCalculations.Add(guid, new ActivityToken { Guid = guid, User = user, Data = data });
            }
            finally
            {
                _activeCalculationsLock.ExitWriteLock();
            }
        }

        public bool IsCalculationRunning(string guid)
        {
            _activeCalculationsLock.EnterReadLock();
            try
            {
                return _activeCalculations != null && _activeCalculations.TryGetValue(guid, out ActivityToken token) && !(token?.IsFinished ?? false);
            }
            finally
            {
                _activeCalculationsLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Removes a calculation from the active ones
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="remove">todo: describe remove parameter on StopCalculation</param>
        /// <param name="">todo: describe  parameter on StopCalculation</param>
        /// <param name="log">todo: describe log parameter on StopCalculation</param>
        /// <param name="isSuccess">todo: describe isSuccess parameter on StopCalculation</param>
        public void StopCalculation(string guid, bool remove = true, IList<string> log = null, bool isSuccess = false)
        {
            // update calculations
            _activeCalculationsLock.EnterWriteLock();
            try
            {
                if (_activeCalculations != null)
                {
                    if (remove)
                    {
                        if (_activeCalculations.ContainsKey(guid))
                            _activeCalculations.Remove(guid);
                    }
                    else
                    {
                        if (_activeCalculations.ContainsKey(guid))
                        {
                            if (_activeCalculations[guid] == null)
                                _activeCalculations[guid] = new ActivityToken();

                            _activeCalculations[guid].Log = log ?? new List<string>();
                            _activeCalculations[guid].IsSuccess = isSuccess;
                            _activeCalculations[guid].IsFinished = true;
                        }
                    }
                }
            }
            finally
            {
                _activeCalculationsLock.ExitWriteLock();
            }


            // export
            ClearExport(guid);
        }

        /// <summary>
        /// Returns the active calculation log if found
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public ActivityToken GetCalculationToken(string guid)
        {
            _activeCalculationsLock.EnterReadLock();
            try
            {
                ActivityToken token = null;
                if (_activeCalculations != null)
                    _activeCalculations.TryGetValue(guid, out token);

                return token;
            }
            finally
            {
                _activeCalculationsLock.ExitReadLock();
            }
        }
        #endregion

        #region Export

        /// <summary>
        /// Sets a guid as Export running.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="user">todo: describe user parameter on SetActiveExport</param>
        /// <returns></returns>
        public void SetActiveExport(string guid, string user = null)
        {
            _activeExportsLock.EnterWriteLock();
            try
            {
                _activeExports = _activeExports ?? new Dictionary<string, ActivityToken>();
                if (_activeExports.ContainsKey(guid))
                    _activeExports.Remove(guid);

                _activeExports.Add(guid, new ActivityToken { Guid = guid, User = user });
            }
            finally
            {
                _activeExportsLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Returns true if a export is running.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public bool IsExportRunning(string guid)
        {
            _activeExportsLock.EnterReadLock();
            try
            {
                return _activeExports != null && _activeExports.TryGetValue(guid, out ActivityToken token) && !(token?.IsFinished ?? false);
            }
            finally
            {
                _activeExportsLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Clears a guid from exports
        /// </summary>
        /// <param name="guid"></param>
        public void ClearExport(string guid)
        {
            _activeExportsLock.EnterWriteLock();
            try
            {
                if (_activeExports != null && _activeExports.ContainsKey(guid))
                    _activeExports.Remove(guid);
            }
            finally
            {
                _activeExportsLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Sets a guid as Export running.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="file">todo: describe file parameter on MarkExportFinished</param>
        /// <param name="user">todo: describe user parameter on MarkExportFinished</param>
        /// <param name="errors">todo: describe errors parameter on MarkExportFinished</param>
        /// <returns></returns>
        public void MarkExportFinished(string guid, string file, string user = null, dynamic errors = null)
        {
            _activeExportsLock.EnterWriteLock();
            try
            {
                _activeExports = _activeExports ?? new Dictionary<string, ActivityToken>();
                if (!_activeExports.ContainsKey(guid))
                    _activeExports.Add(guid, new ActivityToken { Guid = guid, User = user });

                _activeExports[guid].Data = file;
                _activeExports[guid].IsFinished = true;
                _activeExports[guid].ErrorObject = errors;
            }
            finally
            {
                _activeExportsLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Returns a file name if it exists or null otherwise (for an export job ID)
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string GetExportFileName(string guid)
        {
            _activeExportsLock.EnterReadLock();
            try
            {
                ActivityToken token = null;
                if (_activeExports != null)
                    _activeExports.TryGetValue(guid, out token);

                return (token?.Data as string) ?? "";
            }
            finally
            {
                _activeExportsLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Returns the errors / exceptions that happened at export
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public dynamic GetExportErrors(string guid)
        {
            _activeExportsLock.EnterReadLock();
            try
            {
                ActivityToken token = null;
                if (_activeExports != null)
                    _activeExports.TryGetValue(guid, out token);

                return token?.ErrorObject as dynamic;
            }
            finally
            {
                _activeExportsLock.ExitReadLock();
            }
        }
        #endregion

        #region Utilities
        /// <summary>
        /// Tries to delete all files from imports and exports provided directories which are older than the const hours to keep priced data.
        /// </summary>
        public void ClearImportsExports()
        {
            // check time passed
            _lastTimeDeletedLock.EnterReadLock();
            var difference = DateTime.Now - _lastTimeIDeletedFiles;
            _lastTimeDeletedLock.ExitReadLock();


            // delete if proper time passed
            if (difference.TotalHours > HoursToKeepData) // delete only if number of hours has passed
            {
                _lastTimeDeletedLock.EnterWriteLock();
                try
                {
                    var newChangedMaybeDifference = DateTime.Now - _lastTimeIDeletedFiles;
                    if (newChangedMaybeDifference.TotalHours > HoursToKeepData)
                    {
                        var excelImportsDir = Path.Combine(_contentPath, ImportedExcelsDirectory);
                        var excelExportsDir = Path.Combine(_contentPath, SavedExcelsDirectory);
                        var savedJsonsDir = Path.Combine(_contentPath, SavedFilesDirectory);
                        var dateToDelete = DateTime.Now.AddHours(-1 * HoursToKeepData);

                        // trigger file delete, don't wait for them to finish
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        _fileHandler.DeleteFilesFromDirOlderThanDate(excelImportsDir, dateToDelete);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        _fileHandler.DeleteFilesFromDirOlderThanDate(excelExportsDir, dateToDelete);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        _fileHandler.DeleteFilesFromDirOlderThanDate(savedJsonsDir, dateToDelete);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

                        _lastTimeIDeletedFiles = DateTime.Now;
                    }
                }
                finally
                {
                    _lastTimeDeletedLock.ExitWriteLock();
                }
            }
        }

        /// <summary>
        /// Clears the timer of a guid
        /// </summary>
        /// <param name="guid"></param>
        public void ClearTimer(string guid)
        {
            _activeTimersLock.EnterWriteLock();
            try
            {
                if (_activeTimers != null && _activeTimers.ContainsKey(guid))
                {
                    // stop timer too (otherwise might still execute)
                    if (_activeTimers[guid] != null)
                        _activeTimers[guid].Dispose();

                    _activeTimers[guid] = null;
                    _activeTimers.Remove(guid);
                }
            }
            finally
            {
                _activeTimersLock.ExitWriteLock();
            }
        }

        public void Dispose()
        {
            // dispose timers
            _activeTimersLock.EnterWriteLock();
            try
            {
                if (_activeTimers != null)
                {
                    foreach (var tmr in _activeTimers.Keys)
                        _activeTimers[tmr]?.Dispose();

                    _activeTimers.Clear();
                }
                _activeTimers = null;
            }
            finally
            {
                _activeTimersLock.ExitWriteLock();
            }

            // dispose active calculations
            _activeCalculationsLock.EnterWriteLock();
            try
            {
                if (_activeCalculations != null)
                {
                    _activeCalculations.Clear();
                }
                _activeCalculations = null;
            }
            finally
            {
                _activeCalculationsLock.ExitWriteLock();
            }

            // dispose active exports
            _activeExportsLock.EnterWriteLock();
            try
            {
                if (_activeExports != null)
                {
                    _activeExports.Clear();
                }
                _activeExports = null;
            }
            finally
            {
                _activeExportsLock.ExitWriteLock();
            }

            // dispose locks
            if (_activeCalculationsLock != null)
                _activeCalculationsLock.Dispose();
            if (_activeTimersLock != null)
                _activeTimersLock.Dispose();
            if (_activeExportsLock != null)
                _activeExportsLock.Dispose();

            _activeCalculationsLock = null;
            _activeTimersLock = null;
            _activeExportsLock = null;
        }
        #endregion
    }
}
