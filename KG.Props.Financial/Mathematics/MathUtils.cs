﻿using KG.Props.Resources.Text;
using System.Linq;

namespace KG.Props.Financial.Mathematics
{
    public class MathUtils
    {
        /// <summary>
        /// Converts the given value, based on exchange rate. Treats CASE 0 => 1
        /// </summary>
        /// <param name="value"></param>
        /// <param name="exchangeRate"></param>
        /// <returns></returns>
        public static double ConvertInEuro(double value, double exchangeRate)
        {
            if (exchangeRate == 0.0)
                exchangeRate = 1;

            return value / exchangeRate;
        }

        /// <summary>
        /// Calculates the product of two matrices
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns></returns>
        public static double[,] MatrixProduct(double[,] matrix1, double[,] matrix2)
        {
            var rowM1 = matrix1.GetLength(0);
            var colM1 = matrix1.GetLength(1);

            var rowM2 = matrix2.GetLength(0);
            var colM2 = matrix2.GetLength(1);

            if (colM1 != rowM2)
                throw new System.Exception(Literals.MatrixDimensionsDoNotMatch);

            double[,] result = new double[rowM1, colM2];

            for (int i = 0; i < rowM1; i++)
                for (int k = 0; k < colM2; k++)
                    for (int j = 0; j < colM1; j++)
                        result[i, k] += matrix1[i, j] * matrix2[j, k];

            return result;
        }

        /// <summary>
        /// Calculates a matrix transpose
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static double[,] MatrixTranspose(double[,] matrix)
        {
            var row = matrix.GetLength(0);
            var col = matrix.GetLength(1);

            double[,] result = new double[row, col];

            for (int i = 0; i <= row; i++)
                for (int j = 0; j < col; j++)
                    result[i, j] = matrix[j, i];

            return result;
        }

        /// <summary>
        /// Calculate the least squares solution of Ax = b
        /// </summary>
        /// <param name="A"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] GaussElim(double[,] A, double[] b)
        {
            var n = A.GetLength(0);

            for (int j = 1; j <= n; j++)
                for (int i = j; i <= n; i++)
                {
                    var m = A[i, j - 1] / A[j - 1, j - 1];
                    for (int k = 0; k <= n; k++)
                        A[i, k] = A[i, k] - A[j - 1, k] * m;
                    b[i] = b[i] - m * b[j - 1];
                }

            var X = new double[n];

            for (int i = 0; i <= n; i++)
                X[i] = 0;

            X[n] = b[n] / A[n, n];

            for (int j = n - 1; j >= 1; j--)
            {
                double ax = 0;
                for (int i = j + 1; i <= n; i++)
                    ax = ax + A[j, i] * X[i];
                X[j] = (b[j] - ax) / A[j, j];
            }

            return X;
        }

        /// <summary>
        /// Rebase on 100
        /// </summary>
        /// <param name="valueOrig"></param>
        /// <param name="valueOrig100"></param>
        /// <returns></returns>
        public static double RebaseOn100(double valueOrig, double valueOrig100)
        {
            double newValue = valueOrig / valueOrig100 * 100;
            return newValue;
        }

        public static double GetValueMin(double[] a1, double[] a2, double[] a3, bool hasA2, bool hasA3)
        {
            a1 = a1.Where(x => !double.IsNaN(x)).ToArray();
            var a1min = a1.Any() ? a1.Min() : 0;

            a2 = a2.Where(x => !double.IsNaN(x)).ToArray();
            var a2min = a2.Any() ? a2.Min() : 0;

            a3 = a3.Where(x => !double.IsNaN(x)).ToArray();
            var a3min = a3.Any() ? a3.Min() : 0;

            if (!hasA2 & !hasA3)
                return a1min;

            if (!hasA3)
                return System.Math.Min(a1min, a2min);

            return System.Math.Min(System.Math.Min(a1min, a2min), a3min);
        }

        public static double GetValueMax(double[] a1, double[] a2, double[] a3, bool hasA2, bool hasA3)
        {
            a1 = a1.Where(x => !double.IsNaN(x)).ToArray();
            var a1max = a1.Any() ? a1.Max() : 0;

            a2 = a2.Where(x => !double.IsNaN(x)).ToArray();
            var a2max = a2.Any() ? a2.Max() : 0;

            a3 = a3.Where(x => !double.IsNaN(x)).ToArray();
            var a3max = a3.Any() ? a3.Max() : 0;

            if (!hasA2 & !hasA3)
                return a1max;

            if (!hasA3)
                return System.Math.Min(a1max, a2max);

            return System.Math.Min(System.Math.Min(a1max, a2max), a3max);
        }
    }
}
