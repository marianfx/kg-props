﻿using KG.Props.Financial.Models;
using System.Collections.Generic;

namespace KG.Props.Financial.Extensions.Specifics
{
    public static class CurvesExtensions
    {
        /// <summary>
        /// Transforms a list of curve nodes into a Curve object
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static Curve AsCurve(this IList<CurveNode> list)
        {
            var curve = new Curve();
            curve.AddRange(list);
            return curve;
        }

        /// <summary>
        /// Transforms a list of price nodes into a Prices object
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static Prices AsPrices(this IList<PriceNode> list)
        {
            var curve = new Prices();
            curve.AddRange(list);
            return curve;
        }

        /// <summary>
        /// Transforms a list of details nodes into a DetailsList object
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static CurveDetailsList AsDetailsList(this IList<CurveDetailsNode> list)
        {
            var curve = new CurveDetailsList();
            curve.AddRange(list);
            return curve;
        }
    }
}
