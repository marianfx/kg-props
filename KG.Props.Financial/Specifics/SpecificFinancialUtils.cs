﻿using KG.Props.Financial.Models;
using KG.Props.Financial.Models.Bi;
using KG.Props.Resources.Enumerations;
using KG.Props.Resources.Text;
using KG.Props.Resources.Values;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KG.Props.Financial.Specifics
{
    class SpecificFinancialUtils
    {
        #region TimeFactorManager
        public static double ComputeTimeFactor(DateTime startDate, DateTime endDate, int dayCount)
        {
            if (startDate.ToOADate() == endDate.ToOADate())
                return 0;

            var days = endDate.ToOADate() - startDate.ToOADate();

            switch (dayCount)
            {
                case (int)DayCountType.ACT_360:
                    return days / 360.0;
                case (int)DayCountType.ACT_365:
                    return days / 365.0;
                case (int)DayCountType.ACT_ACT:
                    return GetTimeFactorActAct(startDate, endDate);
                case (int)DayCountType.ISDA_30_360:
                    return GetTimeFactorIsda(startDate, endDate);
                case (int)DayCountType.EUR_30_360:
                    return GetTimeFactorEur(startDate, endDate);
                default:
                    throw new Exception("Invalid day count type when computing Time Factor.");
            }
        }

        #region Utilities
        private static double GetTimeFactorActAct(DateTime startDate, DateTime endDate)
        {
            var pseudoDate1 = new DateTime(startDate.Year, 12, 31);
            var pseudoDate2 = new DateTime(endDate.Year - 1, 12, 31);
            var year = endDate.Year - startDate.Year - 1;
            double days1 = pseudoDate1.ToOADate() - startDate.ToOADate();
            double days2 = endDate.ToOADate() - pseudoDate2.ToOADate();

            var divisor1 = DateTime.IsLeapYear(startDate.Year) ? 366.0 : 365.0;
            var divisor2 = DateTime.IsLeapYear(endDate.Year) ? 366.0 : 365.0;

            // 0 division check
            days1 = days1 / divisor1;
            days2 = days2 / divisor2;
            return days1 + year + days2;
        }

        private static double GetTimeFactorIsda(DateTime startDate, DateTime endDate)
        {
            var startDateDay = startDate.Day;
            var endDateDay = endDate.Day;

            // adjust for endDate
            if (endDate.Month == 2)
            {
                if (endDate.Day == 28 || endDate.Day == 29)
                {
                    if (startDate.Day == 28 || startDate.Day == 29)
                    {
                        endDateDay = 30;
                    }
                }
            }
            else
            {
                if (endDate.Day == 31)
                    if (startDate.Day == 30 || startDate.Day == 31)
                        endDateDay = 30;
            }

            // adjust for startDate
            if (startDate.Month == 2)
            {
                if (startDate.Day == 28 || startDate.Day == 29)
                    startDateDay = 30;
            }
            else
            {
                if (startDate.Day == 31)
                    startDateDay = 30;
            }

            return ((endDate.Year - startDate.Year) * 360.0 + (endDate.Month - startDate.Month) * 30.0 + (endDateDay - startDateDay)) / 360.0;

        }

        private static double GetTimeFactorEur(DateTime startDate, DateTime endDate)
        {
            var startDateDay = startDate.Day;
            var endDateDay = endDate.Day;

            if (endDate.Day == 31)
                endDateDay = 30;

            if (startDate.Day == 31)
                startDateDay = 30;

            return ((endDate.Year - startDate.Year) * 360.0 + (endDate.Month - startDate.Month) * 30.0 + (endDateDay - startDateDay)) / 360.0;
        }
        #endregion
        #endregion

        #region FinancialUtilitiesManager
        public static double ComputeCompoundingFactor(double rate, DateTime startDate, DateTime endDate, int dayCount, int compound)
        {
            var time = ComputeTimeFactor(startDate, endDate, dayCount);

            switch (compound)
            {
                case (int)CompoundType.Simple:
                    return 1 + rate * time;
                case (int)CompoundType.Annual:
                    return System.Math.Pow((1 + rate), time);
                case (int)CompoundType.Continuous:
                    return System.Math.Exp(rate * time);
                default:
                    throw new Exception(Literals.ComputeCompoundingFactorInvalidCompound);
            }
        }

        public static double DiscountFactorToRate(double discountFactor, DateTime startDate, DateTime endDate, int dayCount, int compound)
        {
            var time = ComputeTimeFactor(startDate, endDate, dayCount);

            var onetimesdisc = 1 / discountFactor;
            var onetimestime = 1 / time;

            switch (compound)
            {
                case (int)CompoundType.Simple:
                    return (onetimesdisc - 1) * onetimestime;
                case (int)CompoundType.Annual:
                    return System.Math.Pow(onetimesdisc, onetimestime) - 1.0;
                case (int)CompoundType.Continuous:
                    return -1 * System.Math.Log(discountFactor) / time;
                default:
                    throw new Exception(Literals.DiscountFactorToRatesInvalidCompound);
            }
        }

        public static double RateToDiscountFactor(double rate, DateTime startDate, DateTime endDate, int dayCount, int compound)
        {
            var time = ComputeTimeFactor(startDate, endDate, dayCount);

            switch (compound)
            {
                case (int)CompoundType.Simple:
                    return 1 / (1 + (rate * time));
                case (int)CompoundType.Annual:
                    return System.Math.Pow(1 / (1 + rate), time);
                case (int)CompoundType.Continuous:
                    return System.Math.Exp(-rate * time);
                default:
                    throw new Exception(Literals.RateToDiscountFactorInvalidCompound);
            }
        }
        #endregion

        #region InterpolationManager
        public static double InterpolationOnArray(Curve inputCurve, DateTime xdate, int typeInterpolation, int typeValue)
        {
            if (inputCurve == null)
                return 0;

            var n = inputCurve.Count - 1;
            var nSize = n + 1;

            var dates = new DateTime[nSize];
            var values = new double[nSize];

            for (var i = 0; i <= n; i++)
            {
                dates[i] = inputCurve[i].ValueDate;
                values[i] = inputCurve[i].Value;
            }

            return InterpolationOnArray(dates, values, xdate, typeInterpolation, typeValue);
        }

        public static double InterpolationOnArray(DateTime[] dates, double[] values, DateTime xdate, int typeInterpolation, int typeValue)
        {
            try
            {
                var n = dates.Length - 1;

                // Minor
                if (xdate <= dates[0])
                    return ConstantInterpolation(dates[0].ToOADate(), dates[0].ToOADate(), values[0], values[0], xdate.ToOADate());

                // Major
                if (xdate >= dates[n])
                {
                    if (typeValue == (int)TypeValue.All)
                        return ConstantInterpolation(dates[n].ToOADate(), dates[n].ToOADate(), values[n], values[n], xdate.ToOADate());
                    else
                    {
                        var lastRate = DiscountFactorToRate(values[n], dates[0], dates[n], (int)DayCountType.ACT_365, (int)CompoundType.Continuous);
                        lastRate = ConstantInterpolation(dates[n].ToOADate(), dates[n].ToOADate(), lastRate, lastRate, xdate.ToOADate());
                        return RateToDiscountFactor(lastRate, dates[0], xdate, (int)DayCountType.ACT_365, (int)CompoundType.Continuous);
                    }
                }

                var index = FindIndexInArrayOfDates(dates, xdate);

                if (index != -1) return values[index];

                var i = 0;
                while (i < dates.Count() && dates[i].ToOADate() < xdate.ToOADate()) i++;
                i -= 1;

                return Interpolation(dates[i].ToOADate(), dates[i + 1].ToOADate(), values[i], values[i + 1], xdate.ToOADate(), typeInterpolation);
            }
            catch (Exception ex)
            {
                throw new Exception("Interpolation Matrix: " + ex.Message);
            }
        }

        public static double InterpolationOnMatrix(CurveBidimensional inputCurve, DateTime xdate, double xyear, int typeInterpolation, int typeValue)
        {
            var n = inputCurve.CountYears() - 1;
            var m = inputCurve.CountDates() - 1;
            var nSize = n + 1;
            var mSize = m + 1;

            var years = new double[nSize];
            var dates = new DateTime[mSize];
            var values = new double[nSize, mSize];

            for (var i = 0; i <= n; i++)
                years[i] = inputCurve[i * (m + 1)].Year;

            for (var j = 0; j <= m; j++)
                dates[j] = inputCurve[j].ValueDate;

            for (var i = 0; i <= n; i++)
                for (var j = 0; j <= m; j++)
                    values[i, j] = inputCurve[i * (m + 1) + j].Value;

            return InterpolationOnMatrix(dates, years, values, xdate, xyear, typeInterpolation);
        }

        public static double InterpolationOnMatrix(DateTime[] dates, double[] years, double[,] values, DateTime xdate, double xyear, int typeInterpolation)
        {
            try
            {
                int i = 0, j = 0;
                double x1 = 0, x2 = 0, y1 = 0, y2 = 0;

                xyear = xyear < years[0]
                    ? years[0]
                    : xyear > years[years.Length - 1] ? years[years.Length - 1] : xyear;

                xdate = xdate.ToOADate() < dates[0].ToOADate()
                    ? dates[0]
                    : xdate.ToOADate() > dates[dates.Length - 1].ToOADate() ? dates[dates.Length - 1] : xdate;

                var indexDates = FindIndexInArrayOfDates(dates, xdate);
                var indexYears = FindIndexInArrayOfDouble(years, xyear);

                if (indexDates != -1 && indexYears != -1)
                    return values[indexYears, indexDates];

                if (indexDates == -1 && indexYears != -1)
                {
                    while (i < dates.Count() && dates[i].ToOADate() < xdate.ToOADate())
                        i++;
                    i -= 1;

                    x1 = dates[i].ToOADate();
                    x2 = dates[i + 1].ToOADate();
                    y1 = values[indexYears, i];
                    y2 = values[indexYears, i + 1];

                    return Interpolation(x1, x2, y1, y2, xdate.ToOADate(), typeInterpolation);
                }

                if (indexDates != -1 && indexYears == -1)
                {
                    while (j < years.Count() && years[j] < xyear)
                        j++;
                    j -= 1;

                    x1 = years[j];
                    x2 = years[j + 1];
                    y1 = values[j, indexDates];
                    y2 = values[j + 1, indexDates];

                    return Interpolation(x1, x2, y1, y2, xyear, typeInterpolation);
                }

                i = 0;
                while (i < dates.Count() && dates[i].ToOADate() < xdate.ToOADate())
                    i++;
                i -= 1;

                j = 0;
                while (j < years.Count() && years[j] < xyear)
                    j++;
                j -= 1;

                x1 = dates[i].ToOADate();
                x2 = dates[i + 1].ToOADate();

                y1 = values[j, i];
                y2 = values[j, i + 1];

                var value1 = Interpolation(x1, x2, y1, y2, xdate.ToOADate(), typeInterpolation);

                y1 = values[j + 1, i];
                y2 = values[j + 1, i + 1];

                var value2 = Interpolation(x1, x2, y1, y2, xdate.ToOADate(), typeInterpolation);

                x1 = years[j];
                x2 = years[j + 1];

                return Interpolation(x1, x2, value1, value2, xyear, typeInterpolation);
            }
            catch (Exception ex)
            {
                throw new Exception("Interpolation Matrix: " + ex.Message);
            }
        }

        public static double Interpolation(double x1, double x2, double y1, double y2, double x, int typeInterpolation)
        {
            switch (typeInterpolation)
            {
                case (int)InterpolationType.Linear:
                    return LinearInterpolation(x1, x2, y1, y2, x);
                case (int)InterpolationType.Geometric:
                    return GeometricInterpolation(x1, x2, y1, y2, x);
                case (int)InterpolationType.Exponential:
                    return ExponentialInterpolation(x1, x2, y1, y2, x);
                case (int)InterpolationType.Constant:
                    return ConstantInterpolation(x1, x2, y1, y2, x);
                default:
                    throw new Exception(Literals.InvalidInterpolationType);
            }
        }

        private static double LinearInterpolation(double x1, double x2, double y1, double y2, double x)
        {
            return (y1 * (x2 - x) + y2 * (x - x1)) / (x2 - x1);
        }

        private static double ExponentialInterpolation(double x1, double x2, double y1, double y2, double x)
        {
            return Math.Log((1 / (x2 - x1)) * (Math.Exp(y1) * (x2 - x) + Math.Exp(y2) * (x - x1)));
        }

        private static double GeometricInterpolation(double x1, double x2, double y1, double y2, double x)
        {
            return (Math.Pow(y1, ((x2 - x) / (x2 - x1)))) * (Math.Pow(y2, ((x - x1) / (x2 - x1))));
        }

        private static double ConstantInterpolation(double x1, double x2, double y1, double y2, double x)
        {
            return y1;
        }
        #endregion

        #region PaymentDatesManager

        /// <summary>
        /// Calculates payment dates
        /// </summary>
        /// <param name="valueDate"></param>
        /// <param name="issueDate"></param>
        /// <param name="maturityDate"></param>
        /// <param name="frequencyNumber"></param>
        /// <param name="frequencyType"></param>
        /// <param name="bdr"></param>
        /// <param name="convention"></param>
        /// <param name="dayShift"></param>
        /// <param name="dateGenerationMethod"></param>
        /// <param name="holidayCalendar"></param>
        /// <returns></returns>
        public static DateTime[] ComputePaymentDates(DateTime valueDate, DateTime issueDate, DateTime maturityDate, int frequencyNumber, int frequencyType, int bdr, int convention, int dayShift, int dateGenerationMethod, ICollection<DateTime> holidayCalendar)
        {
            var paymentDates = new List<DateTime>();
            var calendarDates = new HolidayCalendar(holidayCalendar);

            switch (dateGenerationMethod)
            {
                case (int)DateGenMethodType.Backward:
                    paymentDates = ComputePaymentDateBackward(valueDate, issueDate, maturityDate, frequencyNumber, frequencyType).ToList();
                    break;
                case (int)DateGenMethodType.Forward:
                    break;
                default:
                    throw new Exception(Literals.InvalidDateGenerationMethod);
            }

            var n = paymentDates.Count - 1;
            for (var i = 0; i <= n; i++)
                paymentDates[i] = calendarDates.ApplyBusinessDayRule(paymentDates[i], bdr, convention, dayShift, HolidayCalendar.DefaultCurrency);

            return paymentDates.ToArray();
        }

        /// <summary>
        /// Calculate the backward coupon plan
        /// </summary>
        /// <param name="valueDate"></param>
        /// <param name="issueDate"></param>
        /// <param name="maturityDate"></param>
        /// <param name="frequencyNumber"></param>
        /// <param name="frequencyType"></param>
        /// <returns></returns>
        public static DateTime[] ComputePaymentDateBackward(DateTime valueDate, DateTime issueDate, DateTime maturityDate, int frequencyNumber, int frequencyType)
        {
            var reverseDates = new List<DateTime>();
            var n = 0;
            var d = new DateTime();

            do
            {
                switch (frequencyType)
                {
                    case (int)FrequencyTypes.Years:
                        d = maturityDate.AddYears(-1 * frequencyNumber * n);
                        break;
                    case (int)FrequencyTypes.Months:
                        d = maturityDate.AddMonths(-1 * frequencyNumber * n);
                        break;
                    case (int)FrequencyTypes.Weeks:
                        d = maturityDate.AddDays(-1 * frequencyNumber * 7 * n);
                        break;
                    case (int)FrequencyTypes.Days:
                        d = maturityDate.AddDays(-1 * frequencyNumber * n);
                        break;
                    case (int)FrequencyTypes.None:
                        throw new Exception(Literals.FrequencyTypeNone);
                    default:
                        throw new Exception(Literals.InvalidFrequencyType);
                }
                reverseDates.Add(d);
                n += 1;
            } while (d.ToOADate() > valueDate.ToOADate() && d.ToOADate() > issueDate.ToOADate());

            reverseDates.RemoveAt(n - 1);
            n -= 1;

            if (issueDate.ToOADate() > valueDate.ToOADate())
            {
                reverseDates.Add(issueDate);
                n += 1;
            }

            reverseDates.Add(valueDate);
            n += 1;

            var paymentDates = new DateTime[n];
            for (var i = 0; i < n; i++)
                paymentDates[i] = reverseDates[n - i - 1];
            return paymentDates;
        }

        /// <summary>
        /// Adds frequency to a date
        /// </summary>
        /// <param name="d"></param>
        /// <param name="frequencyNumber"></param>
        /// <param name="frequencyType"></param>
        /// <returns></returns>
        public static DateTime AddTenorToDate(DateTime d, int frequencyNumber, int frequencyType)
        {
            DateTime myDate;
            switch (frequencyType)
            {
                case (int)FrequencyTypes.Years:
                    myDate = d.AddYears(frequencyNumber);
                    break;
                case (int)FrequencyTypes.Months:
                    myDate = d.AddMonths(frequencyNumber);
                    break;
                case (int)FrequencyTypes.Weeks:
                    myDate = d.AddDays(frequencyNumber * 7);
                    break;
                case (int)FrequencyTypes.Days:
                    myDate = d.AddDays(frequencyNumber);
                    break;
                case (int)FrequencyTypes.None:
                    myDate = d;
                    break;
                default:
                    throw new Exception(Literals.InvalidFrequencyType);
            }
            return myDate;
        }

        /// <summary>
        /// Returns the fraction of a year in frequency
        /// </summary>
        /// <param name="frequencyNumber"></param>
        /// <param name="frequencyType"></param>
        /// <returns></returns>
        public static double FrequencyToYears(int frequencyNumber, int frequencyType)
        {
            switch (frequencyType)
            {
                case (int)FrequencyTypes.Years:
                    return frequencyNumber * 1.0;
                case (int)FrequencyTypes.Months:
                    return frequencyNumber / 12.0;
                case (int)FrequencyTypes.Weeks:
                    return frequencyNumber / 52.0;
                case (int)FrequencyTypes.Days:
                    return frequencyNumber / 365.0;
                case (int)FrequencyTypes.None:
                    return 100.0;
                default:
                    throw new Exception(Literals.InvalidFrequencyType);
            }
        }

        /// <summary>
        /// Calculate the last coupon date
        /// </summary>
        /// <param name="valueDate"></param>
        /// <param name="issueDate"></param>
        /// <param name="maturityDate"></param>
        /// <param name="frequencyNumber"></param>
        /// <param name="frequencyType"></param>
        /// <param name="BDR"></param>
        /// <param name="convention"></param>
        /// <param name="dayShift"></param>
        /// <param name="dateGenMethod"></param>
        /// <param name="holidayCalendar"></param>
        /// <returns></returns>
        public static DateTime ComputeLastCouponDate(DateTime valueDate, DateTime issueDate, DateTime maturityDate, int frequencyNumber, int frequencyType, int BDR, int convention,
            int dayShift, int dateGenMethod, ICollection<DateTime> holidayCalendar)
        {
            if (valueDate < issueDate || valueDate >= maturityDate)
                return new DateTime(0);

            var paymentDatesAllCF = ComputePaymentDates(issueDate, issueDate, maturityDate, frequencyNumber, frequencyType, BDR, convention, dayShift, dateGenMethod, holidayCalendar);
            var n = paymentDatesAllCF.Length - 1;

            for (var i = 0; i <= n - 1; i++)
                if (valueDate >= paymentDatesAllCF[i] && valueDate < paymentDatesAllCF[i + 1])
                    return paymentDatesAllCF[i];

            return new DateTime(0);
        }

        /// <summary>
        /// Adds working days
        /// </summary>
        /// <param name="dateToAdj"></param>
        /// <param name="nDay"></param>
        /// <param name="holidayCalendar"></param>
        /// <returns></returns>
        public static DateTime AddBusyDay(DateTime dateToAdj, int nDay, ICollection<DateTime> holidayCalendar)
        {
            var dateAdj = dateToAdj;
            var calendar = new HolidayCalendar(holidayCalendar);

            var bdr = BdrTypes.Following;
            var day = 1;
            if (nDay < 0)
            {
                bdr = BdrTypes.Preceding;
                day = -1;
            }

            for (var i = 0; i < Math.Abs(nDay); i++)
                dateAdj = calendar.ApplyBusinessDayRule(dateAdj.AddDays(day), (int)bdr, (int)ConventionTypes.Regular, 0, Constants.DefaultCurrency);

            return dateAdj;
        }
        #endregion

        #region CmsManager
        /// <summary>
        /// Computes the CMS rate
        /// </summary>
        /// <param name="paymentDatesCMS"></param>
        /// <param name="dfIssueDate"></param>
        /// <param name="dfMaturityDate"></param>
        /// <param name="indexCurve"></param>
        /// <param name="interpolationMethod"></param>
        /// <param name="dayCount"></param>
        /// <returns></returns>
        public static double CmsRate(DateTime[] paymentDatesCMS, double dfIssueDate, double dfMaturityDate, Curve indexCurve, int interpolationMethod, int dayCount)
        {
            double numerator = dfIssueDate - dfMaturityDate;
            double denominator = 0;

            for (var t = 1; t < paymentDatesCMS.Length; t++)
            {
                double tenor = ComputeTimeFactor(paymentDatesCMS[t - 1], paymentDatesCMS[t], dayCount);
                var df = InterpolationOnArray(indexCurve, paymentDatesCMS[t], interpolationMethod, (int)TypeValue.DiscountFactor) * tenor;
                denominator += df;
            }


            return numerator / denominator;
        }

        /// <summary>
        /// Computes the interpolation of the curve in term volatilidy
        /// </summary>
        /// <param name="ttVola"></param>
        /// <param name="issueDate"></param>
        /// <param name="slidingTerm"></param>
        /// <param name="interpolationMethod"></param>
        /// <returns></returns>
        public static double CmsTermTermVola(CurveBidimensional ttVola, DateTime issueDate, double slidingTerm, int interpolationMethod)
        {
            return InterpolationOnMatrix(ttVola, issueDate, slidingTerm, interpolationMethod, (int)TypeValue.DiscountFactor);
        }

        /// <summary>
        /// Computes the convexity adjustment with a new method
        /// </summary>
        /// <param name="valueDate"></param>
        /// <param name="issueDate"></param>
        /// <param name="cmsRate"></param>
        /// <param name="cmsVola"></param>
        /// <param name="slidingTerm"></param>
        /// <param name="frequencyType"></param>
        /// <param name="frequencyNumber"></param>
        /// <param name="dayCount"></param>
        /// <returns></returns>
        public static double CmsConvAdjustHull(DateTime valueDate, DateTime issueDate, double cmsRate, double cmsVola, Double slidingTerm, int frequencyType, double frequencyNumber, int dayCount)
        {
            var issueTime = ComputeTimeFactor(valueDate, issueDate, dayCount);
            // double G = 0;
            double Gd = 0, Gdd = 0;
            double q = 1;

            switch (frequencyType)
            {
                case (int)FrequencyTypes.Years:
                    q = frequencyNumber;
                    break;
                case (int)FrequencyTypes.Months:
                    q = frequencyNumber / 12.0;
                    break;
                case (int)FrequencyTypes.Weeks:
                    q = frequencyNumber / 52.0;
                    break;
                case (int)FrequencyTypes.Days:
                    q = frequencyNumber / 365.0;
                    break;
            }

            for (var j = 1; j <= slidingTerm; j++)
            {
                // G = G + (cmsRate / q) / Math.Pow(1 + cmsRate / q, j);
                Gd = Gd + (cmsRate / q) * j / Math.Pow(1 + cmsRate / q, j);
                Gdd = Gdd + (cmsRate / q) * j * (j + 1) / Math.Pow(1 + cmsRate / q, j);
            }

            // G = G + 1 / Math.Pow(1 + cmsRate / q, slidingTerm);
            Gd = (-1 / (q * (1 + cmsRate / q))) * (Gd + slidingTerm / Math.Pow(1 + cmsRate / q, slidingTerm));
            Gdd = (1 / (Math.Pow(q * (1 + cmsRate / q), 2))) * (Gdd + slidingTerm * (slidingTerm + 1) / Math.Pow(1 + cmsRate / q, slidingTerm));

            return -(Gdd / (2 * Gd)) * cmsRate * cmsRate * cmsVola * cmsVola * issueTime;
        }
        #endregion

        #region FloatingManager
        // can be removed
        public static double FloatingTermVola(Curve tVola, DateTime paymentDate, int interpolationMethod)
        {
            return InterpolationOnArray(tVola, paymentDate, interpolationMethod, (int)TypeValue.All);
        }
        #endregion

        #region StatisticsManager
        public static double CND(double z)
        {
            if (z < -8.0)
                return 0.0;

            if (z > 8.0)
                return 1.0;

            double sum = 0, term = z;
            var i = 3;
            while ((sum + term) != sum && !double.IsNaN(sum)) // prevent infinite loop (in old app caused overflow)
            {
                sum += term;
                term = term * z * z / i;
                i += 2;
            }
            if (double.IsNaN(sum))
                throw new ArithmeticException("Arithmetic overflow on CND.");

            return 0.5 + sum * Phi(z);
        }

        public static double Phi(double x)
        {
            return Math.Exp(-x * x / 2) / Math.Sqrt(2 * Math.PI);
        }

        #endregion

        #region OptionManager
        /// <summary>
        /// Calculates the pay off of the caplet
        /// </summary>
        /// <param name="valueDate"></param>
        /// <param name="fixingDate"></param>
        /// <param name="underlying"></param>
        /// <param name="strike"></param>
        /// <param name="vola"></param>
        /// <returns></returns>
        public static double PayOffCaplet(DateTime valueDate, DateTime fixingDate, Double underlying, double strike, double vola)
        {
            if (underlying == 0)
                return 0;

            var timeToFixing = ComputeTimeFactor(valueDate, fixingDate, (int)DayCountType.ACT_365);
            var volax = vola * Math.Sqrt(timeToFixing);
            var undrstrike = underlying / strike;

            var d1 = (Math.Log(undrstrike) + ((Math.Pow(vola, 2) * timeToFixing) / 2)) / volax;
            var d2 = d1 - (vola * Math.Sqrt(timeToFixing));

            return underlying * CND(d1) - strike * CND(d2);
        }

        /// <summary>
        /// Calculate the payoff of the floorlet
        /// </summary>
        /// <param name="valueDate"></param>
        /// <param name="fixingDate"></param>
        /// <param name="underlying"></param>
        /// <param name="strike"></param>
        /// <param name="vola"></param>
        /// <returns></returns>
        public static double PayOffFloorlet(DateTime valueDate, DateTime fixingDate, double underlying, double strike, double vola)
        {
            if (underlying <= 0)
                return strike;

            var timeToFixing = ComputeTimeFactor(valueDate, fixingDate, (int)DayCountType.ACT_365);
            var volax = vola * Math.Sqrt(timeToFixing);
            var undrstrike = underlying / strike;

            var d1 = (Math.Log(undrstrike) + ((Math.Pow(vola, 2) * timeToFixing) / 2)) / volax;
            var d2 = d1 - (vola * Math.Sqrt(timeToFixing));

            return -underlying * CND(-d1) + strike * CND(-d2);
        }

        // public static double BondCallOption(DateTime marketDate, DateTime valueDate, )
        // public static double BondPutOption
        #endregion

        #region InflationLinkedManager
        /// <summary>
        /// Interpolation function on a Curve on a certain date
        /// If I want to interpolate on a certain date I have to go 3 months before
        /// </summary>
        /// <param name="paymentDate"></param>
        /// <param name="indexProjection"></param>
        /// <returns></returns>
        public static double IndexValueInterpolation(DateTime paymentDate, Curve indexProjection)
        {
            if (indexProjection == null)
                return -1;

            var interpolationDate = paymentDate.AddMonths(-3);
            var interpolationMonth = interpolationDate.Month;
            var interpolationYear = interpolationDate.Year;

            var lengthMonth = GoToEndMonth(paymentDate).Day;

            var i = 0;
            while (i < indexProjection.Count && (indexProjection[i].ValueDate.Month != interpolationMonth || indexProjection[i].ValueDate.Year != interpolationYear))
                i += 1;

            if (i == indexProjection.Count - 1)
                return -1;

            var paymonth = (paymentDate.Day - 1) / (double)lengthMonth;
            return indexProjection[i].Value + paymonth * (indexProjection[i + 1].Value - indexProjection[i].Value);
        }

        /// <summary>
        /// Given the historical series of the representative index of inflation, it projects it forward over time based on certain parameters
        /// </summary>
        /// <param name="indexExpectedValue"></param>
        /// <param name="maturityDate"></param>
        /// <param name="indexPerform"></param>
        /// <param name="inflationTimeSeries"></param>
        /// <returns></returns>
        public static Curve IndexProjection(double indexExpectedValue, DateTime maturityDate, IndexPerform indexPerform, Curve inflationTimeSeries)
        {
            if (inflationTimeSeries == null || inflationTimeSeries.Count == 0)
                return inflationTimeSeries;

            var adjMaturityDate = GoToEndMonth(maturityDate);

            // Extract the last known date
            var lastNode = inflationTimeSeries.Last();

            // Generate the dates of the future curves
            var futureDates = ComputePaymentDateBackward(lastNode.ValueDate, lastNode.ValueDate, adjMaturityDate, indexPerform.NumberFrequency, (int)indexPerform.Frequency.Value);

            // Creates the new historical series up to the maturity date
            var newTimeSeries = inflationTimeSeries.Clone() as Curve;

            for (var i = 1; i < futureDates.Length; i++)
            {
                var n = newTimeSeries.Count - 1;

                // Creates the values
                var date1 = newTimeSeries[n].ValueDate;
                var value1 = newTimeSeries[n].Value;

                var date2 = futureDates[i];
                var value2 = value1 * Math.Pow((1 + indexExpectedValue), ComputeTimeFactor(date1, date2, (int)indexPerform.DayCount.Value));

                // Create the node
                var node = new CurveNode(date2, value2);
                newTimeSeries.Add(node);
            }

            return newTimeSeries;
        }

        /// <summary>
        /// Calculates the value of the expected value of the bond inflation link
        /// </summary>
        /// <param name="marketDate"></param>
        /// <param name="valueDate"></param>
        /// <param name="maturityDate"></param>
        /// <param name="expectedInflation"></param>
        /// <returns></returns>
        public static double GetExpectedValue(DateTime marketDate, DateTime valueDate, DateTime maturityDate, Curve expectedInflation)
        {
            var i1 = InterpolationOnArray(expectedInflation, valueDate, (int)InterpolationType.Linear, (int)TypeValue.All);
            var i2 = InterpolationOnArray(expectedInflation, maturityDate, (int)InterpolationType.Linear, (int)TypeValue.All);

            var timeToMaturity = ComputeTimeFactor(marketDate, maturityDate, (int)DayCountType.ACT_365);
            var timeToValueDate = ComputeTimeFactor(marketDate, valueDate, (int)DayCountType.ACT_365);
            var time = ComputeTimeFactor(valueDate, maturityDate, (int)DayCountType.ACT_365);

            return (i2 * timeToMaturity - i1 * timeToValueDate) / time;
        }
        #endregion

        #region Pricing

        /// <summary>
        /// Calculate the coupon rate when the coupon type is floating
        /// </summary>
        /// <param name="df1"></param>
        /// <param name="df2"></param>
        /// <param name="tenor"></param>
        /// <param name="spread"></param>
        /// <returns></returns>
        public static double ComputeFloatingRate(double df1, double df2, double tenor, double spread)
        {
            var fwdf = df2 / df1;
            var tnr = 1 / tenor;
            return (((1 / fwdf) - 1) * tnr) + spread;
        }


        /// <summary>
        /// Calculate a coupon rate indexed to an index (inflation linked)
        /// </summary>
        /// <param name="paymentDate"></param>
        /// <param name="indexindexProjection"></param>
        /// <param name="indexBaseValue"></param>
        /// <param name="coupon"></param>
        /// <returns></returns>
        public static double ComputeIndexPerfRate(DateTime paymentDate, Curve indexindexProjection, double indexBaseValue, double coupon)
        {
            var indexValue = IndexValueInterpolation(paymentDate, indexindexProjection);
            var result = (coupon * indexValue / indexBaseValue);
            return result;
        }


        #endregion


        #region Utility
        private static int FindIndexInArrayOfDouble(double[] array, double input)
        {
            if (array == null) return -1;

            for (int i = 0; i < array.Length; i++)
                if (array[i] == input)
                    return i;

            return -1;
        }

        private static int FindIndexInArrayOfDates(DateTime[] array, DateTime input)
        {
            if (array == null) return -1;

            for (int i = 0; i < array.Length; i++)
                if (array[i].ToOADate() == input.ToOADate())
                    return i;

            return -1;
        }

        private static DateTime GoToEndMonth(DateTime inputDate)
        {
            int day;

            var thirtyOneMonths = new List<int>() { 1, 3, 5, 7, 8, 10, 12 };
            if (thirtyOneMonths.Any(month => month == inputDate.Month))
                day = 31;
            else if (inputDate.Month == 2)
                day = DateTime.IsLeapYear(inputDate.Year) ? 29 : 28;
            else
                day = 30;

            return new DateTime(inputDate.Year, inputDate.Month, day);
        }
        #endregion
    }
}
