﻿using KG.Props.Extensions.DataTables;
using KG.Props.Financial.Extensions.Specifics;
using KG.Props.Financial.Models;
using KG.Props.Resources.Enumerations;
using KG.Props.Resources.Text;
using KG.Props.Resources.Values;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace KG.Props.Financial.Models
{
    public class Curve : List<CurveNode>, ICloneable
    {
        public DateTime LoadDate { get; set; } // used for excels only

        /// <summary>
        /// Reads curve nodes from a datatable (first two columns considered, first is date, 2nd is value).
        /// Returns a curve created with that data.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static Curve CreateFromDatatable(DataTable dt)
        {
            if (dt == null || dt.Columns.Count < 2)
                throw new Exception(Literals.InvalidColumnCount);

            // set columns to have proper names
            dt.Columns[0].ColumnName = "ValueDate";
            dt.Columns[1].ColumnName = "Value";

            var curve = dt.ToList<CurveNode>().AsCurve();
            curve.LoadDate = curve.FirstOrDefault()?.ValueDate ?? DateTime.Now.Date;
            return curve;
        }

        /// <summary>
        /// Reads curve nodes from a datatable as an Inflation Curve.
        /// First column (0) = Dates (including [0][0])
        /// Columns 1 - 16 = dates (so it's sort of 16x16 + dates column)
        /// => 17 columns total
        /// The data curve is from the row close to marketDate, and the date added is computed using bdr following-regular rule
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="marketDate">todo: describe marketDate parameter on CreateExpectedInflationCurveFromDatatable</param>
        /// <param name="holidayCalendar">todo: describe holidayCalendar parameter on CreateExpectedInflationCurveFromDatatable</param>
        /// <param name="idCurrency">todo: describe idCurrency parameter on CreateExpectedInflationCurveFromDatatable</param>
        /// <returns></returns>
        public static Curve CreateExpectedInflationCurveFromDatatable(DataTable dt, DateTime marketDate, HolidayCalendar holidayCalendar, string idCurrency)
        {
            var curve = new Curve();
            if (dt == null || dt.Columns.Count < 17)
                throw new Exception(Literals.InvalidColumnCount);

            // no data
            if (dt.Rows.Count == 0 || dt.Columns.Count == 0 || (dt.Rows[0][0] == null || dt.Rows[0][0] == DBNull.Value))
                return curve;

            // get column Index where date > marketDate
            var firstColumnName = dt.Columns[0].ColumnName;
            var startRow = 0;
            for (; startRow < dt.Rows.Count; startRow++)
            {
                var date = dt.Rows[startRow].GetColumnValueMapped(firstColumnName, typeof(DateTime?)) as DateTime? ?? DateTime.Now.Date;
                if (date > marketDate) break;
            }

            if (--startRow < 0)
                throw new Exception(Literals.Date + Literals.Invalid);

            // get data from the row close to market date
            var years = Constants.ExpectedInflationYears;
            var dataRow = dt.Rows[startRow];
            for (var i = 1; i < 17; i++)
            {
                var date = marketDate.AddYears(years[i - 1]);
                date = holidayCalendar.ApplyBusinessDayRule(date, (int)BdrTypes.Following, (int)ConventionTypes.Regular, 0, idCurrency);

                var columnName = dt.Columns[i].ColumnName;
                var value = dataRow.GetColumnValueMapped(columnName, typeof(double?)) as double?;

                var node = new CurveNode(date, value);
                curve.Add(node);
            }

            return curve;
        }

        /// <summary>
        /// Create a Curve from a Anagraphic Details Node collection (MdNodeAnagraphic), at a market date, using the holiday calendar.
        /// Divides all the values by divisor
        /// </summary>
        /// <param name="details"></param>
        /// <param name="divisor"></param>
        /// <param name="marketDate"></param>
        /// <param name="calendar"></param>
        /// <param name="addFirstValue"></param>
        /// <param name="firstValue"></param>
        /// <returns></returns>
        public static Curve CreateFromDetails(CurveDetailsList details, double divisor, DateTime marketDate, HolidayCalendar calendar, bool addFirstValue = false, double firstValue = 1)
        {
            var curve = new Curve();

            if (details == null || details.Count == 0)
                return curve;

            if (addFirstValue)
                curve.Add(new CurveNode(marketDate, firstValue));

            foreach (var node in details)
            {
                var date = calendar.AdjustMarketDate(marketDate, node.Maturity, node.MaturityData, node.BusinessDays, node.Currency.Value, (int)node.Bdr.Value, (int)node.Convention.Value, node.DayShift);
                var value = node.PriceAtDate.Value / divisor;
                curve.Add(new CurveNode(date, value));
            }

            return curve;
        }


        /// <summary>
        /// Create a Curve from a Anagraphic Details Node collection (MdNodeAnagraphic), at a market date, using the holiday calendar.
        /// Divides all the values by divisor
        /// </summary>
        /// <param name="details"></param>
        /// <param name="divisor"></param>
        /// <param name="marketDate"></param>
        /// <param name="calendar"></param>
        /// <param name="addFirstValue"></param>
        /// <param name="firstValue"></param>
        /// <returns></returns>
        public static Curve CreateFromDetailsStandard(CurveDetailsList details, double divisor, DateTime marketDate, HolidayCalendar calendar, bool addFirstValue = false, double firstValue = 1)
        {
            var curve = new Curve();

            if (details == null || details.Count == 0)
                return curve;

            if (addFirstValue)
                curve.Add(new CurveNode(marketDate, firstValue));

            var lastNode = details
                .Where(x => x.ValidityStartDate <= marketDate)
                .OrderBy(x => x.ValidityStartDate).LastOrDefault(); // get most recent market data node
            if (lastNode == null)
                throw new Exception("There is no market data available for " + marketDate.ToString(Constants.DefaultDisplayDateFormat));

            foreach (var price in lastNode.Prices)
            {
                var date = price.ValueDate;
                var value = price.Value / divisor;
                curve.Add(new CurveNode(date, value));
            }

            return curve;
        }

        /// <summary>
        /// Modifies an Expected Inflation curve (returning a new one), applying the shift. 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="shift"></param>
        /// <param name="isAdd"></param>
        /// <returns></returns>
        public static Curve ModifyExpectedInflation(Curve input, double shift, bool isAdd)
        {
            if (input == null || input.Count == 0)
                return input;

            var output = new Curve();
            foreach (var node in input)
            {
                var value = node.Value;
                var newValue = isAdd ? (value + shift) : (value * (1 + shift));
                output.Add(new CurveNode(node.ValueDate, newValue));
            }

            return output;
        }


        /// <summary>
        /// Applies a divisor to the values of the current curve
        /// </summary>
        /// <param name="curveNodes"></param>
        /// <param name="divisor"></param>
        /// <returns></returns>
        public Curve ApplyDivisor(double divisor)
        {
            foreach (var item in this)
            {
                item.Value = item.Value / divisor;
            }

            return this;
        }

        /// <summary>
        /// Clones this Curve
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            var tempCurve = new Curve();

            foreach (var node in this)
            {
                tempCurve.Add(node.Clone() as CurveNode);
            }

            return tempCurve;
        }
    }
}
