﻿using System;
using System.Collections.Generic;

namespace KG.Props.Financial.Models
{
    public class CurveNode : ICloneable
    {
        public double Value { get; set; } = double.NaN;
        public DateTime ValueDate { get; set; } = DateTime.Now.Date;
        public string ValueDateStr { get; set; } // added

        public CurveNode() { }

        public CurveNode(DateTime date, double? value)
        {
            ValueDate = date;
            Value = value ?? double.NaN;
            ValueDateStr = date.ToShortDateString(); // added
        }


        public override bool Equals(object obj)
        {
            var node = obj as CurveNode;
            return node != null &&
                   ValueDate == node.ValueDate &&
                   EqualityComparer<double?>.Default.Equals(Value, node.Value);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ValueDate, Value);
        }

        public object Clone()
        {
            return new CurveNode(ValueDate, Value);
        }
    }
}
