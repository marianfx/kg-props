﻿using KG.Props.Resources.CommonModels;
using KG.Props.Resources.Enumerations;
using KG.Props.Resources.Text;
using KG.Props.Validations.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KG.Props.Financial.Models
{
    public class CurveDetailsNode
    {
        // data
        public int Id { get; set; } = 0;
        public string Description { get; set; } = string.Empty;
        public string Memo { get; set; } = string.Empty;
        public string Ticker { get; set; } = string.Empty;
        public string TickerFxRate { get; set; } = string.Empty;
        public int Years { get; set; } = 0;
        public int Maturity { get; set; } = 0;
        public int BusinessDays { get; set; } = 0;
        public int DayShift { get; set; } = 0;
        public string YellowKey { get; set; } = string.Empty;
        public DateTime ValidityStartDate { get; set; } = DateTime.Now.Date;
        public DateTime? ValidityEndDate { get; set; }
        public DateTime? LastDate { get; set; }

        // domain data
        public Descriptor Currency => new Descriptor(IdCurrency, DescCurrency);
        public NDescriptor Provider => new NDescriptor(IdProvider, DescProvider);
        public NDescriptor MaturityData => new NDescriptor(IdMaturity, DescMaturity);
        public NDescriptor DayCount => new NDescriptor(IdDayCount, DescDayCount);
        public NDescriptor Bdr => new NDescriptor(IdBdr, DescBdr);
        public NDescriptor Convention => new NDescriptor(IdConvention, DescConvention);
        public NDescriptor Compound => new NDescriptor(IdCompound, DescCompound);

        // collections
        public Prices Prices { get; set; } = new Prices();
        public Prices TmpPrices { get; set; } = new Prices();
        public PriceNode PriceAtDate { get; set; } = new PriceNode();

        // domain data unformatted
        public string IdCurrency { get; set; }
        public int IdProvider { get; set; }
        public int IdMaturity { get; set; }
        public int IdDayCount { get; set; }
        public int IdBdr { get; set; }
        public int IdConvention { get; set; }
        public int IdCompound { get; set; }
        public string DescCurrency { get; set; }
        public string DescProvider { get; set; }
        public string DescMaturity { get; set; }
        public string DescDayCount { get; set; }
        public string DescBdr { get; set; }
        public string DescConvention { get; set; }
        public string DescCompound { get; set; }


        public bool IsEmpty()
        {
            return Id == 0;
        }

        public bool IsClosed()
        {
            return !ValidityEndDate.HasValue;
        }

        public bool HasPrices()
        {
            return Prices != null && Prices.Count > 0;
        }

        public IEnumerable<string> Validate(IValidator validator)
        {
            var errors = validator
                .ValidateString(Ticker, Literals.Ticker)
                .ValidateString(Description, Literals.Description)
                .ValidateString(YellowKey, Literals.YellowKey)
                .ValidateDescriptor(Currency, Literals.Currency)
                .ValidateDescriptor(Provider, Literals.Provider)
                .ValidateDescriptor(MaturityData, Literals.Maturity)
                .ValidateDescriptor(Bdr, Literals.BDR)
                .ValidateDescriptor(Convention, Literals.Convention)
                .ValidateDescriptor(Compound, Literals.Compound)
                .ValidateDescriptor(DayCount, Literals.DayCount)
                .GetErrors();

            if (!errors.Any())
            {
                if ((int)MaturityData.Value != (int)FrequencyTypes.None && Maturity == 0)
                    errors.Add(Literals.Maturity + " " + Literals.Number + " " + Literals.MustBeSpecified);
            }

            return errors;
        }


        public override bool Equals(object obj)
        {
            var node = obj as CurveDetailsNode;
            return node != null &&
                   Id == node.Id &&
                   Description == node.Description &&
                   Memo == node.Memo &&
                   Ticker == node.Ticker &&
                   Years == node.Years &&
                   Maturity == node.Maturity &&
                   BusinessDays == node.BusinessDays &&
                   DayShift == node.DayShift &&
                   YellowKey == node.YellowKey &&
                   ValidityStartDate == node.ValidityStartDate &&
                   EqualityComparer<DateTime?>.Default.Equals(ValidityEndDate, node.ValidityEndDate) &&
                   EqualityComparer<Descriptor>.Default.Equals(Currency, node.Currency) &&
                   EqualityComparer<NDescriptor>.Default.Equals(Provider, node.Provider) &&
                   EqualityComparer<NDescriptor>.Default.Equals(MaturityData, node.MaturityData) &&
                   EqualityComparer<NDescriptor>.Default.Equals(DayCount, node.DayCount) &&
                   EqualityComparer<NDescriptor>.Default.Equals(Bdr, node.Bdr) &&
                   EqualityComparer<NDescriptor>.Default.Equals(Convention, node.Convention) &&
                   EqualityComparer<NDescriptor>.Default.Equals(Compound, node.Compound);
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(Id);
            hash.Add(Description);
            hash.Add(Memo);
            hash.Add(Ticker);
            hash.Add(Years);
            hash.Add(Maturity);
            hash.Add(BusinessDays);
            hash.Add(DayShift);
            hash.Add(YellowKey);
            hash.Add(ValidityStartDate);
            hash.Add(ValidityEndDate);
            hash.Add(Currency);
            hash.Add(Provider);
            hash.Add(MaturityData);
            hash.Add(DayCount);
            hash.Add(Bdr);
            hash.Add(Convention);
            hash.Add(Compound);
            return hash.ToHashCode();
        }
    }
}
