﻿using KG.Props.Extensions.DataTables;
using KG.Props.Resources.Text;
using KG.Props.Resources.Values;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace KG.Props.Financial.Models.Bi
{
    public class CurveBidimensional : List<CurveNodeBidimensional>, ICloneable
    {
        public DateTime LoadDate { get; set; } // used for excels only

        /// <summary>
        /// Reads curve nodes from a 16x16 datatable.
        /// First row = dates (including [0][0])
        /// First column is years (exluding [0][0] which is date)
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static CurveBidimensional CreateFromDatatable(DataTable dt)
        {
            var curve = new CurveBidimensional();
            if (dt == null || dt.Columns.Count < 1)
                throw new Exception(Literals.InvalidColumnCount);

            if (dt == null || dt.Rows.Count < 1)
                throw new Exception(Literals.InvalidRowCount);

            // column names will default to Column1, Column2 etc.
            var firstRow = dt.Rows[0] as DataRow;
            curve.LoadDate = firstRow.GetColumnValueMapped(0, typeof(DateTime?)) as DateTime? ?? DateTime.Now.Date;

            for (var i = 1; i < dt.Rows.Count; i++)
            {
                var dataRow = dt.Rows[i] as DataRow;
                var year = dataRow.GetColumnValueMapped(0, typeof(int)) as int? ?? 0;
                for (var j = 1; j < dt.Columns.Count; j++)
                {
                    var column = dt.Columns[j] as DataColumn;
                    var columnName = column.ColumnName;
                    var date = firstRow.GetColumnValueMapped(columnName, typeof(DateTime?)) as DateTime? ?? DateTime.Now.Date;
                    var value = dataRow.GetColumnValueMapped(columnName, typeof(double?)) as double?;
                    var node = new CurveNodeBidimensional(date, value, year);
                    curve.Add(node);
                }
            }

            return curve;
        }

        /// <summary>
        /// Create a Curve from a Anagraphic Details Node collection (MdNodeAnagraphic), at a market date, using the holiday calendar.
        /// Divides all the values by divisor
        /// </summary>
        /// <param name="details"></param>
        /// <param name="divisor"></param>
        /// <param name="marketDate"></param>
        /// <param name="calendar"></param>
        /// <returns></returns>
        public static CurveBidimensional CreateFromDetails(CurveDetailsList details, double divisor, DateTime marketDate, HolidayCalendar calendar)
        {
            var curve = new CurveBidimensional();

            if (details == null || details.Count == 0)
                return curve;

            foreach (var node in details)
            {
                var year = node.Years;
                var date = calendar.AdjustMarketDate(marketDate, node.Maturity, node.MaturityData, node.BusinessDays, node.Currency.Value, (int)node.Bdr.Value, (int)node.Convention.Value, node.DayShift);
                var value = node.PriceAtDate.Value / divisor;
                curve.Add(new CurveNodeBidimensional(date, value, year));
            }

            return curve;
        }


        /// <summary>
        /// Create a Curve from a Anagraphic Details Node collection (MdNodeAnagraphic), at a market date, using the holiday calendar.
        /// Divides all the values by divisor
        /// </summary>
        /// <param name="details"></param>
        /// <param name="divisor"></param>
        /// <param name="marketDate"></param>
        /// <param name="calendar"></param>
        /// <param name="addFirstValue"></param>
        /// <param name="firstValue"></param>
        /// <returns></returns>
        public static CurveBidimensional CreateFromDetailsStandard(CurveDetailsList details, double divisor, DateTime marketDate, HolidayCalendar calendar)
        {
            var curve = new CurveBidimensional();

            if (details == null || details.Count == 0)
                return curve;

            var lastValidity = details.Where(x => x.ValidityStartDate <= marketDate).ToList();

            if (lastValidity.Count == 0)
                throw new Exception("There is no market data available for " + marketDate.ToString(Constants.DefaultDisplayDateFormat));

            var biggestMostRecentDate = lastValidity.Max(x => x.ValidityStartDate);
            var lastNodes = details.Where(x => x.ValidityStartDate == biggestMostRecentDate); // get most recent market data node
            foreach (var item in lastNodes)
            {
                var year = item.Years;
                foreach (var price in item.Prices)
                {
                    var date = price.ValueDate;
                    var value = price.Value / divisor;
                    curve.Add(new CurveNodeBidimensional(date, value, year));
                }
            }

            return curve;
        }


        /// <summary>
        /// Returns the number of distinct years of the nodes in the list
        /// </summary>
        /// <returns></returns>
        public int CountYears()
        {
            var actualDifferent = this.Select(x => x.Year).Distinct().Count();
            return Math.Max(1, actualDifferent);
        }

        /// <summary>
        /// Counts the number of different dates per year (all years have the same number of dates so returns only for the first one)
        /// </summary>
        /// <returns></returns>
        public int CountDates()
        {
            var i = 0;
            var n = this.Count;
            CurveNodeBidimensional o1;
            CurveNodeBidimensional o2;
            do
            {
                if (i + 1 < n)
                {
                    o1 = this[i];
                    o2 = this[i + 1];
                    i++;
                }
                else
                {
                    i = n;
                    break;
                }
            } while (o1.Year == o2.Year);

            return i;

            // the following code is equivalent (somehow)
            //var firstYear = this.FirstOrDefault()?.Year;
            //if (firstYear == null) return 0;

            //return this.Where(x => x.Year == firstYear.Value).Select(x => x.ValueDate).Distinct().Count();
        }

        /// <summary>
        /// Applies a divisor to the current curve
        /// </summary>
        /// <param name="divisor"></param>
        /// <returns></returns>
        public CurveBidimensional ApplyDivisor(double divisor)
        {
            foreach (var item in this)
            {
                item.Value = item.Value / divisor;
            }

            return this;
        }


        public void SortByYear()
        {
            Sort((x, y) =>
            {
                var first = x.Year.CompareTo(y.Year);
                if (first != 0)
                    return first;

                // equals, compare by dates
                return x.ValueDate.CompareTo(y.ValueDate);
            });
        }

        public object Clone()
        {
            var output = new CurveBidimensional();
            foreach (var item in this)
            {
                output.Add(item.Clone() as CurveNodeBidimensional);
            }
            return output;
        }
    }
}
