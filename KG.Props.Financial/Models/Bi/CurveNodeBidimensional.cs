﻿using System;

namespace KG.Props.Financial.Models.Bi
{
    public class CurveNodeBidimensional : CurveNode
    {
        public int Year { get; set; }

        public CurveNodeBidimensional() { }

        public CurveNodeBidimensional(DateTime date, double? value, int year) : base(date, value)
        {
            Year = year;
        }


        public new object Clone()
        {
            return new CurveNodeBidimensional()
            {
                Value = Value,
                ValueDate = ValueDate,
                Year = Year
            };
        }
    }
}
