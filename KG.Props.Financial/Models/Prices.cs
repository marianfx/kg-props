﻿using KG.Props.Financial.Extensions.Specifics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KG.Props.Financial.Models
{
    public class Prices : List<PriceNode>
    {
        public Prices() { }

        public Prices(int idNode, ICollection<CurveNode> values)
        {
            if (values == null || !values.Any())
                return;
            foreach (var value in values)
            {
                Add(new PriceNode(idNode, value.ValueDate, value.Value));
            }
        }


        public Prices RemoveBetweenDates(DateTime startDate, DateTime endDate)
        {
            RemoveAll(x => x.ValueDate > startDate && x.ValueDate < endDate);
            return this;
        }

        public Prices GetAllBetweenDates(DateTime startDate, DateTime endDate)
        {
            return this.Where(x => x.ValueDate >= startDate && x.ValueDate <= endDate).ToList().AsPrices();
        }

        public Prices GetAllByNodeId(int nodeId)
        {
            return this.Where(x => x.Id == nodeId).ToList().AsPrices();
        }

        public PriceNode GetByIdAndDate(int nodeId, DateTime date)
        {
            var existing = this.FirstOrDefault(x => x.Id == nodeId && x.ValueDate == date);
            return existing ?? new PriceNode();
        }

        public PriceNode GetFxRateFromDate(DateTime date)
        {
            var first = this.FirstOrDefault();
            if (first == null || date < first.ValueDate)
                return new PriceNode();

            var possible = this.FirstOrDefault(x => x.ValueDate == date);
            if (possible != null)
                return possible;

            return GetFxRateFromDate(date.AddDays(-1));
        }

        public void SortByDate()
        {
            Sort((x, y) => x.ValueDate.CompareTo(y.ValueDate));
        }
    }
}
