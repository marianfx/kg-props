﻿using KG.Props.Financial.Extensions.Specifics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KG.Props.Financial.Models
{
    public class CurveDetailsList: List<CurveDetailsNode>
    {
        public CurveDetailsNode GetById(int id)
        {
            return this.FirstOrDefault(x => x.Id == id);
        }

        public CurveDetailsNode GetByTicker(string ticker)
        {
            return this.FirstOrDefault(x => x.Ticker == ticker);
        }

        public CurveDetailsList Filter(DateTime date, string ticker = null, string desc = null, string idCurrency = null, int idProvider = -1)
        {
            var output = new Prices();
            var filtered = this.Where(x => x.ValidityStartDate <= date && (x.ValidityEndDate == null || x.ValidityEndDate > date));
            if (string.IsNullOrWhiteSpace(ticker) && string.IsNullOrWhiteSpace(desc) && string.IsNullOrWhiteSpace(idCurrency) && idProvider == -1)
                return filtered.ToList().AsDetailsList();

            if (!string.IsNullOrWhiteSpace(ticker))
                filtered = filtered.Where(x => x.Ticker == ticker);
            if (!string.IsNullOrWhiteSpace(idCurrency))
                filtered = filtered.Where(x => x.Currency?.Value == idCurrency);
            if (!string.IsNullOrWhiteSpace(desc))
                filtered = filtered.Where(x => x.Description == desc);
            if (idProvider != -1)
                filtered = filtered.Where(x => x.Provider?.Value == idProvider);

            return filtered.ToList().AsDetailsList();
        }

        public CurveDetailsList FilterByStrings(string ticker = null, string desc = null)
        {
            var filtered = this;
            if (string.IsNullOrWhiteSpace(ticker) && string.IsNullOrWhiteSpace(desc))
                return filtered.ToList().AsDetailsList();

            if (!string.IsNullOrWhiteSpace(ticker))
                filtered = filtered.Where(x => x.Ticker.ToLower().Contains(ticker.ToLower())).ToList().AsDetailsList();
            if (!string.IsNullOrWhiteSpace(desc))
                filtered = filtered.Where(x => x.Description.ToLower().Contains(desc.ToLower())).ToList().AsDetailsList();

            return filtered.ToList().AsDetailsList();
        }

        public CurveDetailsList FilterByIds(params int[] ids)
        {
            return this.Where(x => ids.Contains((int)x.Id)).ToList().AsDetailsList();
        }
    }
}
