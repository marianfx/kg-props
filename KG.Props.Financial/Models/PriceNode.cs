﻿using System;

namespace KG.Props.Financial.Models
{
    public class PriceNode : CurveNode
    {
        public int Id { get; set; } = 0;

        public PriceNode() { }

        public PriceNode(int id, DateTime date, double? value) : base(date, value)
        {
            Id = id;
        }


        public override bool Equals(object obj)
        {
            return obj is PriceNode node &&
                   Id == node.Id &&
                   ValueDate == node.ValueDate;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, ValueDate);
        }
    }
}
