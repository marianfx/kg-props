﻿using KG.Props.Resources.CommonModels;

namespace KG.Props.Financial.Models
{
    public class IndexPerform
    {
        public double IndexBaseValue { get; set; }
        public string IndexTicker { get; set; }
        public NDescriptor Frequency { get; set; }
        public int NumberFrequency { get; set; }
        public NDescriptor DayCount { get; set; }
        public NDescriptor BDR { get; set; }
        public NDescriptor Convention { get; set; }
        public int DayShift { get; set; }
        public NDescriptor DateGenMethod { get; set; }

        public IndexPerform()
        {
            Frequency = new NDescriptor();
            DayCount = new NDescriptor();
            BDR = new NDescriptor();
            Convention = new NDescriptor();
            DateGenMethod = new NDescriptor();
        }
    }
}
