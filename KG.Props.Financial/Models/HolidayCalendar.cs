﻿using KG.Props.Resources.CommonModels;
using KG.Props.Resources.Enumerations;
using KG.Props.Resources.Values;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KG.Props.Financial.Models
{
    public class HolidayCalendar
    {
        public static string DefaultCurrency { get; set; } = Constants.DefaultCurrency;
        /// <summary>
        /// Holds holidays for each currency
        /// </summary>
        public Dictionary<string, ICollection<DateTime>> HolidaysPerCurrency { get; set; }

        public HolidayCalendar()
        {
            HolidaysPerCurrency = new Dictionary<string, ICollection<DateTime>>();
        }

        public HolidayCalendar(ICollection<DateTime> dates, string currency = null) : this()
        {
            HolidaysPerCurrency.Add(currency ?? DefaultCurrency, dates);
        }

        /// <summary>
        /// Returns dates for a given currency. If currency is not loaded / stored, returns empty list
        /// </summary>
        /// <param name="currency"></param>
        /// <returns></returns>
        public ICollection<DateTime> GetDates(string currency)
        {
            if (HolidaysPerCurrency == null)
                return new List<DateTime>();

            // return EUR calendar if calendar does not exist
            if (!HolidaysPerCurrency.Keys.Contains(currency) || HolidaysPerCurrency[currency] == null)
                return HolidaysPerCurrency.ContainsKey(DefaultCurrency)
                    ? HolidaysPerCurrency[DefaultCurrency]
                    : new List<DateTime>();

            return HolidaysPerCurrency[currency];
        }

        /// <summary>
        /// Returns true if the list of holiday for a given currency contains the given date
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool ContainsDate(string currency, DateTime date)
        {
            var dates = GetDates(currency);
            return dates.Any(d => d.ToOADate() == date.ToOADate());
        }

        /// <summary>
        /// Returns true if the  given date component is a holiday for that currency. Tha last parameter specifies if saturdays and sundays are holidays (default true).
        /// A date is considered holiday if it is included in the holiday calendar of the currency
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="dateToCheck"></param>
        /// <param name="includeSundaysAndSaturdays"></param>
        /// <returns></returns>
        public bool IsHoliday(string currency, DateTime dateToCheck, bool includeSundaysAndSaturdays = true)
        {
            if (includeSundaysAndSaturdays)
            {
                if (dateToCheck.DayOfWeek == DayOfWeek.Saturday || dateToCheck.DayOfWeek == DayOfWeek.Sunday)
                    return true;
            }

            var dates = GetDates(currency);
            return dates.Any(x => x.Date == dateToCheck.Date);
        }

        public DateTime AdjustMarketDate(DateTime marketDate, int maturityNumber, NDescriptor maturity, int businessDays, string idCurrency, int idBdr, int idConvention, int dayShift)
        {
            var effectiveDate = AddBusyDays(marketDate, businessDays, idCurrency);
            switch ((int)maturity.Value)
            {
                case (int)FrequencyTypes.Weeks:
                    effectiveDate = effectiveDate.AddDays(maturityNumber * 7);
                    break;
                case (int)FrequencyTypes.Months:
                    effectiveDate = effectiveDate.AddMonths(maturityNumber);
                    break;
                case (int)FrequencyTypes.Years:
                    effectiveDate = effectiveDate.AddYears(maturityNumber);
                    break;
            }

            return ApplyBusinessDayRule(effectiveDate, idBdr, idConvention, dayShift, idCurrency);
        }

        /// <summary>
        /// Adds to the input date, for each business day (daysToAdd), the currency's corresponding number of holiday days)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="daysToAdd"></param>
        /// <param name="idCurrency"></param>
        /// <returns></returns>
        public DateTime AddBusyDays(DateTime input, int daysToAdd, string idCurrency)
        {
            var output = input;
            for (var i = 0; i < daysToAdd; i++)
            {
                do
                {
                    output = output.AddDays(1);
                } while (IsHoliday(idCurrency, output));
            }

            return output;
        }

        public DateTime ApplyBusinessDayRule(DateTime input, int idBdr, int idConvention, int dayShift, string idCurrency)
        {
            switch (idBdr)
            {
                case (int)BdrTypes.Following:
                    var newDate = ComputeBusinessDayFollowingRegular(input, idCurrency, dayShift);
                    if (idConvention == (int)ConventionTypes.Regular)
                        return newDate;

                    if (newDate.Month != input.Month)
                        newDate = FindPreviousBusinessDay(input, idCurrency);

                    return newDate;
                case (int)BdrTypes.Preceding:
                    var newDatePrev = ComputeBusinessDayPrecedingRegular(input, idCurrency, dayShift);
                    if (idConvention == (int)ConventionTypes.Regular)
                        return newDatePrev;

                    if (newDatePrev.Month != input.Month)
                        newDatePrev = FindNextBusinessDay(input, idCurrency);

                    return newDatePrev;
                default:
                    return input;
            }
        }

        public DateTime ComputeBusinessDayFollowingRegular(DateTime input, string idCurrency, int n)
        {
            if (n == 0)
                return FindNextBusinessDay(input, idCurrency);

            var output = input;
            for (var i = 1; i <= n; i++)
            {
                output = DateTime.FromOADate(output.ToOADate() + 1);
                output = FindNextBusinessDay(output, idCurrency);
            }

            return output;
        }

        public DateTime ComputeBusinessDayPrecedingRegular(DateTime input, string idCurrency, int n)
        {
            if (n == 0)
                return FindPreviousBusinessDay(input, idCurrency);

            var output = input;
            for (var i = 1; i <= n; i++)
            {
                output = DateTime.FromOADate(output.ToOADate() - 1);
                output = FindPreviousBusinessDay(output, idCurrency);
            }

            return output;
        }

        /// <summary>
        /// Given a date, finds the closest next valid business day
        /// </summary>
        /// <param name="input"></param>
        /// <param name="idCurrency"></param>
        /// <returns></returns>
        private DateTime FindNextBusinessDay(DateTime input, string idCurrency)
        {
            var output = input;
            while (true)
            {
                // if sunday / saturday, switch to closest valid day
                var daysToAdd = output.DayOfWeek == DayOfWeek.Sunday ? 1 : (output.DayOfWeek == DayOfWeek.Saturday ? 2 : 0);
                output = new DateTime(output.Year, output.Month, output.Day);
                output = output.AddDays(daysToAdd);

                // if not sunday / saturday + not in holiday => OK
                if (!ContainsDate(idCurrency, output))
                    break;

                // else => try next
                output = DateTime.FromOADate(output.ToOADate() + 1);
            }

            return output;
        }


        /// <summary>
        /// Given a date, finds the closest previous valid business day
        /// </summary>
        /// <param name="input"></param>
        /// <param name="idCurrency"></param>
        /// <returns></returns>
        private DateTime FindPreviousBusinessDay(DateTime input, string idCurrency)
        {
            var output = input;
            while (true)
            {
                // if sunday / saturday, switch to closest valid day
                var daysToAdd = output.DayOfWeek == DayOfWeek.Sunday ? -2 : (output.DayOfWeek == DayOfWeek.Saturday ? -1 : 0);
                output = new DateTime(output.Year, output.Month, output.Day);
                output = output.AddDays(daysToAdd);

                // if not sunday / saturday + not in holiday => OK
                if (!ContainsDate(idCurrency, output))
                    break;

                // else => try next
                output = DateTime.FromOADate(output.ToOADate() - 1);
            }

            return output;
        }
    }
}
